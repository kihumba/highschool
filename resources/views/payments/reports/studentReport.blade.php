 @include('layouts.header')
@include('layouts.sidebar')
<main class="app-content">
	  <div class="app-title">
	    <div>
	      <h1><i class="fa fa-th-list"></i> Students fee report</h1>
	    </div>
	    <ul class="app-breadcrumb breadcrumb side">
	      <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
	      <li class="breadcrumb-item">Students fee report</li>
	      <li class="breadcrumb-item active"><a href="#">Students fee report</a></li>
	    </ul>
	  </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
                    <div class="row mb-3">
                        <h1 style="font-size:1.3em;padding:1%;width: 90%;font-weight: 400;font-family: sans-serif;color: #333" class="top_header"> Fee report for <emp style="color: #7801FF">{{ $studentName->student_name }}</emp> </h1>
                        <span style="float: right;font-size: 25px;font-weight: 300"><a href='{{ route("printreport", ["id" =>$id]) }}' target="_blank"><img src="https://img.icons8.com/offices/30/000000/print.png" style="width: 30px"></a></span>
                        <div style="width: 100%;">
                            <span style="float:right;font-size:13px;margin-top:1.5% !important;margin-right: 3%;">


                                Amount Paid : {{ number_format($amountPaid + $balanceAmountPaid, 2) }}<br>
                                Total Amount : {{ number_format($totalAmount + $totalBalanceAmountPaid + $totalAmountAddedFee, 2) }}<br>
                                <b>Total Balance : {{ number_format($balance + $balanceBroughtForward, 2) }}</b><br>
                            </span>
                        </div>
                    </div>

                     <table id="" class="table dt-responsive nowrap table-striped">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Votehead Name</th>
                                <th>Amount</th>
                                
                            </tr>
                        </thead>

                        <?php $num = 1; ?>

                        <tbody>
                              @foreach($balanceBroughtForwardDetails as $balanceBroughtForwardDetail)

                                    <tr>
                                        <?php $num += 1; $blanceDate = strtotime($balanceBroughtForwardDetail->created_at); ?>
                                        <td>{{date("j-M-Y", $blanceDate)}}</td>
                                        <td>PREVIOUS TERM FEE</td>
                                        <td>{{number_format($balanceBroughtForwardDetail->amount_paid, 2)}}</td>
                                    </tr>
                                    @endforeach

                                    @foreach($payments as $payment)
                                    <?php $num += 1; $date = strtotime($payment->created_at); ?>
                                    <tr>
                                         <td>{{ date("j-M-Y", $date) }}</td>
                                        <td>{{ $payment->votehead_name }}</td>
                                        <td>{{ number_format($payment->amount, 2) }}</td>
                                       
                                    </tr>
                                    @endforeach
                              @foreach($addedStudentFees as $addedStudentFee)
                                @if(!$addedStudentFee->receipt_number == null || !$addedStudentFee->receipt_number == 0)

                                <tr> 
                                    <td> {{ date("j-M-Y", strtotime($addedStudentFee->created_at)) }} </td>
                                    <td>
                                        <?php 
                                            $description = DB::table('added_fee')
                                                ->where('student_id', $id)
                                                ->pluck('fee_description')
                                                ->first();
                                        ?>
                                        {{$description}}
                                    </td>
                                    <td>{{ number_format($addedStudentFee->amount_paid, 2)}}</td>
                                    <td>
                                        
                                    </td>
                                </tr>
                                    @endif
                             @endforeach
                            @foreach($payments as $payment)
                            <?php $date = strtotime($payment->created_at); ?>
                            <tr>
                                 <td>{{ date("j-M-Y", $date) }}</td>
                                <td>{{ $payment->votehead_name }}</td>
                                <td>{{ number_format($payment->amount, 2) }}</td>
                               
                            </tr>
                            @endforeach

                              
                        </tbody>
                    </table>
                </div>

                </div>
            </div>
        </div>
    </div>
</div>
</main>
@include('layouts.footer')