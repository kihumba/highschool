<style>
    p {
        display:block;
        length:50px;
        word-wrap:break-word;
        text-align:justify !important;
        font-size:12px;
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
    }


    * {
        box-sizing: border-box;
        padding: 0 !important;
      }

      .menu {
        width: 22%;
        float: left;
        padding: 15px;
        margin-top:-1.3%;
        /*border: 1px solid red;*/
      }

      .menu11 {
        width: 0%;
        float: right;
        right: -2%;
        padding: 0px;
        margin-top:0%;
        /*border: 1px solid red;*/
      }

      .main {
        width: 65%;
        float: left;
        margin-top: -2.5%;
        /*padding: 15px;*/
        /*border: 1px solid red;*/
      }
      
      .mainm {
        width: 77%;
        float: left;
        margin-top: -3%;
        /*padding: 15px;*/
        /*border: 1px solid red;*/
      }

      .menu1 {
        width: 60%;;
        float: left;
        margin: -3% 0 0 0;
        font-size: 12px;
        color: #ED1C24;
      }
      
       .menu1m {
        width: 100%;
        float: left;
        margin: -3% 0 0 0;
        font-size: 12px;
        color: #ED1C24;
      }

      .main1 {
        width: 41%;
        float: right;
        margin: 0% -8% 0 0;
      }

      .menu1 ul, .main1 ul{
        font-size: 10px;
        list-style-type: none;
      }

      .head1{
        width: 95%;
        padding: 15px;
        margin-top: 6.5%;
        text-align: center;
        margin-left: 1%;
        font-weight: 600;
        font-size: 11.5px;
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
      }
      
      
      .head1m{
        width: 95%;
        padding: 15px;
        margin-top: 8%;
        text-align: center;
        margin-left: 3%;
        font-weight: 600;
        font-size: 11.5px;
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
      }




      .menu12 {
        width: 50%;
        float: left;
        margin: -3% 0 0 0;
        font-size: 12px;
      }

      .main12 {
        width: 50%;
        float: right;
        margin: 0 0 0 0;
        font-size: 12px;
      }

      .menu121 {
        width: 49%;
        float: left;
        font-size: 12px;
        position: relative;
        word-break: break-all;
        word-wrap: break-word;
      }

      .main121 {
        width: 49%;
        float: right;
        position: relative;
        font-size: 12px;
      }


    .kam_cap{
        text-transform: uppercase;
    }

    .checkboxes label {
        display: block;
        float: left;
        /* padding-right: 10px; */
        white-space: nowrap;
    }
    .checkboxes input {
        vertical-align: middle;
    }
    .checkboxes label span {
        vertical-align: middle;
    }

    .head_{
        text-align:left;
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
        font-size: 20px;
        color: #00A650;
        font-weight: 600;
        border-bottom:2px solid #00A650;
    }

    .head{
        text-align:left;
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
        font-size: 13px;
        font-weight: normal;
    }

    ._contacts{
        text-align:right;
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
        font-size: 11px;
        font-weight: 550;
    }

    ._fill_width{
        width:100%;
    }

    ._center{
        text-align:center;
    }

    ._left{
        text-align:left;
    }

    ._right{
        text-align:right;
    }

    .table_font{
        font-size:12px;
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
    }

    small{
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
        font-size:11px;
    }

    ._end{
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
        font-size:11px;
    }

    ._test{
        margin-top:-8px;
    }

    .signature{
        height:75px;
    }

    .holder{
        visibility: hidden;
    }

    .heade{
        width: 100%;
    }

    .column{
        float: left;
        padding: 10px;
    }

    .heade:after {
        content: "";
        display: table;
        clear: both;
    }

    .fees td{
        font-weight:normal;
    }

    .official table td{
        border: 1px solid #333;
        border-collapse: collapse;
        float: right;
        font-size:12px;
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
    }

    .prgh{
        font-size:12px;
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
    }

    .official table {
        width: 50%;
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
    }

    .official b, .salute,  .items td{
        font-size:12px;
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
    }


    .official table td{
        width: 50%;
        padding: 5px;
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
    }

    b{
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
    }

    .fees  td{
        border: 1px solid #cecece;
        border-collapse: collapse;
        float: right;
        font-size:11px;
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
        padding: 4px;
    }

    .fees  th{
        border: 1px solid #cecece;
        border-collapse: collapse;
        float: right;
        font-size:11px;
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
        padding: 4px;
    }

    .banks td{
        font-size:12px;
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
        padding: 8px;
    }

    .headd{
        font-size:16px;
        font-weight: bold;
        font-family: "Guardian Text Sans Web","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
    }

    li label{
      vertical-align: middle;
    }

    .side{
      width: 50% !important;
    }

    .side li{
      display: inline-block;
    }

    .side1 li{
      display: inline-block;
    }
    
</style>

    <div style="margin-left: -2em">
         <img src="images/img.png" alt="No header">
    </div>

<hr color="green"/>

<p class="_center" style="font-size: 13px;"> Fee report for <b>{{ $name }} </b> </p>
<p class="_left">
    <table style="border: 1px solid #333;width: 100%;border-collapse: collapse" class="fees table_font">
      <thead>
          <tr>
              <td style="font-weight:bold">Date</td>
              <td style="font-weight:bold">Vothead Name</td>
              <td style="font-weight:bold">Amount</td>
              
          </tr>
      </thead>
      <tbody>
        <?php $num = 1; ?>
         @foreach($payments as $payment)
            <?php $date = strtotime($payment->created_at); ?>
            <tr>
                <td>{{ date("j-M-Y", $date) }}</td>
                <td>{{ $payment->votehead_name }}</td>
                <td>{{ number_format($payment->amount, 2) }}</td>
               
            </tr>
        @endforeach
         @foreach($addedStudentFees as $addedStudentFee)
            @if(!$addedStudentFee->receipt_number == null || !$addedStudentFee->receipt_number == 0)

            <tr> 
                <td> {{ date("j-M-Y", strtotime($addedStudentFee->created_at)) }} </td>
                <td>
                    <?php 
                        $description = DB::table('added_fee')
                            ->where('student_id', $id)
                            ->pluck('fee_description')
                            ->first();
                    ?>
                    {{$description}}
                </td>
                <td>{{ number_format($addedStudentFee->amount_paid, 2)}}</td>
                <td>
                    
                </td>
            </tr>
                @endif
         @endforeach
        @foreach($balanceBroughtForwardDetails as $balanceBroughtForwardDetail)

            <tr>
                <?php $blanceDate = strtotime($balanceBroughtForwardDetail->created_at); ?>
                <td>{{date("j-M-Y", $blanceDate)}}</td>
                <td>PREVIOUS TERM FEE</td>
                <td>{{number_format($balanceBroughtForwardDetail->amount_paid, 2)}}</td>
            </tr>
            @endforeach

           
      </tbody>
    </table>
</p>

<p class="_left">
    <div style="width: 100%;">
        <span style="float:right;font-size:13px;margin-top:1.5% !important;margin-right: 2.1%;">
           

        Amount Paid : {{ number_format($amountPaid, 2) }}<br>
        Total Amount : {{ number_format($totalAmount, 2) }}<br>
        <b>Total Balance : {{ number_format($balance , 2) }}</b><br>
        </span>
    </div>
</p>


