<style type="text/css">
  .fees{
    border-collapse: collapse;
    min-width: 100%;
    text-align: left;
  }

  .lists{
    text-align: center;
    list-style: none !important;
  }

  .first-element{
    font-weight: bold;
  }

  .location{
    font-size: 0.7em;
    text-align: center;
    font-weight: bold;
  }

  .college-detail{
    font-size: 0.9em;
  }

  .footer {
      position: fixed; 
      bottom: -40px; 
      left: 0px; 
      right: 0px;
      height: 50px;
  }
</style>

<div class="main">
    <div style="margin-left: -2em">
         <img src="images/img.png" alt="No header">
    </div>
  <hr color="green"/>

  <br/>

  <span style="float:left; font-weight: bold;">Receipt #: {{ $receipt_no }}</span><br><br>
  <span style="float:left; font-weight: bold;">Date of payment: {{ date("j-M-Y", strtotime($datePaymentDone)) }}</span>
  <span style="float:right; font-weight: bold;">Date: {{ date("j-M-Y", strtotime($receiptDate)) }}</span>

  <br/><br/><br/>

  <span>Payment of fees received for <span style="font-weight: bold;">{{ $student->student_name }}</span> with particulars as follows:</span>

  <br/><br/><br/>
   <table>
          <tr>
            <td>Bank:</td>
            <td>
              @if($receiptData->payment_method == 1)
                <img style="width:20px;" src="images/checked.png">
              @else
                <img style="width:20px;" src="images/unchecked.png">
              @endif
            </td>
      
            <td>M-Pesa:</td>
            <td>
              @if($receiptData->payment_method == 2)
                <img style="width:20px;" src="images/checked.png">
              @else
                <img style="width:20px;" src="images/unchecked.png">
              @endif
            </td>
          
            <td>Cash:</td>
            <td>
              @if($receiptData->payment_method == 3)
                <img style="width:20px;" src="images/checked.png">
              @else
                <img style="width:20px;" src="images/unchecked.png">
              @endif
            </td>
          
            <td>Other:</td>
            <td>
              @if($receiptData->payment_method == 4)
                <img style="width:20px;" src="images/checked.png">
              @else
                <img style="width:20px;" src="images/unchecked.png">
              @endif
            </td>
          </tr>
  </table>
  <br/>
  <table border="1" class="fees">
    <tr>
      <th>VOTEHEAD NAME</th>
      <th>AMOUNTS PAID<span style="font-style: italic;">(KSHS)</span></th>
    </tr>
    @foreach($paidVoteheads as $paidVotehead)
    <tr>
      <td>{{$paidVotehead->votehead_name}}</td>
      <td>{{ number_format($paidVotehead->amount, 2)}}</td>
    </tr>
    @endforeach
  </table>
  <br/><br/><br/>
  <table border="0" class="fees">
     <tr>
      <td style="width: 65%"></td>
      <td class="first-element" style="width:25%;">Total amount due:</td>
      <td>{{ number_format($receiptData->initial_balance, 2) }}</td>
    </tr>
    <tr>
      <td style="width: 65%"></td>
        <td class="first-element">Amount received:</td>
        <td>{{ number_format($receiptData->amount_paid, 2) }}</td>
      </tr>

      <tr>
        <td style="width: 65%"></td>
        <td class="first-element">Balance due:</td>
        <td>{{ number_format($receiptData->new_balance, 2) }}</td>
      </tr>
    </table>

  <br/><br/><br/>

  <span style="float:right;">
    <span style="font-weight: bold;">Served by:</span> {{ $servedBy }} <br/><br/><br/>
    <span style="font-weight: bold;">Signed:</span> _____________________
  </span>                       
                        
      
</div>                    
        
      
