@include('layouts.header')
@include('layouts.sidebar')
<main class="app-content">
	 <div class="app-title">
	    <div>
	    	<?php $studentName = DB::table('students')->where('id', $student_id)->pluck('student_name')->first();?>
	      <h1><i class="fa fa-users"></i> Fee payment for <span style="color: #009688">{{ $studentName }}</span></h1>
	    </div>
	    <ul class="app-breadcrumb breadcrumb">
	      <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
	      <li class="breadcrumb-item"><a href="{{route('students')}}">Students</a></li>
	      <li class="breadcrumb-item"><a href="#">Fee payment</a></li>
	    </ul>
	  </div>
	  <div class="row">
	    <div class="col-md-12 ">
	      <div class="tile">
	        <div class="tile-body">

			     <div class="alert alert-danger kam-error2" role="alert" style="display:none;">
			      Input Amount And Date payment was Done
			    </div>
			    <div class="row" style="padding-top:2em;">
			        <div class="col-md-6">
			            <label></label>
			            <input type="number" class="form-control received_input" placeholder="Enter amount received">
			            <div class="row payment_m" style="margin-top:1em;">
			                <div class="col-md-3" style="font-weight: bold; color:#456EF3;">Payment method:</div>
			                <div class="col-md-2">
			                    Bank:
			                    <input type="checkbox" name="payment" class="payment_method" value="1" onclick="onlyOne(this)">
			                </div>
			                <div class="col-md-2">
			                    M-Pesa:
			                    <input type="checkbox" name="payment" class="payment_method" value="2" onclick="onlyOne(this)">
			                </div>
			                <div class="col-md-2">
			                    Cash:
			                    <input type="checkbox" name="payment" class="payment_method" value="3" onclick="onlyOne(this)">
			                </div>
			                <div class="col-md-2">
			                    Other:
			                    <input type="checkbox" name="payment" class="payment_method" value="4" onclick="onlyOne(this)">
			                </div>
			            </div>

			             <div class="row" style="padding-top:2em;">
			                <div class="col-md-3" style="font-weight: bold; color:#456EF3;">Payment Date:</div>
			              <div class="col-md-6">
			               <input class="form-control datetimepicker-input" id="demoDate" type="text" placeholder="Select Date">
			              </div>
			            </div>
			            


			        </div>
			        
			        <div class="col-md-2">
			            <label>Received</label><br>
			            <span class="amount_received ">0</span>
			        </div>

			        <div class="col-md-2">
			            <label>Assigned</label><br>
			            <span class="amount_assigned ">0</span>
			        </div>

			        <div class="col-md-2">
			            <label>Balance</label><br>
			            <span class="amount_balance ">0</span>
			        </div>
			    </div>


			    <div class="row">
			    	<div class="col-md-12">
			    		<div class="single-table" style="padding-top:1em;">
			                <div class="table-responsive">

			                  <div class="card">
			                    <div class="card-body">
			                    <table class="table">

			                        <thead>
			                        	@if($balanceBroughtForward > 0)
			                                <tr>
			                                    <td colspan="7"><b style="font-size: 17px" class="bal"> Balance Brought Forward: {{ number_format($balanceBroughtForward, 2) }}</b></td>
			                                </tr>
			                                 <tr>
			                                    <td colspan="5">
			                                        <form action="">
			                                            @csrf

			                                            
			                                            <input type="hidden" name="stud_id" id="stud_id" value="{{ $student_id }}">
			                                            <div class="row ">
			                                                <div class="col-md-4">
			                                                     <input type="text" name="amountPaid" class="blanceamountPaid" id="amountPaid"placeholder="Balance Brought Forward" required class="form-control form-control-sm" style="
			                                             width:100%">
			                                                </div>
			                                                <div class="col-md-2">
			                                                    <button type="button" id="balanceBroughtForward" class="btn btn-sm btn-flat btn-primary" >Pay</button>
			                                                </div>
			                                            </div>
			                                            
			                                        </form>
			                                    </td>
			                                </tr>
			                            @endif
			                        	@if($addedFeeBalance > 0)
			                                <tr>
			                                    <td colspan="7"><b style="font-size: 17px" class="balanc"> {{$addedFeeDescription}}: {{ number_format($addedFeeBalance, 2) }}</b></td>
			                                </tr>
			                                 <tr>
			                                    <td colspan="5">
			                                        <form action="">
			                                            @csrf

			                                            
			                                            <input type="hidden" name="studentId" id="studentId" value="{{ $student_id }}">
			                                            <div class="row ">
			                                                <div class="col-md-4">
			                                                     <input type="text" name="amountPaidA" class="blanceamountPaid" id="amountPaidA"placeholder="balance for {{ $addedFeeDescription }}" required class="form-control form-control-sm" style="
			                                             width:100%">
			                                                </div>
			                                                <div class="col-md-2">
			                                                    <button type="button" id="addedFeeBalance" class="btn btn-sm btn-flat btn-primary" >Pay</button>
			                                                </div>
			                                            </div>
			                                            
			                                        </form>
			                                    </td>
			                                </tr>
			                            @endif

			                            <tr>
			                                <th> Vote Head </th>
			                                <th> Votehead Amount (KSH) </th>
			                                <th> Paid (KSH) </th>
			                                <th> Balance (KSH) </th>
			                                <th> Make Payment </th>
			                                <th></th>
			                            </tr>
			                        </thead>
			                        
			                        <tbody class="dataappend">

			                            @foreach( $balances as $balance )
			                                @foreach( $balance as $b )
			                                    <tr>
			                                        <td> {{ $b->votehead_name }} </td>
			                                        <td> {{ number_format($b->votehead_amount, 2) }} </td>
			                                        <td> {{ number_format($b->paid, 2) }}</td>
			                                        <td>{{ number_format($b->balance, 2) }}</td>
			                                        <td>
			                                            <input type="text" class="amountPaid form-control form-control-sm amount_paid"
			                                            votehead_id = "{{ $b->votehead_id }}"
			                                            semester_id = "{{ $b->semester_id }}"
			                                            student_id = "{{ $student_id }}" id="kam_{{ $b->votehead_id }}">

			                                            <td> <span id="unlock_{{ $b->votehead_id }}" class="fa fa-send unlock" style="color: #4236F8;cursor: pointer;"></span>  </td>
			                                        </td>
			                                    </tr>
			                                @endforeach
			                            @endforeach
			                        </tbody>

			                    </table>
			                </div>
			            </div>
			    	</div>
			    </div>

			</div>

			</div>
	        </div>
	    </div>
	</div>
</div>
</main>

@include('layouts.footer')