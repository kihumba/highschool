@include('layouts.header')
@include('layouts.sidebar')


    <!-- Main Content Area -->
     <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Semesters</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item">Semesters</li>
          <li class="breadcrumb-item active"><a href="#">Semesters</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="tile">
            <div class="tile-body">
            <div class="table-responsive">
                 <table class="table table-hover table-bordered" id="sampleTable">
                    <thead>
                        <tr>
                            <th>Semester Name</th>
                            <th> Added by </th>
                            <th> Current </th>
                            <th> Action </th>
                        </tr>
                    </thead>

                    <?php $num = 0; ?>
                    <tbody class="semesters">
                        @foreach($semesters as $semester)
                        <?php $num++; ?>
                        <tr>
                            <td>
                                    {{ $semester->semester_name}}
                                   
                            </td>
                            <td>{{$semester->name}}</td>
                            <td>
                                @if($semester->current == 1)
                                   <span class="text-success"> Active </span>
                                @else
                                <span> - </span>
                                @endif
                            </td>
                            <td>
                                    <a data-href="#" data-toggle="modal" data-target="#editmyModal{{$semester->id}}"><i class="fa fa-edit text-primary"></i></a>
                                    &nbsp;
                                    <a data-href="#" data-toggle="modal" data-target="#myModal{{ $semester->id}}"><i class="fa fa-trash text-danger"></i></a>

                                    @if($semester->current == 0)
                                        &nbsp;

                                              <a data-href="#" data-toggle="modal" data-target="#confirmsemester{{$semester->id }}"><i class="fa fa-check-circle text-success"></i></a>
                                    @endif

                                        <!--delete modal-->
                                        <div class="modal fade" id="myModal{{ $semester->id}}">
                                            <div class="modal-dialog modal-dialog-centered modal-sm" role="delete">
                                                <div class="modal-content">
                                                    <div class="modal-header bg">
                                                        <h5 class="modal-title"> Comfirm Delete</h5>
                                                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                                    </div>
                                                    <div class="modal-body">

                                                        <P>You are about to delete this semester?</P>
                                                            <div class="row" style="padding-top:1em;"> 
                                                                <div class="col-md-6">
                                                                    <button type="button" class="btn btn-block btn-secondary btn-sm" data-dismiss="modal">Close</button>
                                                                </div>
                                                                <div class="col-md-6">
                                                            <a class="btn btn-danger btn-block  btn-sm btn-ok" href="{{route('deleteSemester',$semester->id)}}">Delete</a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>   

                                         <!--semester confirm modal-->
                                        <div class="modal fade" id="confirmsemester{{ $semester->id}}">
                                            <div class="modal-dialog modal-dialog-centered modal-sm" role="delete">
                                                <div class="modal-content">
                                                    <div class="modal-header bg">
                                                        <h5 class="modal-title"> Comfirm Semester Shift</h5>
                                                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                                    </div>
                                                    <div class="modal-body">

                                                        <P>You are about to activate this Semester?</P>
                                                             <div class="row" style="padding-top:1em;"> 
                                                                 <div class="col-md-6">
                                                                    <button type="button" class="btn btn-block btn-secondary btn-sm" data-dismiss="modal">Close</button>
                                                                </div>
                                                                <div class="col-md-6">
                                                            <a class="btn btn-danger btn-sm btn-ok btn-block" href="{{ route('semesterUpdate', ['id' => $semester->id]) }}">Confirm</a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                                              <!--edit Modal -->
                                            <div class="modal fade" id="editmyModal{{$semester->id}}">
                                                <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header bg">
                                                            <h5 class="modal-title">Edit Semesters</h5>
                                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                                        </div>
                                                        <div class="modal-body">

                                                    <form method="POST" action="{{route('update_semester',$semester->id)}}">
                                                            @csrf

                                                                @if (Session::has('message'))
                                                                <div class="alert alert-danger">{{ Session::get('message') }}</div>
                                                                @endif


                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <label> Semester Name </label>
                                                                    <input type="text" value="{{$semester->semester_name}}" class="form-control form-control-sm" name="semester_name" required autocomplete="semester_name">
                                                                    </div>



                                                                </div>
                                                                <br/>

                                                                <div class="row" style="padding-top:1em;">
                                                                  
                                                                    <div class="col-md-12">
                                                                        <button type="submit" class="btn btn-primary btn-block btn-sm"> Update Semester </button>
                                                                    </div>
                                                                </div>

                                                            </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
         </div>
    </div>
</div>

  <div class="card col-md-4 " >
        <div style="padding:1em;">
             <form method="POST" action="{{route('add_semester')}}">
                    @csrf

                        <div class="alert alert-success kam-success" role="alert" style="display:none;">
                          Semester added successfully!
                        </div>
                        <div class="alert alert-danger kam-error" role="alert" style="display:none;">
                          Semester already exists!
                        </div>
                        <div class="alert alert-danger kam-error2" role="alert" style="display:none;">
                          Input Semester Name!
                        </div>

                <div class="row">
                    <div class="col-md-12">
                        <label> Semester Name </label>
                    <input type="text" class="form-control form-control-sm" id="semester_name" name="semester_name" required autocomplete="semester_name">
                    </div>



                </div>
                        <br/>

                <div class="row">
                  
                    <div class="col-md-12">
                        <button type="button" id="savesemester"class="btn btn-success btn-block btn-sm">Save</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
  


</main>

@include('layouts.footer')