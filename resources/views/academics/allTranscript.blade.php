<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		.top-heading{
			font-size: 1.5em;
			padding-left: 1.5em;
		}

		.header-b{
			text-transform: uppercase;
			font-weight: bold;
			text-align: center;
		}

		.boldText{
			font-weight: bold;
		}

		.table-detail tr td{
			padding-top:1em;
		}
		.results {
			border: 1px ;
			width: 100%;
			border-collapse: collapse;



		}
		.head{
 			 border-bottom: 1px solid black;
		}

		thead tr th, tfoot tr td{
 			padding:0.6em 0.2em;
		}

		.kborder td{
			border-top:0px !important;
			border-bottom:0px !important;
			padding:0.6em 0.5em;
		}

	</style>
</head>
<body>
	@foreach( $studentIds as $studentId )
	<?php
		$results = DB::table('results')
            ->join('students', 'students.id', '=', 'results.student_id')
            ->join('subjects', 'subjects.id', '=', 'results.subject_id')
            ->where('results.student_id', $studentId )
            ->where('results.semester_id', $semesterId )
            ->select(
                'results.cat_one',
                'results.cat_two',
                'results.end_term',
                'subjects.subject_name'
            )
            ->get();

        $studentName = DB::table('students')->where('id', $studentId)->pluck('student_name')->first();
        $AdmNo = DB::table('students')->where('id', $studentId)->pluck('admission_number')->first();
        $AdmDate = DB::table('students')->where('id', $studentId)->pluck('created_at')->first();
        $streamIds = DB::table('students')->where('id', $studentId)->pluck('stream_id')->first();
        $streamName = DB::table('streams')->where('id', $streamIds)->pluck('stream_name')->first();
        $formName = DB::table('forms')->where('id', $formId)->pluck('form_name')->first();
        $semester = DB::table('semesters')->where('id', $semesterId)->pluck('semester_name')->first();

         $studentForm = DB::table('students')->where('id',$studentId)->pluck('form_id')->first();
     
             $catOne = DB::table('results')
                ->where('semester_id', $semesterId)
                ->where('student_id', $studentId )
                ->pluck('cat_one')
                ->sum();
            $catTwo = DB::table('results')
                ->where('semester_id', $semesterId)
                ->where('student_id', $studentId )
                ->pluck('cat_two')
                ->sum();
            $endTerm = DB::table('results')
                ->where('semester_id', $semesterId)
                ->where('student_id', $studentId )
                ->pluck('end_term')
                ->sum();
            $totalMarks = $catOne + $catTwo + $endTerm;
           
            	if ($studentForm == 1) {
            		$gradeStudent=$totalMarks/7;
            	}elseif ($studentForm == 2) {
            		$gradeStudent=$totalMarks/7;
            	}elseif ($studentForm == 3) {
            		$gradeStudent=$totalMarks/7;
            	}elseif ($studentForm == 4) {
            		$gradeStudent=$totalMarks/7;
            	}
           
        	$studentCount = DB::table('students')->where('form_id', $studentForm)->count();
        	$catAllOne = DB::table('results')
                ->where('semester_id', $semesterId)
                ->where('form_id', $studentForm )
                ->pluck('cat_one')
                ->sum();
            $catAllTwo = DB::table('results')
                ->where('semester_id', $semesterId)
                ->where('form_id', $studentForm )
                ->pluck('cat_two')
                ->sum();
            $endAllTerm = DB::table('results')
                ->where('semester_id', $semesterId)
                ->where('form_id', $studentForm )
                ->pluck('end_term')
                ->sum();
            $totalAllMarks = $catAllOne + $catAllTwo + $endAllTerm;
        	$totalClassMarks = ($totalAllMarks/$studentCount);
        	$totalClassMarksGrade = ($totalClassMarks/$totalAllMarks)*100;




	 ?>	


	  <div style="margin-left: -2em">
         <img src="images/img.png" alt="No header">
    </div>

	<br/><br/>

	<table class="table-detail" style="min-width: 100%;">
		<tr>
			<td class="boldText">Name of the student:</td>
			<td><i>{{ $studentName }}</i></td>

			<td class="boldText">Reg. No.</td>
			<td><i>{{ $AdmNo }}</i></td>
		</tr>

		<tr>
			<td class="boldText">Stream:</td>
			<td><i>{{ $streamName }}</i></td>

			<td class="boldText">Date of Admission.</td>
			<td><i>{{ date("j-M-Y", strtotime($AdmDate)) }}</i></td>
		</tr>

		<tr>
			<td class="boldText">Level:</td>
			<td><i>{{ $formName }}</i></td>

			<td class="boldText">Term / Semester of study.</td>
			<td><i>{{ $semester }}</i></td>
		</tr>
	</table>
	<br>
	<table class="" border="1px" width="100%" cellspacing="0">
		<thead class="head">
			<tr>
				<th>SUBJECTS</th>
				<th>CAT ONE</th>
				<th>CAT TWO</th>
				<th>END TERM</th>
				<th>GRADE</th>
				<th>REMARK</th>
			</tr>
		</thead>

		<tbody>
				@foreach( $results as $result )	
			
				<tr class="kborder" style="text-align: center;">
					<td>{{ $result->subject_name }}</td>
					<td>
						@if($result->cat_one == null)
						<span> - </span>
						@else
						{{ $result->cat_one }}
						@endif
					</td>
					<td>
						@if($result->cat_two == null)
						<span> - </span>
						@else
						{{ $result->cat_two }}
						@endif
					</td>
					<td> @if($result->end_term == null)
						<span> - </span>
						@else
						{{ $result->end_term }}
						@endif
					</td>
					<td>
						<?php 
							$total = $result->cat_one + $result->cat_two + $result->end_term;
							if($total >= 70 && $total <= 100)
							    $grade = "A";
							elseif($total >= 60)
							    $grade = "B";
							elseif($total >= 50)
							    $grade = "C";
							elseif($total >= 40)
							    $grade = "D";
							elseif($total <= 39 && $total >= 0)
							    $grade = "E";
							else
							    $grade = "INV";
						?>

						{{ $grade }}
						
					</td>
					<td>
						<?php 
							$total = $result->cat_one + $result->cat_two + $result->end_term;
							if($total >= 70 && $total <= 100)
							    $remark = "Excellent";
							elseif($total >= 60)
							    $remark = "Well done! Keep up ";
							elseif($total >= 50)
							    $remark = "Put more effort";
							elseif($total >= 40)
							    $remark = "Pull up your socks";
							elseif($total <= 39 && $total >= 0)
							    $remark = "Poor Performance wake up!";
							else
							    $remark = "See your class Teacher";
						?>

					<span>{{ $remark }}</span>
					</td>
				</tr>
				@endforeach
				
		
			
		</tbody>
		<tfoot style="text-align: center;">
			<tr>
				<td colspan="2">Total Marks:{{$totalMarks}}</td>
				
				
				<td colspan="2">
					<?php 
							

						if($gradeStudent >= 70 && $gradeStudent <= 100)
						    $grade = "A";
						elseif($gradeStudent >= 60)
						    $grade = "B";
						elseif($gradeStudent >= 50)
						    $grade = "C";
						elseif($gradeStudent >= 40)
						    $grade = "D";
						elseif($gradeStudent <= 39 && $gradeStudent >= 0)
						    $grade = "E";
						else
						    $grade = "INV";
					?>

						Mean grade:{{ $grade }}

				</td>
				<td colspan="2">
						<?php 
							
						if($totalClassMarksGrade >= 70 && $totalClassMarksGrade <= 100)
						    $totalClassGrade = "A";
						elseif($totalClassMarksGrade >= 60)
						    $totalClassGrade = "B";
						elseif($totalClassMarksGrade >= 50)
						    $totalClassGrade = "C";
						elseif($totalClassMarksGrade >= 40)
						    $totalClassGrade = "D";
						elseif($totalClassMarksGrade <= 39 && $totalClassMarksGrade >= 0)
						    $totalClassGrade = "E";
						else
						    $totalClassGrade = "INV";

					?>

				Class mean Grade:{{$totalClassGrade}} of {{ number_format($totalClassMarks,1)}}	

			</td>
			
			</tr>
		</tfoot>
	
	</table>


	<br/><br/>

	<b>KEY TO GRADING SYSTEM & Remarks</b> <br/>

	<br/>

	<table>
		@foreach($remarks as $remark)
		<tr>
			<td>{{$remark->grade}}</td>
			<td>=</td>
			<td>{{$remark->min_marks}}% - {{$remark->max_marks}}%</td>
			<td style="padding-left: 1.5em">- {{$remark->remark}}</td>
		</tr>
		@endforeach

	</table>

	<div style="position:absolute; bottom:0px; padding-left: 3em"> <span>Date Issued __________________________</span> &nbsp;&nbsp;<span>Signed _____________________________</span></div>

		@endforeach
</body>
</html>