@include('layouts.header')
@include('layouts.sidebar')


    <!-- Main Content Area -->
     <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-book"></i> Academics</h1>
          <p>Upload Marks</p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item">Upload marks</li>
          <li class="breadcrumb-item active"><a href="#">Upload marks</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
                <div class="alert alert-success kam-success" role="alert" style="display:none;">
                    Marks uploaded successfully!
                  </div>
                  
                  <div class="alert alert-danger error2" role="alert" style="display:none;">
                    Marks cannot be more than 100!
                  </div>
                <div class="row">
                   <div class="col-md-3">
                    <label> Form </label>
                    <div class="input-group">
                        <select class="form-control form-control-sm forms" name="formId" style="padding:0px;">
                            <option selected disabled>Select Form</option>
                            @foreach( $forms as $form )
                                <option value="{{ $form->id }}">
                                    {{ $form->form_name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3 streams" style="display:none;">
                   <label> Streams </label>
                    <select name="streamId" class="form-control form-control-sm" id="streamId"></select>
                </div>
                <div class="col-md-3 subjects" style="display:none;">
                  <label> Subjects </label>
                  <div class="input-group">
                      <select class="form-control form-control-sm " name="subjectId" id="subjectId" style="padding:0px;">
                          <option selected disabled>Select a Subject</option>
                          @foreach( $subjects as $subject )
                              <option value="{{ $subject->id }}">
                                  {{ $subject->subject_name }}
                              </option>
                          @endforeach
                      </select>
                  </div>
                </div>
                 <div class="col-md-3 exams" style="display:none;">
                  <label> Exams </label>
                    <select class="form-control form-control-sm" id="examtype" >
                        <option selected disabled>--Select an exam--</option>
                        <option value="1">Cat 1</option>
                        <option value="2">Cat 2</option>
                        <option value="3">End term</option>
                    </select>
                  </div>
  
    
                </div>
                <hr/>

                <div class="row">
                    <div class="col-md-5 form_students"></div>
                    <div class="col-md-7">
                        <table class="table table-condensed table-sm">
                            <thead id="studentsTableHeader" style="display:none;">
                                <tr>
                                    <th>Student</th>
                                    <th>ADM No.</th>
                                    <th>Cat one</th>
                                    <th>Cat two</th>
                                    <th>End term</th>
                                </tr>
                            </thead>

                            <tbody id="uploaded_marks"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</main>

@include('layouts.footer')