@include('layouts.header')
@include('layouts.sidebar')


    <!-- Main Content Area -->
     <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Remarks</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item">Remarks</li>
          <li class="breadcrumb-item active"><a href="#">Remarks</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="tile">
            <div class="tile-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" id="sampleTable">
                   <thead>
                        <tr>
                            <th>#</th>
                            <th>Marks</th>
                            <th>Grade</th>
                            <th>Remarks</th>
                            <th>Added By</th>
                            <th> Action </th>
                        </tr>
                    </thead>

                    <tbody class="remarks">
                        <?php $num = 1 ?>
                        @foreach($remarks as $remark)
                        <tr>
                            <td>{{ $num++ }}</td>
                           <td>{{$remark->min_marks}}% - {{$remark->max_marks}}%</td>
                            <td>{{ $remark->grade}}</td>
                            <td>{{ $remark->remark}}</td>
                            <td>{{ $remark->name}}</td>
                            <td>
                                <a data-href="#" data-toggle="modal" data-target="#editmyModal{{$remark->id}}"><i class="fa fa-edit text-primary"></i></a>
                                <a data-href="#" data-toggle="modal" data-target="#myModal{{ $remark->id}}"><i class="fa fa-trash text-danger"></i></a>
                         
                             
                            </td>
                        </tr>
                                       <!--delete modal-->
                            <div class="modal fade" id="myModal{{ $remark->id}}">
                                <div class="modal-dialog modal-dialog-centered modal-sm" role="delete">
                                    <div class="modal-content">
                                        <div class="modal-header bg">
                                            <h5 class="modal-title"> Comfirm Delete</h5>
                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                        </div>
                                        <div class="modal-body">

                                            <P>You are about to delete this remark?</P>
                                                <div class="row" style="padding-top:1em;"> 
                                                    <div class="col-md-6">
                                                    <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>
                                                </div>
                                                <div class="col-md-6">
                                                     <a class="btn btn-sm btn-danger btn-block btn-ok" href="{{route('deleteRemark',$remark->id)}}">Delete</a>
                                                 </div>
                                                </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                                                <!--edit Modal -->
                            <div class="modal fade" id="editmyModal{{$remark->id}}">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg">
                                            <h5 class="modal-title"> Remarks</h5>
                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                        </div>
                                        <div class="modal-body">
                                           
                                            <form method="POST" action="{{route('updateRemark',$remark->id)}}">
                                                    @csrf

                                                @if (Session::has('message'))
                                                <div class="alert alert-danger">{{ Session::get('message') }}</div>
                                                @endif
                                             

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label> Minimum mark </label>
                                                        <input type="text" value="{{$remark->min_marks}}" class="form-control form-control-sm" name="min_marks" required autocomplete="min_marks" >
                                                    </div>

                                                    <div class="col-md-6">
                                                        <label> Maximum marks </label>
                                                        <input type="text" value="{{$remark->max_marks}}" class="form-control form-control-sm" name="max_marks" required autocomplete="max_marks" >
                                                    </div>


                                                    <div class="col-md-12">
                                                        <label> Remark </label>
                                                        <input type="text" value="{{$remark->remark}}" class="form-control form-control-sm" name="remark" required autocomplete="remark">
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label> Grade </label>
                                                        <input type="text" value="{{$remark->grade}}" class="form-control form-control-sm" name="grade" required autocomplete="grade">
                                                    </div>



                                                </div>
                                                <br/>

                                                <div class="row">
                                                
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary btn-block btn-sm"> Update </button>
                                                    </div>
                                                </div>

                                            </form>
                                              
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </tbody>
                </table>
            </div>
         </div>
    </div>
</div>

  <div class="card col-md-4 " >
        <div style="padding:1em;">
            <form method="POST" action="">
                @csrf

                <div class="alert alert-success kam-success" role="alert" style="display:none;">
                  Remark Saved successfully!
                </div>

                <div class="alert alert-danger kam-error" role="alert" style="display:none;">
                  Remark already exists!
                </div>
                <div class="alert alert-danger kam-error2" role="alert" style="display:none;">
                  Input all field!
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <label> Minimum Mark </label>
                        <input type="number" class="form-control form-control-sm" id="min_marks" required name="min_marks" autocomplete="min_marks">
                    </div>

                    <div class="col-md-6">
                        <label> Maximum Marks </label>
                        <input type="number" class="form-control form-control-sm" id="max_marks" required name="max_marks" autocomplete="max_marks">
                    </div>
                    

                     <div class="col-md-12">
                        <label> Remark </label>
                    <input type="text" class="form-control form-control-sm" id="remark" required name="remark" autocomplete="remark">
                    </div>
                    <div class="col-md-12">
                        <label> Grade </label>
                    <input type="text" class="form-control form-control-sm" id="grade" required name="grade" autocomplete="grade">
                    </div>

                    
                </div>
                <br/>

                <div class="row">

                    <div class="col-md-12">
                        <button type="button" id="saveRemark"class="btn btn-success btn-block btn-sm">Save</button>
                    </div>
                    
                </div>
            </form>
        </div>
    </div>
</div>
  


</main>

@include('layouts.footer')