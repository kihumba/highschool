@include('layouts.header')
@include('layouts.sidebar')


    <!-- Main Content Area -->
     <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Academics</h1>
          <p>Generate Student Report / Transcripts </p>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item">Transcripts</li>
          <li class="breadcrumb-item active"><a href="#">Transcripts</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-8 offset-md-2">
          <div class="tile">
            <div class="tile-body">
                 <div class="row">
                    <div class="col-md-4" >
                       <div class="form-check form-check-inline">
                          <input class="form-check-input" type="checkbox" id="studentCheckbox" name="studentCheckbox" value="singleTranscript">
                          <label class="form-check-label" for="studentCheckbox">(Check to print single transcript)</label>
                        </div>
                    </div>
                    
                </div>
                <div class="alert alert-success ksuccess" role="alert" style="display:none;">
                    printing done successfully!
                  </div>
                  
                  <div class="alert alert-danger error2" role="alert" style="display:none;">
                    Error Occured!
                  </div>
                <br>
              <div class="allTranscripts">
               <div class="row">
                   <div class="col-md-3">
                    <label> Form </label>
                    <div class="input-group">
                        <select class="form-control form-control-sm form" name="formId" style="padding:0px;">
                            <option selected disabled>Select Form</option>
                            @foreach( $forms as $form )
                                <option value="{{ $form->id }}">
                                    {{ $form->form_name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3 streams" style="display:none;">
                   <label> Streams </label>
                    <select name="streamId" class="form-control form-control-sm" id="streamId"></select>
                </div>
                <div class="col-md-3 print" style="display:none;">
                  <label> Print </label>
                  <a type="button" class="btn btn-primary btn-sm btn-block" id="getFormIdStreamId">print</a>
                   
                </div>

               </div>
             </div>

            <div class="singleTranscript" style="display: none">
               <div class="row">
                  <div class="col-md-6">
                      <label>Admission Number</label>
                      <div class="input-group">
                         <input type="text" id="admission" class="form-control" placeholder="Enter Admission Number" required>
                      </div>

                      <div class="alert alert-danger admissionNumError" role="alert" style="display:none;">
                        Please enter student admission number.
                      </div>

                      <br/>

                      <button type="button" id="searchStudentData" class="btn btn-primary btn-block btn-sm">Search</button>

                      <div class="selectSemester" style="display:none; margin-top:1.5em;">
                          <form method="POST" action="">
                              @csrf
                             <div class="col-sm-12">
                                <select  id="semesterId" class="form-control form-control-sm">
                                      <option selected disabled >--Select Semester--</option>
                                      @foreach($semesters as $semester)
                                          <option value="{{ $semester->id }}">{{ $semester->semester_name }}</option>
                                         
                                      @endforeach
                                       <option value="all">All</option>
                                </select>
                              </div> 
                          </form>
                      </div>

                  </div>

                  <div class="col-md-6">
                      <div class="studentDetails"></div>

                  </div>

                </div> 
                <div class="row">
                <div class="col-sm-12" >

                  <div class="alert alert-info noResults" role="alert" style="display:none;">
                    Results for the selected semester not available.
                  </div>

                  <div class="showResults" style="display:none;">
                    <div class="card-header" style="background-color: #D1F2FB">
                        Transcripts
                        <a target="_blank" href="{{ route('transcript') }}">
                          <i class="fa fa-print" style="float: right; font-size: 25px" ></i>
                        </a>
                    </div>
                    <div class="card-body ">
                       <div class="row">
                           <div class="col-sm-12">
                              <table class="table table-sm table-stripped">
                                <thead>
                                  <tr>
                                    <th>Unit name</th>
                                    <th>CAT one (<small>15</small>)</th>
                                    <th>CAT two (<small>15</small>)</th>
                                    <th>End term (<small>70</small>) </th>
                                    <th>Total</th>
                                    <th>Grade</th>
                                  </tr>
                                </thead>

                                <tbody class="semesterResults"></tbody>
                              </table>
                           </div>
                       </div>
                    </div>
                  </div>
                </div> 
              </div>
            </div>
            </div>
        </div>
    </div>
</div>

</main>

@include('layouts.footer')