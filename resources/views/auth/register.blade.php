<!DOCTYPE html>
<html lang="en">
  
<head>
    <!-- Twitter meta-->
   
    <title>Smart School</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="/assets/css/main.css">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- Font-icon css-->
  </head>
  <body>
    <section class="material-half-bg">
      <div class="cover"></div>
    </section>
    <section class="login-content">
      <div class="logo">
        <h1>Smart School</h1>
      </div>
      <div class="login-box col-md-3">
        <form method="POST" action="{{ route('register') }}" >
           @csrf
          <h3 class="login-head" style=""><i class="fa fa-lg fa-fw fa-user mt-4"></i>SIGN UP</h3>
          <div class="form-group">
            <label class="control-label"> {{ __('Name') }}</label>
           <input id="name" type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>
          <div class="form-group">
            <label class="control-label">{{ __('E-Mail Address') }}</label>
            <input id="email" type="email" class="form-control form-control-sm @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
          </div>
          <div class="form-group">
            <label class="control-label">{{ __('Password') }}</label>
             <input id="password" type="password" class="form-control form-control-sm @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
          </div> 
          <div class="form-group">
            <label class="control-label">{{ __('Confirm Password') }}</label>
             <input id="password-confirm" type="password" class="form-control form-control-sm" name="password_confirmation" required autocomplete="new-password">
          </div>
         
           <div class="form-group btn-container mb-4">
            <button class="btn btn-primary btn-block"><i class="fa fa-sign-in fa-lg fa-fw "></i> {{ __('Register') }}</button>
          </div>
          <div class="form-group">
            <div class="utility">
              <div class="animated-checkbox">

                 <!--  <label for="remember">
                   <a class="btn btn-sm btn-link" href="/">Back to website</a> -->
              </div>
                <a class="btn btn-sm btn-link"  href="{{ route('login') }}">Back to Login</a>
            </div>
          </div>
          
        </form>
       
      </div>
    </section>
    <!-- Essential javascripts for application to work-->
    <script src="/assets/js/jquery-3.3.1.min.js"></script>
    <script src="/assets/js/popper.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="/assets/js/plugins/pace.min.js"></script>
    <script type="text/javascript">
      // Login Page Flipbox control
      $('.login-content [data-toggle="flip"]').click(function() {
      	$('.login-box').toggleClass('flipped');
      	return false;
      });
    </script>
  </body>

<!-- Mirrored from pratikborsadiya.in/vali-admin/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 30 Apr 2020 12:16:17 GMT -->
</html>