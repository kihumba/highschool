<!DOCTYPE html>
<html>
<head>
	<title>nonTeachingCourses</title>
	<style type="text/css">
		.head{
			padding-left: 4em;
			color: #383092
		}
		.heading{
			color: #383092;
			text-align: center;
			font-weight: bold;
			padding-top: -1em
		}
		
		.head2{
			font-size: 1em;
			color: #383092
		}
		.check{
			height: 18px;
			width: 18px;
			padding-left: 0.5em
		}
		.final{
			padding-top: -0.8em;
		}
		
	</style>
</head>
<body>
	<div style="margin-left: -2.5em;padding-top: -2.7em">
		<img src="images/header.png">
	</div>
	<h4 style="color: red;text-align: center;padding-top: -3em;padding-bottom: 1em"><u>NON TEACHING COURSES</u></h4>
	<table style="padding-top: -1.5em">
		<tr>
			<td class="head2">Name:...............................................................................</td>
			<td class="head">Ref. No:___________________________</td>
		</tr>
		<tr>
			<td class="head2">Depertment:.....................................................................</td>
			<td class="head">Date:______________________________</td>
		</tr>
		<tr>
			<td class="head2">Courses:..........................................................................</td>
		</tr>
	
	</table>
	<h4 class="heading"><u>RE: LETTER OF ADMISSION/JOINING INSTRUCTIONS 2020/2021/u></h4 >
	<p>
		This is to inform you that you have been selected to join the International TTC. You are required to report and
		register on 4th May, 2020 and not later than 29th May, 2020 and hence commence studies.
	</p>
	<h4 style="font-weight: bolder;padding-top: -1em;">1. COURSES / PROGRAMS</h4>
	<p>Please tick the course of your choice below</p>
	<table  border="1px"  width="100%" cellspacing="0" style="padding-top: -1em">
		<tr>
			<th>COURSES</th>
			<th>CERTIFICATE</th>
			<th>DIPLOMA</th>
		</tr>
		<tr>
			<td>FASHION AND DESIGN- CLOTHING & TAILORING</td>
			<td><img src="images/check.png" class="check"></td>
			<td><img src="images/check.png"class="check"></td>
		</tr>
		<tr>
			<td>INFORMATION COMMUNICATION TECHNOLOGY (ICT)</td>
			<td><img src="images/check.png" class="check"></td>
			<td><img src="images/check.png" class="check"></td>
		</tr>
		<tr>
			<td>COMPUTER STUDIES</td>
			<td><img src="images/check.png" class="check"></td>
			<td><img src="images/check.png" class="check"></td>
		</tr>
		<tr>
			<td>FOOD AND BEVERAGES (CATERING)</td>
			<td><img src="images/check.png" class="check"></td>
			<td><img src="images/check.png" class="check"></td>
		</tr>
		<tr>
			<td>OFFICE MANAGEMENT(FRONT OFFICICE MANAGEMENT)</td>
			<td><img src="images/check.png" class="check"></td>
			<td><img src="images/check.png" class="check"></td>
		</tr>
		<tr>
			<td>SECRETARIAL STUDIES (ADMINISTRATIVE ASSISTANT)</td>
			<td><img src="images/check.png" class="check"></td>
			<td><img src="images/check.png" class="check"></td>
		</tr>
	</table>
	<h4 style="font-weight: bolder;padding-top: -0.8em;">2. DURATION</h4>
	<p style="padding-top: -1.3em;">
		Duration for our courses are four (4) semesters/terms for Certificate Courses and six (6) Semesters/Terms
 		For Diploma Courses.
	</p>
	<h4 style="font-weight: bolder;">3. FEES PAYMENT</h4>
	<p style="padding-top: -1.3em;">
		You will be required to pay fees on arrival as indicated in the attached fees structure. Fees should be paid to: 
	</p>
	<p style="padding-top: -0.5em;">
		<b>INTERNATIONAL TEACHING & TRAINING CENTRE</b> by mode of:
	</p>
	<p style="padding-top: -1em;">a) Bankers Cheque or</p>
	<p style="padding-top: -1em;">b) Pay in slips for cash deposited into:</p>
	<table  style="padding-top: -1em;">
		<tr>
			<td>ACCOUNT NAME: </td>
			<td>INTERNATIONAL TEACHING & TRAINING CENTRE</td>
		</tr> 
		<tr>
			<td>National Bank: </td>
			<td>01020060934300</td>
		</tr>
	</table>
	<p  style="padding-top: -1em;">Note: Cheques must be written to: INTERNATIONAL TEACHING & TRAINING CENTRE. Cash can be deposited to any branch of the above bank.
	</p>
	<p style="padding-top: -0.8em;">
		c) Personal cheques will NOT be accepted
	</p>
	<div style="margin-left: -2.8em;position: absolute;bottom: -3em;">
		<img src="images/footer2.png">
	</div>
	<h4 style="font-weight: bolder;margin-top: 5em">4. ACCOMMODATION</h4>
	<p>
		International TTC is a boarding college only, we do not have day scholars. Our fees include food, accommodation and
		tuition. Students should report with the following.	
	</p>
	<ul style="list-style: disc;padding-top: -0.8em">
		<li> 4 Inch single bed mattress (4” x 3 ft x 6ft)</li>
		<li> Heavy blanket and two blue plain bed sheets and cover</li>
		<li> Plastic trough or bucket</li>
		<li> Plate, mug and spoon</li>
		<li> Personal effects</li>
	</ul>
	<h4 style="font-weight: bolder;">5. RULES AND REGULATIONS</h4>
	<p style="padding-top: -0.8em">
		A copy of College Rules and Regulations is attached to the application form, you are required and abide by them.
	</p>
		<h4 style="font-weight: bolder;">6. GENERAL REQUIREMENTS.</h4>
	<p style="padding-top: -1.3em"> a) ALL STUDENTS</p>
	<ul style="list-style: disc;padding-top: -1.3em">
		<li> Photocopy of National Identity Card</li>
		<li> Photocopy of ALL academic certificates and testimonials</li>
		<li> Copy of Birth Certificate</li>
		<li> Original Certificate/testimonials for confirmation and return</li>
		<li> Adequate Writing materials</li>
		<li> Scientific Calculator</li>
	</ul>
	<p>
		b) FOOD - All students are served with a common college diet. Private cooking for students is not allowed within the
		college. The college diet comprises ugali, sukumawiki, meat, githeri , rice and beans. The college does not cater for
		special diet. Black tea or hot water and a quarter bread will be provided for breakfast. Hot water will also be available in the morning and break time.
	</p>
	<h4 style="font-weight: bolder;">7. CHANGE OF COURSE</h4>
	<p style="padding-top: -1.3em">
		Students wishing to change course have a maximum of twenty-eight days from the commencement date of the
		programme to apply for change of course
	</p>
	<h4 style="font-weight: bolder;">8.LOCATION AND DIRECTION TO COLLEGE</h4>
	<p style="padding-top: -1.3em">
	
		The International TTC is now situated in a new and prestigious campus between Kitengela and Isinya town next to
		Kitengela Boys High School along Nairobi-Namanga Highway. Matatu and buses to Kitengela are found at the Nairobi
		Railways station NOS.110 and Easy Coach Entrance (Rembo and Seven City Shuttle). Alight in Kitengela bus station.
		Board another Nissan to college at Kitengela Bus station (Naekana, Wamasaa or Super Coach) then alight on the way
		at teachers college stage, Hawa stage or at the next Kobil Station near Kenchic or call our numbers to be picked at
		Kitengela
	</p>
	<h4 style="font-weight: bolder;padding-top: -1.3em">9. DRESS CODE / COLLEGE UNIFORM</h4>
	<p style="padding-top: -1.3em">
		Students are required to be in full school uniform from Monday to Friday 8:00 to 4:30PM. Therefore, carry home clothing
		for personal use. The college uniform colour is Black trouser/skirts, white long sleeved shirts, a black tie, black sweater,
		sky blue track suit and white socks for ladies. Students are advised to buy black low soled shoes. Ladies using black
		trousers must buy a black blazer or coat. The college will provide a sweater with college logo @ KSH. 700 and a T-Shirt
		@ KSH. 600. Tracksuit is also available @ KSH. 1500 for those who wish to buy from us.
	</p>
	<h4 style="font-weight: bolder;">ACADEMIC REQUIREMENTS:</h4>
	<p style="padding-top: -1.3em">
		The entry requirements for ALL Diplomas shall be C Minus and Above grade in the Kenya Certificate of Secondary
		Education (KCSE) or its equivalent (as equated by the Kenya National Examinations Council (KNEC).
		For the Certificates is D plain and above.

	</p>
	<h4 style="font-weight: bolder;">10. MEDICAL</h4>
	<p style="padding-top: -1.3em">
		Only first aid and simp le cases are treated at the college dispensary. Serious cases will be referred to government
		general hospitals. Special cases which require attention or admission to private hospitals for specialized treatment such
		as eye sight, dental, chronic diseases etc will be paid for by the students themselves. Ksh 500 /= is payable per term as
		medical fee.
	</p>
	<p>
		May I wish you safe journey to International TTC and success in your studies. Please note that the weather often
		becomes wet and cold and you may need protective clothing. 
	</p>
	<p>
		Welcome.
	</p>
	<img src="images/sign.png">
	<p style="padding-top: -1.3em">Sarah Kweya</p>
	<p>PRINCIPAL</p>
	<p>INTERNATIONAL TTC</p>

<!-- 	<div style="padding-top: 37.7em">
 -->	<div style="padding-top: 32em">

	<h3 style="text-align: center;"><u>FEE STRUCTURE FOR 2020-2021 CERTIFICATE AND DIPLOMA COURSES</u></h3>
	<table border="1px" cellspacing="0" width="100%">
		<tr>
			<td></td>
			<td>VOTE HEAD</td>
			<td>TERM ONE</td>
			<td>TERM TWO</td>
			<td>TERM THREE </td>
			<td>TERM FOUR</td>
		</tr>
		<tr>
			<td>1.</td>
			<td>FOOD & ACCOMODATION</td>
			<td>8,000</td>
			<td>8,000</td>
			<td>8,000</td>
			<td>8,000</td>
		</tr>	
		<tr>
			<td>2.</td>
			<td>TUITION</td>
			<td>12,500</td>
			<td>12,500</td>
			<td>12,500</td>
			<td>12,500</td>
		</tr>
		<tr>
			<td>3.</td>
			<td>WATER, ELECTRICITY & BUS MAINTENANCE</td>
			<td>1,500</td>
			<td>1,500</td>
			<td>1,500</td>
			<td>1,500</td>
		</tr>
		
		<tr>
			<td colspan="2"></td>
			<td>Total</td>
			<td>22,000</td>
			<td>22,000</td>
			<td>22,000</td>
		</tr>
	</table>
	<p>NOTE</p>
	<p style="padding-top: -1em">1.PAYABLE TO THE BANK BELOW (Cash will not be accepted)</p>
	<table width="100%">
		<tr>
			<td>ACCOUNT NAME: </td>
			<td>INTERNATIONAL TEACHING & TRAINING CENTRE</td>
			
		</tr>
		<tr>
			<td>Coop Bank: </td>
			<td>01136098607600</td>
			
		</tr>
		<tr>
			<td>KCB Bank</td>
			<td>1106204271</td>
			
		</tr>
		<tr>
			<td>National Bank: </td>
			<td>01020060934300</td>
			
		</tr>

	</table>
	<p>Note: Cheques must be written to: INTERNATIONAL TEACHING & TRAINING CENTRE. </p>
	<p class="final">Cash can be deposited to any branch of the above bank. </p>
	<p class="final">2.Refundable caution money of Ksh. 1000 payable once on admission.</p>
	<p class="final">3.Food and Beverage students will pay additional Ksh. 5,000, while other fashion and design students to pay Ksh. 6,500 per term for practicals.</p>
	<p class="final">4.Attachment fees will be communicated to students one term before the due date.</p>
	<p>5.EXAMINATION FEES NOT INCLUDED.</p>
	<p class="final">6. TERM 1 refers to term of intake. </p>
	<img src="images/sign.png">
	<p style="padding-top: -1.3em">Sarah Kweya</p>
	<p>CHIEF PRINCIPAL/SECRETARY B.O.M</p>
	<div style="margin-left: -2.8em;position: absolute;bottom: -3em">
		<img src="images/footer3.png">
	</div></p>
	</div>
</body>
</html>