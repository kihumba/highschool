<!DOCTYPE html>
<html>
<head>
	<title>Vocational & Technical Courses</title>
	<style type="text/css">
		.head{
			padding-left: 4em;
			color: #383092
		}
		.heading{
			color: #383092;
			text-align: center;
			font-weight: bold;
			padding-top: -1em
		}
		
		.head2{
			font-size: 1em;
			color: #383092
		}
		.check{
			height: 18px;
			width: 18px;
			padding-left: 0.5em
		}
		.final{
			padding-top: -0.8em;
		}
		.check{
			height: 18px;
			width: 18px;
			padding-left: 0.5em
			padding-top: 0.2em
		}
		fieldset {
		  border:1px solid #215BA8;
		}
		p{
			padding-top: -1em;
		}
		
	</style>
</head>
<body>
	<div>
	<div style="margin-left: -2.5em;padding-top: -2.7em">
		<img src="<?php echo base_path().'/images/header.png';  ?>">
	</div>
	<h4 style="color: red;text-align: center;padding-top: -3em;padding-bottom: 1em"><u>NON TEACHING COURSES</u></h4>
	<table style="padding-top: -1.5em">
		<tr>
			<td class="head2">Name:...............................................................................</td>
			<td class="head">Ref. No:___________________________</td>
		</tr>
		<tr>
			<td class="head2">Depertment:.....................................................................</td>
			<td class="head">Date:______________________________</td>
		</tr>
		<tr>
			<td class="head2">Courses:..........................................................................</td>
		</tr>
	
	</table>
	<h4 class="heading"><u>RE: LETTER OF ADMISSION/JOINING INSTRUCTIONS 2020/2021/u></h4 >
	<p>
		This is to inform you that you have been selected to join the International TTC. You are required to report and
		register on 7<sup>th</sup> September, 2020 and not later than 25<sup>th</sup> September, 2020 and hence commence studies.
	</p>
	<h4 style="font-weight: bolder;padding-top: -1em;">1. COURSES / PROGRAMS</h4>
	<p>Please tick the course of your choice below</p>
	<table  border="1px"  width="100%" cellspacing="0" style="padding-top: -1em">
		<tr>
			<th>COURSES</th>
			<th>CERTIFICATE</th>
			<th>DIPLOMA</th>
		</tr>
		<tr>
			<td>FASHION AND DESIGN- CLOTHING & TAILORING</td>
			<td><img src="<?php echo base_path().'/images/check.png';  ?>" class="check"></td>
			<td><img src="<?php echo base_path().'/images/check.png';  ?>" class="check"></td>
		</tr>
		<tr>
			<td>INFORMATION COMMUNICATION TECHNOLOGY (ICT)</td>
			<td><img src="<?php echo base_path().'/images/check.png';  ?>" class="check"></td>
			<td><img src="<?php echo base_path().'/images/check.png';  ?>" class="check"></td>
		</tr>
		<tr>
			<td>COMPUTER STUDIES</td>
			<td><img src="<?php echo base_path().'/images/check.png';  ?>" class="check"></td>
			<td><img src="<?php echo base_path().'/images/check.png';  ?>" class="check"></td>
		</tr>
		<tr>
			<td>FOOD AND BEVERAGES (CATERING)</td>
			<td><img src="<?php echo base_path().'/images/check.png';  ?>" class="check"></td>
			<td><img src="<?php echo base_path().'/images/check.png';  ?>" class="check"></td>
		</tr>
		<tr>
			<td>OFFICE MANAGEMENT(FRONT OFFICICE MANAGEMENT)</td>
			<td><img src="<?php echo base_path().'/images/check.png';  ?>" class="check"></td>
			<td><img src="<?php echo base_path().'/images/check.png';  ?>" class="check"></td>
		</tr>
		<tr>
			<td>SECRETARIAL STUDIES (ADMINISTRATIVE ASSISTANT)</td>
			<td><img src="<?php echo base_path().'/images/check.png';  ?>" class="check"></td>
			<td><img src="<?php echo base_path().'/images/check.png';  ?>" class="check"></td>
		</tr>
		<tr>
			<td>BUSINESS MANAGEMENT</td>
			<td><img src="<?php echo base_path().'/images/check.png';  ?>" class="check"></td>
			<td><img src="<?php echo base_path().'/images/check.png';  ?>" class="check"></td>
		</tr>
		<tr>
			<td>ACCOUNTING</td>
			<td><img src="<?php echo base_path().'/images/check.png';  ?>" class="check"></td>
			<td><img src="<?php echo base_path().'/images/check.png';  ?>" class="check"></td>
		</tr>
	</table>
	<h4 style="font-weight: bolder;padding-top: -0.8em;">2. DURATION</h4>
	<p style="padding-top: -1.3em;">
		Duration for our courses are four (4) semesters/terms for Certificate Courses and six (6) Semesters/Terms
 		For Diploma Courses.
	</p>
	<h4 style="font-weight: bolder;">3. FEES PAYMENT</h4>
	<p style="padding-top: -1.3em;">
		You will be required to pay fees on arrival as indicated in the attached fees structure. Fees should be paid to: 
	</p>
	<p style="padding-top: -0.5em;">
		<b>INTERNATIONAL TEACHING & TRAINING CENTRE</b> by mode of:
	</p>
	<p style="padding-top: -1em;">a) Bankers Cheque or</p>
	<p style="padding-top: -1em;">b) Pay in slips for cash deposited into:</p>
	<table  style="padding-top: -1em;">
		<tr>
			<td>ACCOUNT NAME: </td>
			<td>INTERNATIONAL TEACHING & TRAINING CENTRE</td>
		</tr> 
		<tr>
			<td>National Bank: </td>
			<td>01020060934300</td>
		</tr>
	</table>
	<p  style="padding-top: -1em;">Note: Cheques must be written to: INTERNATIONAL TEACHING & TRAINING CENTRE. Cash can be deposited to any branch of the above bank.
	</p>
	<p style="padding-top: -0.8em;">
		c) Personal cheques will NOT be accepted
	</p>
	<div style="margin-left: -2.8em;position: absolute;bottom: -3em;">
		<img src="<?php echo base_path().'/images/footer2.png';  ?>">
	</div>
	<h4 style="font-weight: bolder;">4. ACCOMMODATION</h4>
	<p>
		International TTC is a boarding college only, we do not have day scholars. Our fees include food, accommodation and
		tuition. Students should report with the following.	
	</p>
	<ul style="list-style: disc;padding-top: -0.8em">
		<li> 4 Inch single bed mattress (4” x 3 ft x 6ft)</li>
		<li> Heavy blanket and two blue plain bed sheets and cover</li>
		<li> Plastic trough or bucket</li>
		<li> Plate, mug and spoon</li>
		<li> Personal effects</li>
	</ul>
	<h4 style="font-weight: bolder;">5. RULES AND REGULATIONS</h4>
	<p style="padding-top: -0.8em">
		A copy of College Rules and Regulations is attached to the application form, you are required and abide by them.
	</p>
		<h4 style="font-weight: bolder;">6. GENERAL REQUIREMENTS.</h4>
	<p style="padding-top: -1.3em"> a) ALL STUDENTS</p>
	<ul style="list-style: disc;padding-top: -1.3em">
		<li> Photocopy of National Identity Card</li>
		<li> Photocopy of ALL academic certificates and testimonials</li>
		<li> Copy of Birth Certificate</li>
		<li> Original Certificate/testimonials for confirmation and return</li>
		<li> Adequate Writing materials</li>
		<li> Scientific Calculator</li>
	</ul>
	<p>
		b) FOOD - All students are served with a common college diet. Private cooking for students is not allowed within the
		college. The college diet comprises ugali, sukumawiki, meat, githeri , rice and beans. The college does not cater for
		special diet. Black tea or hot water and a quarter bread will be provided for breakfast. Hot water will also be available in the morning and break time.
	</p>
	<h4 style="font-weight: bolder;">7. CHANGE OF COURSE</h4>
	<p style="padding-top: -1.3em">
		Students wishing to change course have a maximum of twenty-eight days from the commencement date of the
		programme to apply for change of course
	</p>
	<h4 style="font-weight: bolder;">8.LOCATION AND DIRECTION TO COLLEGE</h4>
	<p style="padding-top: -1.3em">
	
		The International TTC is now situated in a new and prestigious campus between Kitengela and Isinya town next to
		Kitengela Boys High School along Nairobi-Namanga Highway. Matatu and buses to Kitengela are found at the Nairobi
		Railways station NOS.110 and Easy Coach Entrance (Rembo and Seven City Shuttle). Alight in Kitengela bus station.
		Board another Nissan to college at Kitengela Bus station (Naekana, Wamasaa or Super Coach) then alight on the way
		at teachers college stage, Hawa stage or at the next Kobil Station near Kenchic or call our numbers to be picked at
		Kitengela
	</p>
	<h4 style="font-weight: bolder;padding-top: -1.3em">9. DRESS CODE / COLLEGE UNIFORM</h4>
	<p style="padding-top: -1.3em">
		Students are required to be in full school uniform from Monday to Friday 8:00 to 4:30PM. Therefore, carry home clothing
		for personal use. The college uniform colour is Black trouser/skirts, white long sleeved shirts, a black tie, black sweater,
		sky blue track suit and white socks for ladies. Students are advised to buy black low soled shoes. Ladies using black
		trousers must buy a black blazer or coat. The college will provide a sweater with college logo @ KSH. 700 and a T-Shirt
		@ KSH. 600. Tracksuit is also available @ KSH. 1500 for those who wish to buy from us.
	</p>
	<h4 style="font-weight: bolder;">ACADEMIC REQUIREMENTS:</h4>
	<p style="padding-top: -1.3em">
		The entry requirements for ALL Diplomas shall be C Minus and Above grade in the Kenya Certificate of Secondary
		Education (KCSE) or its equivalent (as equated by the Kenya National Examinations Council (KNEC).
		For the Certificates is D plain and above.

	</p>
	<h4 style="font-weight: bolder;">10. MEDICAL</h4>
	<p style="padding-top: -1.3em">
		Only first aid and simp le cases are treated at the college dispensary. Serious cases will be referred to government
		general hospitals. Special cases which require attention or admission to private hospitals for specialized treatment such
		as eye sight, dental, chronic diseases etc will be paid for by the students themselves. Ksh 500 /= is payable per term as
		medical fee.
	</p>
	<p>
		May I wish you safe journey to International TTC and success in your studies. Please note that the weather often
		becomes wet and cold and you may need protective clothing. 
	</p>
	<p>
		Welcome.
	</p>
	<img src="<?php echo base_path().'/images/sign.png';  ?>">
	<p style="padding-top: -1.3em">Sarah Kweya</p>
	<p>PRINCIPAL</p>
	<p>INTERNATIONAL TTC</p>


	<div style="padding-top: 27em">

	<h3 style="text-align: center;"><u>FEE STRUCTURE FOR 2020-2021 CERTIFICATE AND DIPLOMA COURSES</u></h3>
	<table border="1px" cellspacing="0" width="100%">
		<tr>
			<td></td>
			<td>VOTE HEAD</td>
			<td>TERM ONE</td>
			<td>TERM TWO</td>
			<td>TERM THREE </td>
			<td>TERM FOUR</td>
		</tr>
		<tr>
			<td>1.</td>
			<td>FOOD & ACCOMODATION</td>
			<td>8,000</td>
			<td>8,000</td>
			<td>8,000</td>
			<td>8,000</td>
		</tr>	
		<tr>
			<td>2.</td>
			<td>TUITION</td>
			<td>12,500</td>
			<td>12,500</td>
			<td>12,500</td>
			<td>12,500</td>
		</tr>
		<tr>
			<td>3.</td>
			<td>WATER, ELECTRICITY & BUS MAINTENANCE</td>
			<td>1,500</td>
			<td>1,500</td>
			<td>1,500</td>
			<td>1,500</td>
		</tr>
		
		<tr>
			<td colspan="2"></td>
			<td>Total</td>
			<td>22,000</td>
			<td>22,000</td>
			<td>22,000</td>
		</tr>
	</table>
	<p>NOTE</p>
	<p style="padding-top: -1em">1.PAYABLE TO THE BANK BELOW (Cash will not be accepted)</p>
	<table width="100%">
		<tr>
			<td>ACCOUNT NAME: </td>
			<td>INTERNATIONAL TEACHING & TRAINING CENTRE</td>
			
		</tr>
		<tr>
			<td>Coop Bank: </td>
			<td>01136098607600</td>
			
		</tr>
		<tr>
			<td>KCB Bank</td>
			<td>1106204271</td>
			
		</tr>
		<tr>
			<td>National Bank: </td>
			<td>01020060934300</td>
			
		</tr>

	</table>
	<p>Note: Cheques must be written to: INTERNATIONAL TEACHING & TRAINING CENTRE. </p>
	<p class="final">Cash can be deposited to any branch of the above bank. </p>
	<p class="final">2.Refundable caution money of Ksh. 1000 payable once on admission.</p>
	<p class="final">3.Food and Beverage students will pay additional Ksh. 5,000, while other fashion and design students to pay Ksh. 6,500 per term for practicals.</p>
	<p class="final">4.Attachment fees will be communicated to students one term before the due date.</p>
	<p>5.EXAMINATION FEES NOT INCLUDED.</p>
	<p class="final">6. TERM 1 refers to term of intake. </p>
	<img src="<?php echo base_path().'/images/sign.png';  ?>">
	<p style="padding-top: -1.3em">Sarah Kweya</p>
	<p>CHIEF PRINCIPAL/SECRETARY B.O.M</p>
	<div style="margin-left: -2.8em;position: absolute;bottom: -3em">
		<img src="<?php echo base_path().'/images/footer3.png';  ?>">
	</div>
	</div>
</div>



<div style="color: #215BA8;">
	<div style="margin-left: -2.5em;padding-top: -2.9em">
		<img src="<?php echo base_path().'/images/ITTC.png';  ?>">
	</div>
	<div style="padding-top: -0.9em;">
		<p style="font-size: 14;text-decoration: underline;">SECTION ONE - CHECK DETAILS</p>
	</div>
	<div style="padding-top: -0.9em;">
		<fieldset>
		<p>Student Name:_________________________________Adm No: ______________________________
		</p>
		<p>phone No:________________________Date of Birth______________________Male<img src="<?php echo base_path().'/images/check1.png';  ?>" class="check">  Female<img src="<?php echo base_path().'/images/check1.png';  ?>" class="check"></p>
		
		<p>Nationality:______________ID.No_____________county:______________Location:_______________</p>
		<p>
		Sub-location:________________Home Address P.O Box:_______________Town:____________________
		</p>
		<p>
			Course Applied For:___________________________________________________________________
		</p>
		<p>
			The mean grade attained in the last exam:_____________________________________________
		</p>
	</fieldset>
	</div>
	
	<div style="padding-top: 0.4em;">
		<table border="1" cellspacing="0" width="100%">
			<tr>
				<th>KCPE Index Number(in full)</th>
				<th>Year</th>
				<th>KCPE Index Number(in full)</th>
				<th>Year</th>
			</tr>
			<tr>
				<td> </td>
				<td> </td>
				<td> </td>
				<td>. </td>
			</tr>
		</table>
	</div>
	<div>
		<p>
			Please attach copies of the following:
		</p>
		<ol style="list-style: decimal-leading-zero; padding-top: -0.9em">
			<li>
				Academic and professional certificates i.e form 4, Std 8
			</li>
			<li>
				National ID card (both sides) or National Passport
			</li>
			<li>
				Birth certificate and Leaving certificate
			</li>
			<li>
				Any other relevant Certicate
			</li>
			
		</ol>
	</div>
	<div>
		<div>
		<p style="font-size: 14;text-decoration: underline;">SECTION 2 - Complete Employer / Sponsor if applicable</p>
		</div>
		<div style="padding-top: -0.9em;">
		<fieldset>
			<p>
				Name:____________________________________________________________________________
			</p>
			<p>
				Address:_______________________Towm:_____________________________________________
			</p>
			<p>
				Phone NO 1:_____________________________Phone NO 2:_______________________________
			</p>
			
		</fieldset>
	</div>
	</div>
	<div>
		<div style="padding-top: 0.4em;">
		<p style="font-size: 14;text-decoration: underline;">SECTION 3 - Complete of Next of Kin</p>
		</div>
		<div style="padding-top: -0.9em;">
		<fieldset>
			<p>
				Next of Name:______________________________ RelationShip:__________________________
			</p>
			<p>
			Home Address P.O Box:___________________________Town:__________________________________ 
			</p>
			<p>
				Phone NO 1:_____________________________Phone NO 2:_______________________________
			</p>
			<p>
				Signature:____________________________Date:_______________________________________
			</p>
		</fieldset>
	</div>
	</div>
	<div>
		<h4><i>
			For Official use:
		</i>
		</h4>
		<p>
			Recieved by:_______________________Sign:___________________________Date:_______________
		</p>
		<p>
			Recommended by:_______________________Sign:________________________Date:_______________
		</p>
		<p>
			Recommedation:______________________________________________________Date:_______________
		</p>
	</div>
	<div>
		<p><b>
			ADDITIONAL INFORMATION
		<b></p>
			<p>
				How did you learn about Internation ITTC?<br>
			...............................................................................................................................................................................
		</p>
		<p>
			<b>Personal Statement(use additional paper(s) if neccesary)</b>
		</p>
		<p>
			What career goal do you hope to achieve from of your studies at International ITTC?
			...............................................................................................................................................................................<br>
			...............................................................................................................................................................................<br>
			...............................................................................................................................................................................
		</p>
	</div>
	<div>
		<fieldset>
			ATTESTATION
		</fieldset>
	</div>
	<div>
		<fieldset>
			I hearby certify that the information given is correct o the best of my knowledgr and hearby give my perission to the admissions office to obtain any verification deemed neccesary to process my application. False information may lead to dismisal if admitted and subsequent prosecution where necessary.
		</fieldset>
	</div>
	<div style="padding-top: 0.5em">
		<p>
		Signature:.................................................................... Date:......................................................
		</p>
	</div>
	<div>
		<h6 style="font-weight: bolder;">
			SIGN YOUR APPLICATION FORM BEFORE REETURNING IT TO INTERNATIONAL ITTC
		</h6>
	</div>
	<p>
		<b>CHECKLIST</b>
	</p>
	<ol style="list-style: decimal-leading-zero; padding-top: -0.9em">
			<li>
				Non- refundable application/admission(Ksh. 1000/=. No Cash payment, a deposit slip indicating the amount to be presented to the admission office)
			</li>
			<li>
				Dully filled application form.
			</li>
			<li>
				Copies of all transcrips, Diplomas and Certificate.
			</li>
			<li>
				Two recent passport size photo
			</li>
			<li>
				Copy of ID/Passport
			</li>
	</ol>
	<div>
		<p>
			<b>
				All Payments to be made to:
			</b>
	</p>
	</div>
	<div>
		<table width="100%">
			<tr>
				<td>Account Name:</td>
				<td>International Teaching & Training Center</td>
			</tr>
			<tr>
				<td> Bank:</td>
				<td>Cooperative Bank of Kenya</td>
			</tr>
			<tr>
				<td>Account Name:</td>
				<td>01136098607600</td>
			</tr>
			<tr>
				<td>Branch:</td>
				<td> Athi-River OR</td>
			</tr>
			<tr>
				<td>Account Name:</td>
				<td>International Teaching & Training Center</td>
			</tr>
			<tr>
				<td> Bank:</td>
				<td>National Bank of Kenya</td>
			</tr>
			<tr>
				<td>Account Name:</td>
				<td>01020060934300</td>
			</tr>
			<tr>
				<td>Branch:</td>
				<td> harambee Avenue</td>
			</tr>
			
		</table>
		<p>
			For more information visit us at our Email:infor@ittc.ac.ke or www.ittc.ac.ke
		</p>
	</div>
	<div>
		<p>
			signed:......................................................................  Date:..........................................................................
		</p>
		<p>
			Name: ........................................................................ Application No: .....................................................
		</p>
	</div>
	<div>
		<p>
			For official use only
		</p>
		<p>
			Admitted or Not Admitted.............................................................................................................................
		</p>
		<p>
			Admission No:.....................................................................
		</p>
	</div>
	<div>
			
		<div style="margin-left: -2.5em;padding-top: -4em">
			<img src="<?php echo base_path().'/images/ittc2.png';  ?>">
		</div>
	</div>
	<div>
		<p>
			NOTE: Application for entrynto the Institution MUST get this form completed a REGISTERED DOCTOR at a clinic or Hospital.
		</p>
		<p>
			Name of the Student: .....................................................................................................................................
		</p>
	</div>
	<div>
		<table border="1px" width="100%" cellspacing="0">
			
			
			<tr>
				<td width="10%" height="5%">1.</td>
				<td>Eye and Vision Unaided Right - Left Aided Right - Left Colour Blind visual field</td>
				<td width="15%"></td>

			</tr>
			<tr>
				<td height="5%">2.</td>
				<td> Nose and Throat is Nasal Breathing habit? Adenoids</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">3.</td>
				<td>Ear:Hearing Voice - Right - Left</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">4.</td>
				<td>Mouth and Teeth</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">5.</td>
				<td>Glands in the neck</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">6.</td>
				<td>Chest, Heart, Lungs</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">7.</td>
				<td>Spinal column</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">8.</td>
				<td>Urine, Stool</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">9.</td>
				<td>Sleen</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">10.</td>
				<td>Ant other weakness, defects or disease defects Or Spai, Venereal, or Rheumatic tendeny</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">11.</td>
				<td> General observations:if care is desirable in any special Direction. Please give particulars</td>
				<td></td>

			</tr>
			

		</table>
	</div>
	<div style="padding-top: 2em">
	
		<p style="padding-top: 4em">
			NAME AND RUBBER STAMP OF REGISTERED DOCTOR..........................................................
		</p>
	</div>
	<div>
		<p style="padding-top: 2em">
			Address:.................................................... Town:.......................................................
		</p>
		<p style="padding-top: 2em">
		 Date:.........................................................Signature.....................................................
		</p>
	</div>
	<div>

		<div style="margin-left: -2.5em;padding-top: -4em">
			<img src="<?php echo base_path().'/images/ittc3.png';  ?>">
		</div>
	</div>
	<div>
		<p>
			I (NAME)MISS/MRS/MR/:....................................................................................................
		</p>
		<p>
			I.D. NO:........................................................................Do hear by solemnly declare that:
		</p>
	</div>
	<div>
		<ol style="list-style: decimal; padding-top: -0.9em">
			<li>
				I am the authenticated candidate selected by International TEACHERS TRANING CENTRE.
			</li>
			<li>
				The certificates i have presented are genuine and belong to me.
			</li>
			<li>
				I will abide by the college rules and regulations
			</li>
			<li>
				I shall be in college at all required time
			</li>
			<li>
				I will give all my time exclusively to the requirements of my course study.
			</li>
			<li>
				I will endeavor to protect a good image of the college.
			</li>
			<li>
				I will live and display a mature life worth of a teacher.
			</li>
			<li>
				I will live to the exepectation of the college property and handle it with care.
			</li>
			<li>
				I will honor the payments of all the college dues.
			</li>
			<li>
				I will accept the diet provided by the college and if not i will make my own arrangements for the same.
			</li>
			<li>
				I will effectively and actively take part in P.E oriented activities and all co-curricular activitities unless on medical grounds.
						
				<ol style="list-style: lower-alpha;">
					<li>
						I shall not absent myself from college for seven consecutive days and a total of 15 days in a term and or a total of 21 days in an academic year without permission. If i do so. I shall be considered to have withdrawn from college.
					</li>
					<li>
						I will not miss class/lesson attendance totaling to 25% in an academic year in the interpretion of this rule.
					</li>
				</ol>
				
			</li>
			<li>
				Furthermore, I understand that if i get in involved in circumstances such as ones listed here below, will be suspended fronm the college.
				<ul style="list-style: disc;">
					<li>
						Gross insubordination / failure to obey my superiors
					</li>
					<li>
						Drug taking or drug trafficking, taking alcohol in the college or being found in a drunk state.
					</li>
					<li>
						Impersonation or forging  a certificate or being in possession of a forged document
					</li>
					<li>
						Fighting, cause fighting or bullying others.
					</li>
					<li>
						Promoting or taking parting in strike
					</li>
					<li>
						To be convicted of a criminal offence while undergoing training
					</li>
					<li>
						Failure or refusal to take part in college activities
					</li>
					<li>
						Minor ofences that sufficiently aggravate to warrant suspension
					</li>
				</ul>
			</li>
			
			
		</ol>
		
	</div>
	<div>
		<p>
			HAVING READ AND UNDERSTOOD THE ABOVE CONDITIONS FOR THE ADMISSION INTO THE COLLEGE, I DO ACCEPT THE OFFER AND AGREE THAT DISCIPLINARY ACTIONS BE TAKEN AGANIST ME IF I BREAK ANY OF THE STIPULATED COLLEGE RULES AND REGULATIONS
		</p>
	</div>
	<div>
		<p style="padding-top: 1em">
			NAME (student):.............................................. SIGNATURE:......................................................
		</p>
		<p style="padding-top: 1em">
			NAME (guardian):.............................................. SIGNATURE:......................................................
		</p>
		<p style="padding-top: 1em">
			VERIFYING OFFICER:.............................................. DATE:.........................................................
		</p>
	
	</div>

</div>
</body>
</html>