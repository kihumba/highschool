<!DOCTYPE html>
<html>
<head>
	<title>ITTC LETTER</title>
	<style type="text/css">
		.check{
			height: 18px;
			width: 18px;
			padding-left: 0.5em
			padding-top: 0.2em
		}
		fieldset {
		  border:1px solid #215BA8;
		}
		p{
			padding-top: -1em;
		}
		.head{
			padding-left: 4em;
		}
		.heading{
			text-align: center;
			font-weight: bold;
			padding-top: -1em
		}
		.heading2{
			text-align: center;
			font-weight: bold;
			padding-top: -1em;
		}
		.headDip{
			text-align: left;
			font-weight: bold;
			margin-left: 4em;
			background-color: #E6E7E9;
		}
		.reg{
			padding-top: -1em;
		}
		.regTop{
			padding-top: -2em;
		}
		.staffs{
			padding-top: -1em;
			padding-left: 2em 
		}
		.staffsTop{
			padding-top: -0.9em
		}
	</style>
</head>
<body>
	<div>

		<div style="margin-left: -2.5em;padding-top: -2.7em">
			<img src="<?php echo base_path().'/images/header.png';  ?>">
		</div>
		<table style="padding-top: -2.5em">
			<tr>
				<td>TO:___________________________________</td>
				<td class="head">Ref. No:__________________________</td>
			</tr>
			<tr>
				<td>______________________________________</td>
				<td class="head">Date:____________________________</td>
			</tr>
			<tr>
				<td>______________________________________</td>
			</tr>
			<tr>
				<td>
					Dear Applicant,
				</td>
			</tr>
		</table>
		<h4 class="heading"><u>RE: ADMISSION TO TEACHERS’ TRAINING COURSE.</u></h4 >
		<h4 class="heading2"><u>A LETTER OF INVITATION/JOINING INSTRUCTIONS 2020/2021</u></h4 >
		<p>
			This is to inform you that you have been selected to join the International TTC to pursue a Teachers’ Training Course leading to Kenya National Examination Council Teachers’ qualifications in.
		</p>
		<h4 class="headDip" style="padding-top: -0.9em">
		    <img src="<?php echo base_path().'/images/check.png';  ?>" style="height: 14px;width: 14px;padding-right: 0.5em">
		    DIPLOMA IN PRIMARY TEACHERS’ EDUCATION (D.P.T.E) - (3 YEARS)
		</h4>
		<h4 class="headDip"><img src="<?php echo base_path().'/images/check.png';  ?>" style="height: 14px;width: 14px;padding-right: 0.5em">DIPLOMA IN EARLY CHILHOOD EDUCATION - (3 YEARS)</h4>
		<h4 class="headDip"><img src="<?php echo base_path().'/images/check.png';  ?>" style="height: 14px;width: 14px;padding-right: 0.5em">DIPLOMA IN TEACHER EDUCATION (D.T.E) - (3 YEARS)</h4>
		<p style="padding-top: -0.9em">
			This is in line with the Government of Kenya through the Ministry of Education (MOE) initiative to have all form four leavers placed in college for career training and eventual employment by the teachers service commission (TSC), County Government or Private Sector
		</p>
		<p>
			You are expected to report for registration and Classes on 7<sup>th</sup> September 2020 and not later than 25<sup>th</sup> September 2020 during the day before 5:30 p.m.
		</p>
		<h4 style="padding-top: 0.1em;font-weight: bolder;"><u>REGISTRATION / ADMISSION REQUIREMENT</u></h4>
		<h4 style="padding-top: -1.5em">Please report for registration and classes with the following:</h4>
		<div >
			<p class="regTop">1. This letter</p>
			<p class="reg">2. Copies of your o-level KCSE certificate or Result slip</p>
			<p class="reg">3. Copies of your National ID or ID waiting card, </p>
			<p class="reg">4. Copy of your birth certificate.</p>
			<p class="reg">5. 2 passports size photographs.</p>
			<p class="reg">6. Filled admission form attached.</p>
			<p class="reg">7. Kshs. 1000/= Admission Fee. (bank slip)</p>
		</div>
		<h4 style="font-weight: bolder;"><u>LOCATION AND DIRECTION TO COLLEGE:</u></h4>
		<p style="padding-top: -1em">
			The International TTC is now situated in a new and prestigious campus between Kitengela and Isinya town next to Kitengela Boys High School along Nairobi-Namanga Highway. Matatu and buses to Kitengela are found at the Nairobi Railways station NOS.110 and Easy Coach Entrance (Rembo and Seven City Shuttle). Alight in Kitengela bus station. Board another Nissan to college at Kitengela Bus station (Naekana, Wamasaa or Super Coach) then alight on the wayat teachers college stage, Hawa stage or at the next Kobil Station near Kenchic or call our numbers to be picked at Kitengela.
		</p>
		<div style="margin-left: -2.8em;position: absolute;bottom: -3em;">
			<img src="<?php echo base_path().'/images/footer2.png';  ?>">
		</div>
		<h4 style="font-weight: bolder;"><u>ACADEMIC REQUIREMENTS:</u></h4>
		<p style="padding-top: -1.3em">
			The entry requirements for ALL Diplomas in Teacher Education shall be C Plain grade in the Kenya Certificate of
			Secondary Education (KCSE) or its equivalent (as equated by the Kenya National Examinations Council (KNEC). For
			the Diploma in Secondary Teacher Education (DSTE) the candidates shall also be required to have attained a C+
			Plus grade in KCSE or its equivalent in the teaching subjects. Certificate in Early Childhood Education is D+ and
			above.*The Special Needs Candidates (SNE) could be admitted with C Minus (-) grade in KCSE or equivalent for the
			respective Diplomas. This refers to Candidates who have Special Needs – themselves).  
		</p>

		<h4 style="font-weight: bolder;"><u>REQUIREMENT FOR TUITION (WHAT YOU NEED WHEN REPORTING) </u></h4>
		<h5 style="font-weight: bolder;padding-top: -1em">1.BOARDING</h5>
			<p style="padding-top: -1.3em">
				This is a boarding college and provides full boarding facilities. The college will provide a bed. Students should report
				with a 4” single bed mattress(4”x3ft x6ft), a plate, a cup, a heavy blanket, two blue plain bed sheets and cover, towel, a
				spoon, bucket /basin, sugar and cocoa. College canteen and shop is within the college for students to buy supplement food.
			</p>
			<p>
				FOOD - All students are served with a common college diet. Private cooking for students is not allowed within the
				college. The college diet comprises ugali, sukumawiki, meat, githeri , rice and beans. The college does not cater for
				special diet. Black tea or hot water and a quarter bread will be provided for breakfast. Hot water will also be available in
				the morning and break time. 
			</p>
		<h5 style="font-weight: bolder;">2.DRESS CODE / COLLEGE UNIFORM</h5>
			<p style="padding-top: -1.3em">
				Students are required to be in full school uniform from Monday to Friday 8:00 to 4:30PM. Therefore carry home clothing
				for personal use. The college uniform colour is Black trouser/skirts, white long sleeved shirts, a black tie, black sweater,
				sky blue track suit and white socks for ladies. Students are advised to buy black low soled shoes. Ladies using black
				trousers must buy a black blazer or coat. The college will provide a sweater with college logo @ KSH. 600 and a
				T-Shirt @ KSH. 400. Tracksuit is also available @ KSH.1,500 for those who wish to buy from us. 
			</p>
		<h5 style="font-weight: bolder;">3.MEDICAL</h5>
			<p style="padding-top: -1.3em">
				Only first aid and simple cases are treated at the college dispensary. Serious cases will be referred to government
				general hospitals. Special cases which require attention or admission to private hospitals for specialized treatment such
				as eye sight, dental, chronic diseases etc will be paid for by the students themselves. Ksh 500 /= is payable per term as
				medical fee.
			</p>
		<h5 style="font-weight: bolder;">4.WRITING AND SPORTS MATERIALS</h5>
			<p class ="staffsTop">Buy for use:</p>
			<p class ="staffs">1. 1 ream of foolscaps</p>
			<p class ="staffs">2. 12 A4 exercise books 200 pages</p>
			<p class ="staffs">3. 1 Graph book</p>
			<p class ="staffs">4. Geometrical set</p>
			<p class ="staffs">5. Pencils and pens</p>
			<p class ="staffs">6. One ream of photocopying papers</p>
			<p class ="staffs">7. 2 blue manila papers</p>
			<p class ="staffs">8. Ruler 30cm</p>
			<p class ="staffs">9. Scientific calculator</p>
			<p class ="staffs">10. Marker pen (Black, blue, Red)</p>
			<p class ="staffs">11. Hockey sticks or Kshs. 2500</p>

		<h5 style="font-weight: bolder;">5.BOOK LEVY (OPTIONAL)</h5>
		<p style="padding-top: -1.3em">
			To avoid carrying or buying many books, the college advices parents to pay kshs. 3000 as book levy. Your son or
			daughter will have access to books he or she requires for the entire time of his/ her study at International ttc from our library
		</p>

		<h5 style="font-weight: bolder;">6.TEACHING PRACTICE</h5>
		<p style="padding-top: -1.3em">
			Teaching Practice Fees will be issued to students one term before the due da
		</p>
		<p><i>
			COLLEGE FEES (FEE STRUCTURE FOR 2020/2023 ACADEMIC YEAR) FEES INCLUDES FOOD, ACCOMMODATION AND TUITION.
		</i></p>
		<table border="1px" width="100%">
			<tr>
				<th>S/No.</th>
				<th>VOTE HEADS </th>
				<th>1ST TERM </th>
				<th>2ND TERM </th>
				<th>3RD TERM</th>
			</tr>
			<tr>
				<td>1</td>
				<td>Tuition & Boarding </td>
				<td>22,500.00</td>
				<td>22,500.00</td>
				<td>22,500.00</td>
			</tr>
		</table>
		<p>OTHER CHARGES: Water, Electricity and Bus Maintanance Ksh.1,500.00 per term</p>
		<h5>PAYABLE TO THE BANK BELOW (Cash will not be accepted)</h5>
		<table width="100%" style="padding-top: -1.3em;">
			<tr>
				<td>ACCOUNT NAME: </td>
				<td style="text-align: left;">INTERNATIONAL TEACHING & TRAINING CENTRE</td>
			</tr>
			<tr>
				<td>Coop Bank : </td>
				<th style="text-align: left;">01136098607600</th>	
			</tr>
			<tr>
				<td >KCB Bank : </td>
				<th style="text-align: left;"> 1106204271</th>	
			</tr>
		</table>
		<p style="padding-top: -1em;">Note: Cheques must be written to: INTERNATIONAL TEACHING & TRAINING CENTRE. Cash can be deposited to
	any branch of the above bank. </p>
	<p style="font-size:0.9em">
		LOOKING FOR A TEACHERS COURSE IN KENYA…..? LOOK NO FURTHER THAN INTERNATIONAL T.T.C:
	</p>
	<ul style="list-style: disc;">
		<li> At International TTC, we guarantee employment for all our students by recommending them to the
	 		respective employers after completion of their courses as they wait to be employed by Teachers
	 		Service Commission (TSC). We have assisted a number of our students to get teaching jobs locally
	 		especially in Nairobi as well as in South Sudan, Tanzania, Somalia
	 	</li>
		 <li>
		 	 We offer free Computer Training to all our students, and assist those who want to impove on
		 	their KCSE mean grade to resit the KCSE examinations in any suject they want to improve on
		 </li>
		 <li>
		 	 Our students sit for the Kenya National Examination Council (K.N.E.C) for the award of Diploma in Teaching
	 		Certificate just like other students in the Public and Private College, and are approved and registered
	 		by the Teachers Service Commission (T.S.C) of Kenya on Completion of their courses. Teachers
	 		trained from our college enjoy several privileges on employment.
		 </li>
		<li>
			 We guarantee our Teacher Trainee of completing their Course even after when the student sponsor
	 			or parent cannot afford to pay fees due to job loss.
		</li>
	</ul>
	<h4 style="padding-top: 1em;font-weight: bolder;"><u>RECOGNITION, REGISTRATION AND BRIEF HISTORY OF THIS COLLEGE</u></h4>
	<p style="padding-top: -1.3em;">
		Established in 2001, the college is fully registered by the Ministry of Education as per the education ACT CAP 211
		PART IV Registration Number: P/TC950/13 to offer teachers training courses to 1600 students per year. We admit only
		Kenyans and offer the Kenya National Examination Council (KNEC). The College is a leading Sporting teachers’
		training college in Kenya. Having be champions in Men and women Soccer at the Kenya Teachers Colleges National
		Championship for many years.We offer up to 50% sponsorship to those students who have showcased their talent in
		football, hockey, volleyball, handball, basketball and netball at County or National level in sports during their time in
		high school or after.
	</p>
	<h4 style="padding-top: 1em;font-weight: bolder;"><u>EDUCATION</u></h4>
	<p style="padding-top: -1.3em;">
		Teaching in 21st Century is an existing challenging career. Do you think you are special enough to change children’s
		life? Do you enjoy prospects of studying for a teachers' course, diploma which will take you into Primary schools or
		Secondary classrooms as well as furthering your knowledge and understanding of a specialist subjects? Do you look
		forward to the challenge of working with primary and secondary age children?
	</p>
	<p style="padding-top: -1.3em;">
		Are you interested in studying in one of the most popular and prestigious Kenya Teachers Training Course Diploma?
		Can you communicate well?
	</p>
	<p>
		If the answer to those questions is a clear committed ‘Yes’ then you will enjoy the opportunity we offer at International
		TTC for you to follow a Diploma route into teaching & furthering your Education in any University in Kenya and abroad
		for a degree.
	</p>
	<p>
		NOTE: (I)
	</p>
	<ul style="list-style: disc;">
		<li>
			You are requested to read through these instructions carefully in order to understand what you are expected to do.
		</li>
		<li>
			All the above requirements will be well checked on reporting date.
		</li>
		<li>
			You are advised to bring with those habits that are expected for a good future leader. All other practices/habits must be dumped before you come.
		</li>
		<li> 
			Finally we expect you to be responsible, hard working, respectful, co-operative and self disciplined.
		</li>
	</ul>
	<p>
		NOTE (II)
	</p>
	<p>
		This letter guarantees you admision to International TTC, start preparing to report as soon as you receive this letter by
		buying the requirements listed in this letter and report on the dates listed above. 
	</p>
	<p>We look forward to seeing you at the college and wish you God’s blessings as you prepare to come. </p>
	<p> Thank you in advance for choosing International TTC - Nairobi for your Teachers' Training Course.</p>
	<p>Yours sincerely,</p>
	<img src="<?php echo base_path().'/images/sign.png';  ?>">
	<p style="padding-top: -1.3em">Sarah Kweya</p>
	<p>PRINCIPAL</p>
	<p>INTERNATIONAL TTC</p>
	<div style="margin-left: -2.8em;position: absolute;bottom: -3em">
		<img src="<?php echo base_path().'/images/footer.png';  ?>">
	</div>

</div>


<div style="color: #215BA8;">
	<div style="margin-left: -2.5em;padding-top: -2.9em">
		<img src="<?php echo base_path().'/images/ITTC.png';  ?>">
	</div>
	<div style="padding-top: -0.9em;">
		<p style="font-size: 14;text-decoration: underline;">SECTION ONE - CHECK DETAILS</p>
	</div>
	<div style="padding-top: -0.9em;">
		<fieldset>
		<p>Student Name:_________________________________Adm No: ______________________________
		</p>
		<p>phone No:________________________Date of Birth______________________Male<img src="<?php echo base_path().'/images/check1.png';  ?>" class="check">  Female<img src="<?php echo base_path().'/images/check1.png';  ?>" class="check"></p>
		<p>Nationality:______________ID.No_____________county:______________Location:_______________</p>
		<p>
		Sub-location:________________Home Address P.O Box:_______________Town:____________________
		</p>
		<p>
			Course Applied For:___________________________________________________________________
		</p>
		<p>
			The mean grade attained in the last exam:_____________________________________________
		</p>
	</fieldset>
	</div>
	
	<div style="padding-top: 0.4em;">
		<table border="1" cellspacing="0" width="100%">
			<tr>
				<th>KCPE Index Number(in full)</th>
				<th>Year</th>
				<th>KCPE Index Number(in full)</th>
				<th>Year</th>
			</tr>
			<tr>
				<td> </td>
				<td> </td>
				<td> </td>
				<td>. </td>
			</tr>
		</table>
	</div>
	<div>
		<p>
			Please attach copies of the following:
		</p>
		<ol style="list-style: decimal-leading-zero; padding-top: -0.9em">
			<li>
				Academic and professional certificates i.e form 4, Std 8
			</li>
			<li>
				National ID card (both sides) or National Passport
			</li>
			<li>
				Birth certificate and Leaving certificate
			</li>
			<li>
				Any other relevant Certicate
			</li>
			
		</ol>
	</div>
	<div>
		<div>
		<p style="font-size: 14;text-decoration: underline;">SECTION 2 - Complete Employer / Sponsor if applicable</p>
		</div>
		<div style="padding-top: -0.9em;">
		<fieldset>
			<p>
				Name:____________________________________________________________________________
			</p>
			<p>
				Address:_______________________Towm:_____________________________________________
			</p>
			<p>
				Phone NO 1:_____________________________Phone NO 2:_______________________________
			</p>
			
		</fieldset>
	</div>
	</div>
	<div>
		<div style="padding-top: 0.4em;">
		<p style="font-size: 14;text-decoration: underline;">SECTION 3 - Complete of Next of Kin</p>
		</div>
		<div style="padding-top: -0.9em;">
		<fieldset>
			<p>
				Next of Name:______________________________ RelationShip:__________________________
			</p>
			<p>
			Home Address P.O Box:___________________________Town:__________________________________ 
			</p>
			<p>
				Phone NO 1:_____________________________Phone NO 2:_______________________________
			</p>
			<p>
				Signature:____________________________Date:_______________________________________
			</p>
		</fieldset>
	</div>
	</div>
	<div>
		<h4><i>
			For Official use:
		</i>
		</h4>
		<p>
			Recieved by:_______________________Sign:___________________________Date:_______________
		</p>
		<p>
			Recommended by:_______________________Sign:________________________Date:_______________
		</p>
		<p>
			Recommedation:______________________________________________________Date:_______________
		</p>
	</div>
	<div>
		<p><b>
			ADDITIONAL INFORMATION
		<b></p>
			<p>
				How did you learn about Internation ITTC?<br>
			...............................................................................................................................................................................
		</p>
		<p>
			<b>Personal Statement(use additional paper(s) if neccesary)</b>
		</p>
		<p>
			What career goal do you hope to achieve from of your studies at International ITTC?
			...............................................................................................................................................................................<br>
			...............................................................................................................................................................................<br>
			...............................................................................................................................................................................
		</p>
	</div>
	<div>
		<fieldset>
			ATTESTATION
		</fieldset>
	</div>
	<div>
		<fieldset>
			I hearby certify that the information given is correct o the best of my knowledgr and hearby give my perission to the admissions office to obtain any verification deemed neccesary to process my application. False information may lead to dismisal if admitted and subsequent prosecution where necessary.
		</fieldset>
	</div>
	<div style="padding-top: 0.5em">
		<p>
		Signature:.................................................................... Date:......................................................
		</p>
	</div>
	<div>
		<h6 style="font-weight: bolder;">
			SIGN YOUR APPLICATION FORM BEFORE REETURNING IT TO INTERNATIONAL ITTC
		</h6>
	</div>
	<p>
		<b>CHECKLIST</b>
	</p>
	<ol style="list-style: decimal-leading-zero; padding-top: -0.9em">
			<li>
				Non- refundable application/admission(Ksh. 1000/=. No Cash payment, a deposit slip indicating the amount to be presented to the admission office)
			</li>
			<li>
				Dully filled application form.
			</li>
			<li>
				Copies of all transcrips, Diplomas and Certificate.
			</li>
			<li>
				Two recent passport size photo
			</li>
			<li>
				Copy of ID/Passport
			</li>
	</ol>
	<div>
		<p>
			<b>
				All Payments to be made to:
			</b>
	</p>
	</div>
	<div>
		<table width="100%">
			<tr>
				<td>Account Name:</td>
				<td>International Teaching & Training Center</td>
			</tr>
			<tr>
				<td> Bank:</td>
				<td>Cooperative Bank of Kenya</td>
			</tr>
			<tr>
				<td>Account Name:</td>
				<td>01136098607600</td>
			</tr>
			<tr>
				<td>Branch:</td>
				<td> Athi-River OR</td>
			</tr>
			<tr>
				<td>Account Name:</td>
				<td>International Teaching & Training Center</td>
			</tr>
			<tr>
				<td> Bank:</td>
				<td>National Bank of Kenya</td>
			</tr>
			<tr>
				<td>Account Name:</td>
				<td>01020060934300</td>
			</tr>
			<tr>
				<td>Branch:</td>
				<td> harambee Avenue</td>
			</tr>
			
		</table>
		<p>
			For more information visit us at our Email:infor@ittc.ac.ke or www.ittc.ac.ke
		</p>
	</div>
	<div>
		<p>
			signed:......................................................................  Date:..........................................................................
		</p>
		<p>
			Name: ........................................................................ Application No: .....................................................
		</p>
	</div>
	<div>
		<p>
			For official use only
		</p>
		<p>
			Admitted or Not Admitted.............................................................................................................................
		</p>
		<p>
			Admission No:.....................................................................
		</p>
	</div>
	<div>
			
		<div style="margin-left: -2.5em;padding-top: -4em">
			<img src="<?php echo base_path().'/images/ittc2.png';  ?>">
		</div>
	</div>
	<div>
		<p>
			NOTE: Application for entrynto the Institution MUST get this form completed a REGISTERED DOCTOR at a clinic or Hospital.
		</p>
		<p>
			Name of the Student: .....................................................................................................................................
		</p>
	</div>
	<div>
		<table border="1px" width="100%" cellspacing="0">
			
			
			<tr>
				<td width="10%" height="5%">1.</td>
				<td>Eye and Vision Unaided Right - Left Aided Right - Left Colour Blind visual field</td>
				<td width="15%"></td>

			</tr>
			<tr>
				<td height="5%">2.</td>
				<td> Nose and Throat is Nasal Breathing habit? Adenoids</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">3.</td>
				<td>Ear:Hearing Voice - Right - Left</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">4.</td>
				<td>Mouth and Teeth</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">5.</td>
				<td>Glands in the neck</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">6.</td>
				<td>Chest, Heart, Lungs</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">7.</td>
				<td>Spinal column</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">8.</td>
				<td>Urine, Stool</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">9.</td>
				<td>Sleen</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">10.</td>
				<td>Ant other weakness, defects or disease defects Or Spai, Venereal, or Rheumatic tendeny</td>
				<td></td>

			</tr>
			<tr>
				<td height="5%">11.</td>
				<td> General observations:if care is desirable in any special Direction. Please give particulars</td>
				<td></td>

			</tr>
			

		</table>
	</div>
	<div style="padding-top: 2em">
	
		<p style="padding-top: 4em">
			NAME AND RUBBER STAMP OF REGISTERED DOCTOR..........................................................
		</p>
	</div>
	<div>
		<p style="padding-top: 2em">
			Address:.................................................... Town:.......................................................
		</p>
		<p style="padding-top: 2em">
		 Date:.........................................................Signature.....................................................
		</p>
	</div>
	<div>

		<div style="margin-left: -2.5em;padding-top: -4em">
			<img src="<?php echo base_path().'/images/ittc3.png';  ?>">
		</div>
	</div>
	<div>
		<p>
			I (NAME)MISS/MRS/MR/:....................................................................................................
		</p>
		<p>
			I.D. NO:........................................................................Do hear by solemnly declare that:
		</p>
	</div>
	<div>
		<ol style="list-style: decimal; padding-top: -0.9em">
			<li>
				I am the authenticated candidate selected by International TEACHERS TRANING CENTRE.
			</li>
			<li>
				The certificates i have presented are genuine and belong to me.
			</li>
			<li>
				I will abide by the college rules and regulations
			</li>
			<li>
				I shall be in college at all required time
			</li>
			<li>
				I will give all my time exclusively to the requirements of my course study.
			</li>
			<li>
				I will endeavor to protect a good image of the college.
			</li>
			<li>
				I will live and display a mature life worth of a teacher.
			</li>
			<li>
				I will live to the exepectation of the college property and handle it with care.
			</li>
			<li>
				I will honor the payments of all the college dues.
			</li>
			<li>
				I will accept the diet provided by the college and if not i will make my own arrangements for the same.
			</li>
			<li>
				I will effectively and actively take part in P.E oriented activities and all co-curricular activitities unless on medical grounds.
						
				<ol style="list-style: lower-alpha;">
					<li>
						I shall not absent myself from college for seven consecutive days and a total of 15 days in a term and or a total of 21 days in an academic year without permission. If i do so. I shall be considered to have withdrawn from college.
					</li>
					<li>
						I will not miss class/lesson attendance totaling to 25% in an academic year in the interpretion of this rule.
					</li>
				</ol>
				
			</li>
			<li>
				Furthermore, I understand that if i get in involved in circumstances such as ones listed here below, will be suspended fronm the college.
				<ul style="list-style: disc;">
					<li>
						Gross insubordination / failure to obey my superiors
					</li>
					<li>
						Drug taking or drug trafficking, taking alcohol in the college or being found in a drunk state.
					</li>
					<li>
						Impersonation or forging  a certificate or being in possession of a forged document
					</li>
					<li>
						Fighting, cause fighting or bullying others.
					</li>
					<li>
						Promoting or taking parting in strike
					</li>
					<li>
						To be convicted of a criminal offence while undergoing training
					</li>
					<li>
						Failure or refusal to take part in college activities
					</li>
					<li>
						Minor ofences that sufficiently aggravate to warrant suspension
					</li>
				</ul>
			</li>
			
			
		</ol>
		
	</div>
	<div>
		<p>
			HAVING READ AND UNDERSTOOD THE ABOVE CONDITIONS FOR THE ADMISSION INTO THE COLLEGE, I DO ACCEPT THE OFFER AND AGREE THAT DISCIPLINARY ACTIONS BE TAKEN AGANIST ME IF I BREAK ANY OF THE STIPULATED COLLEGE RULES AND REGULATIONS
		</p>
	</div>
	<div>
		<p style="padding-top: 1em">
			NAME (student):.............................................. SIGNATURE:......................................................
		</p>
		<p style="padding-top: 1em">
			NAME (guardian):.............................................. SIGNATURE:......................................................
		</p>
		<p style="padding-top: 1em">
			VERIFYING OFFICER:.............................................. DATE:.........................................................
		</p>
	
	</div>
</div>
</body>
</html>