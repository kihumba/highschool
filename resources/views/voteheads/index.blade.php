@include('layouts.header')
@include('layouts.sidebar')


    <!-- Main Content Area -->
     <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Voteheads</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item">Voteheads</li>
          <li class="breadcrumb-item active"><a href="#">Voteheads</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="tile">
            <div class="tile-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" id="sampleTable">
                   <thead>
                        <tr>
                            <th>#</th>
                            <th>Votehead Name</th>
                            <th>Added By</th>
                            <th> Action </th>
                        </tr>
                    </thead>

                    <tbody class="voteheads">
                        <?php $num = 1 ?>
                        @foreach($voteheads as $votehead)
                        <tr>
                            <td>{{ $num++ }}</td>
                            <td>{{ $votehead->votehead_name}}</td>
                            <td>{{ $votehead->name}}</td>
                            <td>
                                <a data-href="#" data-toggle="modal" data-target="#editmyModal{{$votehead->id}}"><i class="fa fa-edit text-primary"></i></a>
                                <a data-href="#" data-toggle="modal" data-target="#myModal{{ $votehead->id}}"><i class="fa fa-trash text-danger"></i></a>
                         
                             
                            </td>
                        </tr>
                                       <!--delete modal-->
                            <div class="modal fade" id="myModal{{ $votehead->id}}">
                                <div class="modal-dialog modal-dialog-centered modal-sm" role="delete">
                                    <div class="modal-content">
                                        <div class="modal-header bg">
                                            <h5 class="modal-title"> Comfirm Delete</h5>
                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                        </div>
                                        <div class="modal-body">

                                            <P>You are about to delete this Votehead?</P>
                                                <div class="row" style="padding-top:1em;"> 
                                                    <div class="col-md-6">
                                                    <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>
                                                </div>
                                                <div class="col-md-6">
                                                     <a class="btn btn-sm btn-danger btn-block btn-ok" href="{{route('deleteVotehead',$votehead->id)}}">Delete</a>
                                                 </div>
                                                </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                                                <!--edit Modal -->
                            <div class="modal fade" id="editmyModal{{$votehead->id}}">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg">
                                            <h5 class="modal-title"> Voteheads</h5>
                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                        </div>
                                        <div class="modal-body">
                                           
                                            <form method="POST" action="{{route('updateVotehead',$votehead->id)}}">
                                                    @csrf

                                                @if (Session::has('message'))
                                                <div class="alert alert-danger">{{ Session::get('message') }}</div>
                                                @endif
                                             

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label> Votehead Name </label>
                                                    <input type="text" value="{{$votehead->votehead_name}}" class="form-control form-control-sm" name="votehead_name" required autocomplete="votehead_name">
                                                    </div>



                                                </div>
                                                <br/>

                                                <div class="row">
                                                
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary btn-block btn-sm"> Update votehead </button>
                                                    </div>
                                                </div>

                                            </form>
                                              
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </tbody>
                </table>
            </div>
         </div>
    </div>
</div>

  <div class="card col-md-4 " >
        <div style="padding:1em;">
            <form method="POST" action="">
                @csrf

                <div class="alert alert-success kam-success" role="alert" style="display:none;">
                  Votehead added successfully!
                </div>

                <div class="alert alert-danger kam-error" role="alert" style="display:none;">
                  Votehead already exists!
                </div>
                <div class="alert alert-danger kam-error2" role="alert" style="display:none;">
                  Input Votehead Name!
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label> Votehead Name </label>
                    <input type="text" class="form-control form-control-sm" id="votehead_name" required name="votehead_name" autocomplete="votehead_name">
                    </div>
                </div>
                <br/>

                <div class="row">

                    <div class="col-md-12">
                        <button type="button" id="saveVotehead"class="btn btn-success btn-block btn-sm">Save</button>
                    </div>
                    
                </div>
            </form>
        </div>
    </div>
</div>
  


</main>

@include('layouts.footer')