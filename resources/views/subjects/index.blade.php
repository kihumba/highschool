@include('layouts.header')
@include('layouts.sidebar')


    <!-- Main Content Area -->
     <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Subjects</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item">Subjects</li>
          <li class="breadcrumb-item active"><a href="#">Subjects</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="tile">
            <div class="tile-body">
            <div class="table-responsive">
                 <table class="table table-hover table-bordered" id="sampleTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Subject Name</th>
                            <th> Added by </th>
                            <th> Created at </th>
                            <th> Action </th>
                        </tr>
                    </thead>

                    <?php $num = 1; ?>
                    <tbody class="subjects">
                        @foreach($subjects as $subject)
     
                        <tr>
                            <td>{{$num ++}}</td>
                            <td>{{ $subject->subject_name}}</td>
                            <td>{{$subject->name}}</td>
                            <td>{{$subject->created_at}}</td>
                            <td>
                                    <a data-href="#" data-toggle="modal" data-target="#editmyModal{{$subject->id}}"><i class="fa fa-edit text-primary"></i></a>
                                    &nbsp;
                                    <a data-href="#" data-toggle="modal" data-target="#myModal{{ $subject->id}}"><i class="fa fa-trash text-danger"></i></a>

                                        <!--delete modal-->
                                        <div class="modal fade" id="myModal{{ $subject->id}}">
                                            <div class="modal-dialog modal-dialog-centered modal-sm" role="delete">
                                                <div class="modal-content">
                                                    <div class="modal-header bg">
                                                        <h5 class="modal-title"> Comfirm Delete</h5>
                                                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                                    </div>
                                                    <div class="modal-body">

                                                        <P>You are about to delete this subject?</P>
                                                            <div class="row" style="padding-top:1em;"> 
                                                                <div class="col-md-6">
                                                                    <button type="button" class="btn btn-block btn-secondary btn-sm" data-dismiss="modal">Close</button>
                                                                </div>
                                                                <div class="col-md-6">
                                                            <a class="btn btn-danger btn-block  btn-sm btn-ok" href="{{route('deleteSubjects',$subject->id)}}">Delete</a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>   

                                         
                                                              <!--edit Modal -->
                                <div class="modal fade" id="editmyModal{{$subject->id}}">
                                    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header bg">
                                                <h5 class="modal-title">Edit Semesters</h5>
                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                            </div>
                                            <div class="modal-body">

                                            <form method="POST" action="{{route('editSubject',$subject->id)}}">
                                                @csrf

                                                    @if (Session::has('message'))
                                                    <div class="alert alert-danger">{{ Session::get('message') }}</div>
                                                    @endif


                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label> Subject Name </label>
                                                        <input type="text" value="{{$subject->subject_name}}" class="form-control form-control-sm" name="subject_name" required autocomplete="subject_name">
                                                        </div>



                                                    </div>
                                                    <br/>

                                                    <div class="row" style="padding-top:1em;">
                                                      
                                                        <div class="col-md-12">
                                                            <button type="submit" class="btn btn-primary btn-block btn-sm"> Update </button>
                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
         </div>
    </div>
</div>

  <div class="card col-md-4 " >
        <div style="padding:1em;">
             <form method="POST" action="{{route('saveSubject')}}">
                    @csrf

                        <div class="alert alert-success kam-success" role="alert" style="display:none;">
                          Subject added successfully!
                        </div>
                        <div class="alert alert-danger kam-error" role="alert" style="display:none;">
                          Subject already exists!
                        </div>
                        <div class="alert alert-danger kam-error2" role="alert" style="display:none;">
                          Input Subject Name!
                        </div>

                <div class="row">
                    <div class="col-md-12">
                        <label> Subject Name </label>
                    <input type="text" class="form-control form-control-sm" id="subject_name" name="subject_name" required autocomplete="subject_name">
                    </div>



                </div>
                        <br/>

                <div class="row">
                  
                    <div class="col-md-12">
                        <button type="button" id="saveSubject"class="btn btn-success btn-block btn-sm">Save</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
  


</main>

@include('layouts.footer')