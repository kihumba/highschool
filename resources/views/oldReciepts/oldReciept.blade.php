@include('layouts.header')
@include('layouts.sidebar')
    <main class="app-content">
      <div class="app-title">
        <div>
          <?php $studentName = DB::table('students')->where('id', $studentId)->pluck('student_name')->first();?>
          <h1><i class="fa fa-users"></i> Old Reciepts for <span style="color: #009688">{{ $studentName }}</span></h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><a href="route('home')"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item"><a href="{{route('students')}}">Students</a></li>
          <li class="breadcrumb-item"><a href="#">Old Reciepts</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-10 offset-md-1">
          <div class="tile">

            <form method="POST" action="{{ route('paidOldReceiptfee') }}">
                @csrf
                <div class="alert alert-success kam-success" role="alert" style="display:none;">
                  Payment Done successfully!
                </div>

                <div class="alert alert-danger kam-error2" role="alert" style="display:none;">
                  Input all fields & 0 not accepted as a number!
                </div>
                <input type="hidden" name="studentId" id="studentId" value="{{$studentId}}">
                <div class="row payment_m" style="margin-top:1em;">
                    <div class="col-md-3" style="font-weight: bold;">Payment method:</div>
                    <div class="col-md-2">
                        Bank:
                        <input type="checkbox" name="payment" class="payment_method" value="1" onclick="onlyOne(this)">
                    </div>
                    <div class="col-md-2">
                        M-Pesa:
                        <input type="checkbox" name="payment" class="payment_method" value="2" onclick="onlyOne(this)">
                    </div>
                    <div class="col-md-2">
                        Cash:
                        <input type="checkbox" name="payment" class="payment_method" value="3" onclick="onlyOne(this)">
                    </div>
                    <div class="col-md-2">
                        Other:
                        <input type="checkbox" name="payment" class="payment_method" value="4" onclick="onlyOne(this)">
                    </div>
                </div>
                <br>
                 <div class="row">
                    <?php  

                            $studentbalance = DB::table('old_receipt')
                                ->where('student_id', $studentId)
                                ->pluck('balance')
                                ->last();

                            if($studentbalance){
                                $balance = $studentbalance;

                            }else{
                                 $balance = "";
                            }

                    ?>
                
                    <div class="col-md-6">
                        
                        <label>Total balance </label>
                        <input type="text" class="form-control form-control-sm" id="totalBalance" name="totalBalance" value="{{$balance}}" required autocomplete="totalBalance">
                    
                    </div>

                    <div class="col-md-6">
                        <label> Amount paid </label>
                        <input type="text" class="form-control form-control-sm" id="amountPaid" name="amountPaid" required autocomplete="amountPaid">
                    </div>
                </div>

                <br/>

                <div class="row">
                     <div class="col-md-6">
                         <label> Semesters </label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <div data-toggle="modal" data-target="#addSemester" class="input-group-text btn-group btn-group-sm kam_add_button"> + </div>

                            </div>
                          <select class="form-control" id="semesterId" name="semesterId" required style="padding:0px;">
                                <option disabled selected>Select semester</option>
                                @foreach( $semesters as $semester )
                                    <option value="{{ $semester->id }}">
                                        {{ $semester->semester_name }} 
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                     <div class="col-md-6">
                      <label >Payment Date:</label>
                    <div >
                     <input class="form-control datetimepicker-input" id="demoDate" type="text" placeholder="Select Date">
                    </div>
                  </div>
                  </div>
                   
                <div class="row" style="padding-top:1em;">
                     <div class="col-md-6">
                       @if($student->status == 0)
                        <a href="{{route('inactiveStudents')}}" class="btn btn-secondary btn-block btn-sm"><i class="fa fa-chevron-left" style="font-size: 14px"></i>&nbsp;&nbsp;&nbsp;Back to Students</a>
                        @else
                          <?php 
                            $studentForm = DB::table('students')->where('id', $studentId)->pluck('form_id')->first();
                              if ($studentForm == 1) {
                                $formUrl ="form1";
                              }else if ($studentForm == 2){
                                $formUrl ="form2";
                              }else if ($studentForm == 3){
                              $formUrl ="form3";
                              }elseif ($studentForm == 4) {
                                  $formUrl ="form4";
                              }else{
                                $formUrl = 0;
                              }

                          ?>

                            <a href="{{route($formUrl)}}" class="btn btn-secondary btn-block btn-sm"><i class="fa fa-chevron-left" style="font-size: 14px"></i>&nbsp;&nbsp;&nbsp;Back to Students</a>
                        @endif
                    </div>
                   
                    <div class="col-md-6">
                        <button type="button" id="payclose" class="btn btn-primary btn-block btn-sm"> Pay </button>
                    </div>
                </div>
            </form>

          </div>
        </div>
    </div>
</main>
@include('layouts.footer')
              