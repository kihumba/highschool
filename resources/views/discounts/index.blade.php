@include('layouts.header')
@include('layouts.sidebar')


<!-- Main Content Area -->
 <main class="app-content">
  <div class="app-title">
    <div>
      <h1><i class="fa fa-th-list"></i> Discounted Students</h1>
    </div>
    <ul class="app-breadcrumb breadcrumb side">
      <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
      <li class="breadcrumb-item">Discounted Students</li>
      <li class="breadcrumb-item active"><a href="#">Discounted Students</a></li>
    </ul>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="tile">
        <div class="tile-body">
        <div class="table-responsive">
            <table class="table table-hover table-bordered" id="sampleTable">
              <thead>
                <tr>
                    <th>#</th>
                    <th>Student Name</th>
                    <th> Adm No </th>
                    <th> Discounted by </th>
                    <th> Description </th>
                    <th> Discounted Amount </th>
                    <th> Action </th>
                </tr>
              </thead>

              <?php $num = 1; ?>
              <tbody>
                @foreach($discounts as $discount)
                  <tr>  
                      <td>{{ $num ++ }}</td>
                      <td>{{ $discount->student_name }}</td>
                      <td>{{ $discount->admission_number }}</td>
                      <td>{{ $discount->name }}</td>
                      <td>{{ $discount->discount_description }}</td>
                      <td>{{ number_format($discount->discount_amount, 2) }}</td>
                      <td>
                        <a data-href="#" data-toggle="modal" data-target="#editDiscount{{$discount->id}}"><i class="fa fa-edit text-primary pointers" data-toggle="tooltip" data-placement="top" title="Edit discount"></i></a>

                        <a data-href="#" data-toggle="modal" data-target="#deleteDiscount{{$discount->id}}"><i class="fa fa-trash text-danger pointers" data-toggle="tooltip" data-placement="top" title="Delete discount"></i></a>
                        

                      </td>
          
                  </tr>
                    <!-- DELETE A STUDENT -->
                    <div class="modal fade" id="deleteDiscount{{$discount->id}}">
                        <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg">
                                    <h5 class="modal-title">Confirm Delete </h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                </div>
                                <div class="modal-body">
                                  <p>You are about to Delete {{$discount->discount_description}}</p>
                                   <div class="row" style="padding-top:1em;">
                                      <div class="col-md-6">
                                          <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>
                                        </div>
                                      <div class="col-md-6">
                                          <a href="{{route('deleteDiscount', $discount->id)}}" class="btn btn-danger btn-block btn-sm">Delete </a>
                                      </div>
                                  </div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--EDIT DISCOUNT modal-->
                    <div class="modal fade" id="editDiscount{{$discount->id}}">
                        <div class="modal-dialog modal-dialog-centered" role="pay">
                            <div class="modal-content">
                                <div class="modal-header bg">
                                    <h5 class="modal-title"> Discount - <small>{{$discount->discount_description}}</small> </h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                </div>
                                <div class="modal-body">

                                     <form method="POST" action="{{route('editDiscount',$discount->id)}}">
                                    @csrf
                                    <label>Description</label>
                                    <textarea class="form-control form-control-sm" name="discount_description" maxlength="50" placeholder="Maximum lenght = 50 characters" required>{{$discount->discount_description}}</textarea>

                                    <br/>
                                    <label>Amount</label>
                                    <input type="number" class="form-control form-control-sm"  value="{{$discount->discount_amount}}"  name="amount"  required>

                                    <br/>

                                    <button type="submit" class="btn btn-primary btn-block btn-sm">Discount</button>
                                </form>

                                </div>
                            </div>
                        </div>
                    </div>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@include('layouts.footer')