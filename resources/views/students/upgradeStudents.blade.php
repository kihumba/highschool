@include('layouts.header')
@include('layouts.sidebar')


    <!-- Main Content Area -->
     <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Upgrade {{$formName}} Students</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item">Upgrade Students</li>
          <li class="breadcrumb-item active"><a href="#">Upgrade Students</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-8 offset-md-2">
          <div class="tile">
            <div class="tile-body">
                 @if (Session::has('message'))
                       <div class="alert alert-danger">{{ Session::get('message') }}</div>
                @endif
              <form method="post" action="{{route('saveUpgradeStudent')}}">
                @csrf
              <input type="hidden" name="intialFormId" value="{{$formId}}">
              <div class="row">
                   <div class="col-md-6">
                    <label> Form </label>
                    <div class="input-group">
                        <select class="form-control form-control-sm" required name="formId" id="formIds" style="padding:0px;">
                            <option selected disabled>Select Form</option>
                            @foreach( $forms as $form )
                                <option value="{{ $form->id }}">
                                    {{ $form->form_name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                  </div>
                <div class="col-md-6">
                   <label> Streams </label>
                    <select id="streamId" name="streamId" required class="form-control form-control-sm"></select>
                </div>


                </div>
                <br>

              <div class="table-responsive">

                  <table class="table table-hover nowrap" style="">
              @foreach($students as $student)

              <tr>
                <td>
                  <label style="font-size: 16px; cursor: pointer;">
                  <input type="checkbox" name="students[]" value="{{ $student->id }}"> {{ $student->student_name }}
                </label>
                </td>
              </tr>

              @endforeach
            </table>
          </div>
              <div class="row" style="padding-top:1em;">
                <?php 
                    if ($formId == 1) {
                        $formUrl ="form1";
                    }else if ($formId == 2){
                        $formUrl ="form2";
                    }else if ($formId == 3){
                        $formUrl ="form3";
                    }elseif ($formId == 4) {
                            $formUrl ="form4";
                    }else{
                        $formUrl = "students";
                    }

                 ?>
                <div class="col-md-6">
                    <a href="{{route($formUrl)}}" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Back to students</a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block btn-sm"> Update Student </button>
                </div>
              </div>
            </form>
            </div>
          </div>
        </div>
      </div>
    </main>
    @include('layouts.footer')

