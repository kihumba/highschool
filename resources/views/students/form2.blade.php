@include('layouts.header')
@include('layouts.sidebar')

   <!-- Main Content Area -->
     <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Form Two Students</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item">Students</li>
          <li class="breadcrumb-item active"><a href="#">Form Two Students</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
                <a href="{{ route('addstudent', 2) }}" class="btn btn-rounded btn-primary mb-3" style="font-weight: bold; color: #fff;">+</a>
                <a href="{{ route('upgradeStudents', 2) }}" class="btn btn-rounded btn-success mb-3" style="font-weight: bold; color: #fff;"><i class="fa fa-arrow-right"></i></a>
              <a data-href="#" data-toggle="modal" data-target="#changeAllStudentsForm" class="btn btn-rounded btn-info mb-3" style="font-weight: bold; color: #fff;"><i class="fa fa-arrow-up"></i></a>
                 @if (Session::has('message'))
                       <div class="alert alert-danger">{{ Session::get('message') }}</div>
                @endif

            <div class="table-responsive">
                <table class="table table-hover table-bordered nowrap" id="sampleTable">
                    <thead>
                        <tr>
                            <th>Student name</th>
                            <th>Admission Number</th>
                            <th>Gender</th>
                            <th>K.C.P.E Grade</th>
                            <th>County</th>
                            <th>Stream</th>
                            <th>Balance</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($students as $student)
                        <tr>
                            <td>
                              <a href="{{route('studentDetails', $student->id)}}" style="text-decoration: none;">{{$student->student_name}}</a>

                              </td>
                            <td>{{$student-> admission_number}}</td>
                            <td>{{$student->gender}}</td>
                            <td>{{$student->grade}}</td>
                            <td>{{$student->county_name}}</td>
                            <td>{{$student->stream_name}}</td>
                            <td>
                                <?php
                                        
                                     $currentSemester = DB::table('semesters')
                                            ->where('current', 1)
                                            ->pluck('id')
                                            ->first();
                                            
                                    $amountPaid =  DB::table('payments')
                                        ->join('votehead_amounts', 'votehead_amounts.votehead_id', '=', 'payments.votehead_id')
                                        ->join('semesters', 'semesters.id', '=', 'votehead_amounts.semester_id')
                                        ->where('payments.student_id', $student->id)
                                        ->where('payments.semester_id', $currentSemester)
                                        ->where('semesters.current', 1)
                                        ->pluck('payments.amount')
                                        ->sum();

                                    $voteheadIds = DB::table('form_voteheads')
                                        ->where('form_id', $student->form_id)
                                        ->pluck('votehead_amount_id')
                                        ->toArray();

                                    $totalAmount = DB::table('votehead_amounts')
                                        ->join('semesters', 'semesters.id', '=', 'votehead_amounts.semester_id')
                                        ->where('semesters.current', 1)
                                        ->whereIn('votehead_amounts.id', $voteheadIds)
                                        ->pluck('votehead_amounts.amount')
                                        ->sum();


                                    $balance = $totalAmount - $amountPaid;
                                    $addedFeeReceipt = DB::table('added_fee')
                                        ->where('student_id', $student->id)
                                        ->where('semester_id', $currentSemester)
                                        ->pluck('receipt_number')
                                        ->last();
                                        if($addedFeeReceipt == Null){
                                            $addbalance = DB::table('added_fee')
                                                ->where('student_id', $student->id)
                                                ->where('semester_id', $currentSemester)
                                                ->pluck('amount')
                                                ->first();
                                            $totalBalances = $balance + $addbalance;
                                        }else{
                                            $addbalance = DB::table('added_fee')
                                                ->where('student_id', $student->id)
                                                ->where('receipt_number', $addedFeeReceipt)
                                                ->where('semester_id', $currentSemester)
                                                ->pluck('balance')
                                                ->last();
                                            $totalBalances = $balance + $addbalance;
                                        }
                                        $discount = DB::table('discount_fees')
                                            ->where('semester_id', $currentSemester)
                                            ->where('student_id', $student->id)
                                            ->pluck('discount_amount')
                                            ->first();
                                            if($discount){
                                                $totalBalance = $totalBalances - $discount;
                                            }else{
                                                $totalBalance = $totalBalances;
                                            }
                                        $balanceBroughtForward = DB::table('balance_brought_forward')
                                                ->where('student_id', $student->id)
                                                ->pluck('balance')
                                                ->last();
                              

                                 ?>
                                  <a href="{{route('studentReport', $student->id)}}" style="text-decoration: none;"> {{ number_format($totalBalance + $balanceBroughtForward, 2) }}</a>
                                
                              

                            </td>
                             <td>
                                <a href="{{route('feePayment', $student->id)}}"><i class="fa fa-money text-primary" data-toggle="tooltip" data-placement="top" title="Fee payments"></i></a>&nbsp;&nbsp;

                                <a href="{{route('oldReciepts', $student->id)}}"><i class="fa fa-money text-dark" data-toggle="tooltip" data-placement="top" title="Old receipts"></i></a>&nbsp;&nbsp;

                                  <a data-href="#" data-toggle="modal" data-target="#discount{{$student->id}}"><i class="fa fa-percent text-success" data-toggle="tooltip" data-placement="top" title="Discount"></i></a>&nbsp;&nbsp;

                                 <a data-href="#" data-toggle="modal" data-target="#addFee{{$student->id}}"><i class="fa fa-dollar text-warning pointers" data-toggle="tooltip" data-placement="top" title="Add fee"></i></a>&nbsp;&nbsp;

                                <a href="{{route('editStudent', $student->id)}}"><i class="fa fa-edit text-primary" data-toggle="tooltip" data-placement="top" title="Edit Student"></i></a>&nbsp;&nbsp;

                                <a data-href="#" data-toggle="modal" data-target="#changeForm{{$student->id}}"><i class="fa fa-arrow-right text-success pointers" data-toggle="tooltip" data-placement="top" title="upgrade or Degrade student"></i></a>&nbsp;&nbsp;

                                <a data-href="#" data-toggle="modal" data-target="#deleteStudent{{$student->id}}"><i class="fa fa-trash text-danger pointers" data-toggle="tooltip" data-placement="top" title="Delete student"></i></a>
                              </td>
                           
                        </tr>
                         <!-- DELETE A STUDENT -->
                          <div class="modal fade" id="deleteStudent{{$student->id}}">
                              <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                                  <div class="modal-content">
                                      <div class="modal-header bg">
                                          <h5 class="modal-title">Confirm Delete </h5>
                                          <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                      </div>
                                      <div class="modal-body">
                                        <p>You are about to Delete {{$student->student_name}}</p>
                                         <div class="row" style="padding-top:1em;">
                                            <div class="col-md-6">
                                                <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>
                                              </div>
                                            <div class="col-md-6">
                                                <a href="{{route('deleteStudent', $student->id)}}" class="btn btn-danger btn-block btn-sm">Delete </a>
                                            </div>
                                        </div>
                                         
                                      </div>
                                  </div>
                              </div>
                          </div>
                              <!--STUDENT upgrade/Degrade -->
                          <div class="modal fade" id="changeForm{{$student->id}}">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                  <div class="modal-content">
                                      <div class="modal-header bg">
                                          <h5 class="modal-title">Confirm Upgrade or Degrade </h5>
                                          <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                      </div>
                                      <div class="modal-body">
                                        <p>You are about to Upgrade or Degrade {{$student->student_name}}</p>
                                        <form method="post" action="{{route('changeForm', $student->id)}}">
                                          @csrf
                                        <div class="row">
                                           <div class="col-md-6">
                                            <label> Form </label>
                                            <div class="input-group">
                                                <select class="form-control form-control-sm formId" name="formId" style="padding:0px;">
                                                    <option selected disabled>Select Form</option>
                                                    @foreach( $forms as $form )
                                                        <option value="{{ $form->id }}">
                                                            {{ $form->form_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                           <label> Streams </label>
                                            <select name="streamId" class="form-control form-control-sm streams"></select>
                                        </div>
                      
                        
                                        </div>
                                         <div class="row" style="padding-top:1em;">
                                            <div class="col-md-6">
                                                <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>
                                              </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-success btn-block btn-sm">Upgrade or Degrade </button>
                                            </div>
                                        </div>
                                      </form>
                                         
                                      </div>
                                  </div>
                              </div>
                          </div>
                            <!--ADD FEE Modal -->
                           <div class="modal fade" id="addFee{{$student->id}}">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg">
                                            <h5 class="modal-title"> Add fee to <small>{{$student->student_name}}</small></h5>
                                            <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                        </div>
                                        <div class="modal-body">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <form method="POST" action="{{route('addStudentFee',$student->id)}}">
                                                    @csrf
                                                    <input type="hidden" name="student_id" value="{{$student->id}}">
                                                    <label>Description</label>
                                                    <textarea class="form-control form-control-sm" name="fee_description" maxlength="50" placeholder="Maximum lenght = 50 characters" required></textarea>

                                                    <br/>

                                                    <label>Amount</label>
                                                    <input type="number" class="form-control form-control-sm" name="amount" maxlength="2" required>

                                                    <br/>

                                                    <button type="submit" class="btn btn-primary btn-block btn-sm">Add</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <!--GIVE DISCOUNT modal-->
                        <div class="modal fade" id="discount{{$student->id}}">
                            <div class="modal-dialog modal-dialog-centered" role="pay">
                                <div class="modal-content">
                                    <div class="modal-header bg">
                                        <h5 class="modal-title"> Discount - <small>{{$student->student_name}}</small> </h5>
                                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                    </div>
                                    <div class="modal-body">

                                         <form method="POST" action="{{route('discount',$student->id)}}">
                                        @csrf
                                        <input type="hidden" name="student_id" value="{{$student->id}}">
                                        <label>Description</label>
                                        <textarea class="form-control form-control-sm" name="discount_description" maxlength="50" placeholder="Maximum lenght = 50 characters" required></textarea>

                                        <br/>
                                        <label>Amount</label>
                                        <input type="number" class="form-control form-control-sm" value="" name="amount"  required>

                                        <br/>

                                        <button type="submit" class="btn btn-primary btn-block btn-sm">Discount</button>
                                    </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                   </table>
            </div>
         </div>
    </div>
</div>
</div>
<!--STUDENT upgrade/Degrade -->
    <div class="modal fade" id="changeAllStudentsForm">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg">
                    <h5 class="modal-title">Confirm Upgrade or Degrade </h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                  <p>You are about to Upgrade or Degrade</p>
                  <form method="post" action="{{route('changeAllStudentsForm', 2)}}">
                    @csrf
                  <div class="row">
                     <div class="col-md-6">
                      <label> Form </label>
                      <div class="input-group">
                          <select class="form-control form-control-sm" name="formId" id="formIds" style="padding:0px;">
                              <option selected disabled>Select Form</option>
                              @foreach( $forms as $form )
                                  <option value="{{ $form->id }}">
                                      {{ $form->form_name }}
                                  </option>
                              @endforeach
                          </select>
                      </div>
                  </div>
                  <div class="col-md-6">
                     <label> Streams </label>
                      <select id="streamId" name="streamId" class="form-control form-control-sm"></select>
                  </div>

  
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-12">
                     <label> Number of students in selected Stream </label>
                    <input type="number" name="limit" required class="form-control form-control-sm">
                  </div>
                  </div>
                   <div class="row" style="padding-top:1em;">
                      <div class="col-md-6">
                          <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>
                        </div>
                      <div class="col-md-6">
                          <button type="submit" class="btn btn-success btn-block btn-sm">Upgrade or Degrade </button>
                      </div>
                  </div>
                </form>
                   
                </div>
            </div>
        </div>
    </div>

     
</main>
@include('layouts.footer')
