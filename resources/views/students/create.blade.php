@include('layouts.header')
@include('layouts.sidebar')
    <main class="app-content">
          <div class="app-title">
            <div>
              <h1><i class="fa fa-users"></i> Create student</h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
              <li class="breadcrumb-item"><a href="route('home')"><i class="fa fa-home fa-lg"></i></a></li>
              <li class="breadcrumb-item"><a href="{{route('students')}}">Students</a></li>
              <li class="breadcrumb-item"><a href="#">Create students</a></li>
            </ul>
          </div>
          <div class="row">
            <div class="col-md-10 offset-md-1">
              <div class="tile">
              
                    <form method="POST" action="{{ route('savestudent') }}">
                        @csrf

                        @if (Session::has('message'))
                           <div class="alert alert-danger">{{ Session::get('message') }}</div>
                        @endif
                        <input type="hidden" name="formId" value="{{$formId}}">
                         <div class="row">
                            <div class="col-md-4" >
                               <div class="form-check form-check-inline">
                                  <input class="form-check-input" type="checkbox" id="studentCheckbox" name="oldStudents" value="oldStudent">
                                  <label class="form-check-label" for="studentCheckbox">(Check if the student is old)</label>
                                </div>
                            </div>
                                        
                        </div>    
                            <div class="row oldstudents" style="display: none;">
                             <div class="col-md-6">
                                <label> Year joined  </label>
                                <div class="input-group">
                                   <select name="yearOld" class="form-control form-control-sm">
                                    <option selected disabled>Year joined</option>
                                    <?php
                                      for ( $year = date('Y'); $year >= 1920;  $year--) {
                                        $selected = (isset($getYear) && $getYear == $year) ? 'selected' : '';
                                        echo "<option value=$year $selected>$year</option>";
                                      }
                                    ?>
                                  </select>
                                </div>
                            </div>

                            <div class="col-md-6" >
                                <label> Select Status  </label>
                               <select class="form-control" name="status" required>
                                            <option value="0">Inactive</option>
                                            <option value="1">Active</option>
                                </select>
                            </div>
                           
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <label> Parent </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <div data-toggle="modal" data-target="#addParent" class="input-group-text btn-group btn-group-sm " style="cursor: pointer;"> + </div>

                                    </div>
                                    <!-- <input class="parents form-control form-control-sm" type="text" name="parent" required> -->
                                    <select class="form-control" name="parent" style="padding:0px;">
                                        <option disabled selected>Select Parent</option>
                                        @foreach( $parents as $parent )
                                            <option value="{{ $parent->id }}">
                                                {{ $parent->parent_name }} - {{ $parent->phone_number }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <label> Relationship to Student </label>
                                <div class="input-group">
                                    <select class="form-control" name="relationship" style="padding:0px;">
                                        <option disabled selected>Select Relationship</option>
                                        @foreach( $relationships as $relation )
                                            <option value="{{ $relation->id }}">
                                                {{ $relation->relationship }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            
                        </div>


                        <br/>


                        <div class="row">
                            
                            <div class="col-md-6">
                                <label> Home County </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text btn-group btn-group-sm">
                                           <a data-href="#" data-toggle="modal" data-target="#add">+</a>

                                      </div>
                                    </div>
                                    <select class="form-control drop" name="countyId" style="padding:0px;">
                                        @foreach( $counties as $county )
                                            <option value="{{ $county->id }}">
                                                {{ $county->county_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <label> Streams </label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text btn-group btn-group-sm">
                                       <a data-href="#" data-toggle="modal" data-target="#addStreams" style="cursor: pointer;">+</a>
                                      </div>
                                    </div>

                                    <select class="form-control" name="streamId" style="padding:0px;">
                                        @foreach( $streams as $stream )
                                            <option value="{{ $stream->id }}">
                                                {{ $stream->stream_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                        <br>

                        <div class="row">
                               <div class="col-md-6">
                                <label> Gender </label>
                                <div class="input-group">
                                    <select class="form-control form-control-sm" name="gender" style="padding:0px;">
                                        <option selected disabled>Select Gender</option>
                                        @foreach( $genders as $gender )
                                            <option value="{{ $gender->id }}">
                                                {{ $gender->gender }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                           <div class="col-md-6">
                                <label>K.C.P.E / Previous exam Grade </label>
                                <input type="text" class="form-control form-control-sm" name="grade" required autocomplete="grade">
                            </div>
                            
                        </div>
                        <br>
                        <div class="row">
                          
                       
                            <div class="col-md-6">
                                <label> Student Full Name </label>
                                <input type="text" class="form-control form-control-sm" name="student_name" required autocomplete="student_name">
                            </div>

                            <div class="col-md-6">
                                <label> Admission Number </label>
                                <input type="text" class="form-control form-control-sm" name="admission_number" required autocomplete="admission_number">
                            </div>
                        </div>

                        <br/>

                        <div class="row">
                            <div class="col-md-6">
                                <label> KCPE Index Number </label>
                                <input type="text" class="form-control form-control-sm" name="index_number" autocomplete="index_number">
                            </div>

                            <div class="col-md-6">
                                <label> Date of Birth </label>
                                <div class="row">
                                    <div class="col-md-4" style="padding-bottom:20px;">
                                      <select name="year" class="form-control form-control-sm drop" required>
                                        <option selected disabled>Year</option>
                                        <?php
                                          for ( $year = date('Y'); $year >= 1920;  $year--) {
                                            $selected = (isset($getYear) && $getYear == $year) ? 'selected' : '';
                                            echo "<option value=$year $selected>$year</option>";
                                          }
                                        ?>
                                      </select>
                                    </div>


                                    <div class="col-md-4 drop" style="padding-bottom:20px;">
                                      <select name="month" class="form-control form-control-sm " id="selectMonth">
                                        <option selected disabled>Month</option>
                                        <?php
                                          foreach (['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'] as $monthNumber => $month) {
                                              echo "<option value='$monthNumber'>{$month}</option>";
                                          }
                                        ?>
                                      </select>
                                    </div>


                                    <div class="col-md-4">
                                      <select name="dob" class="form-control form-control-sm drop" required style="overflow: scroll;">
                                        <option selected disabled>Day</option>
                                          <?php
                                            for ( $day = 1; $day <= 31;  $day++) {
                                              $selected = (isset($getYear) && $getYear == $day) ? 'selected' : '';
                                              echo "<option value=$day $selected>$day</option>";
                                            }
                                          ?>
                                      </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br/>

                        <div class="row" style="padding-top:1em;">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary btn-block btn-sm"> Save Student </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div>
</main>
          <!--Add parent Modal -->

     <div class="modal fade" id="addParent">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Parent</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                   
                    <form method="POST" action="{{ route('saveparent', $formId) }}">
                        @csrf

                        @if (Session::has('message'))
                           <div class="alert alert-danger">{{ Session::get('message') }}</div>
                        @endif

                        <div class="row">
                            <div class="col-md-6 ">
                                <label>Full Name </label>
                            <input type="text"  class="form-control form-control-sm" name="parent_name" required autocomplete="parent_name">
                            </div>

                            <div class="col-md-6">
                                <label>  Occupation </label>
                            <input type="text" class="form-control form-control-sm" name="occupation" required autocomplete="occupation">
                            </div>
                        </div>

                        <br/>

                        <div class="row">
                            <div class="col-md-6">
                                <label> National ID </label>
                                <input type="number"  required class="form-control form-control-sm" name="national_id" autocomplete="national_id">
                            </div>

                            <div class="col-md-6">
                                <label>  Phone Number </label>
                                <input type="text"   class="form-control form-control-sm" name="phone_number" autocomplete="phone_number" required>
                            </div>
                        </div>

                        <br/>
                        
                        <div class="row" style="padding-top:1em;">
                            <div class="col-md-6">
                                    <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>                                                                    </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary btn-block btn-sm"> Save & Continue  </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addStreams">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Streams</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                   <form method="POST" action="{{route('addStream')}}">
                        @csrf

                        @if (Session::has('message'))
                        <div class="alert alert-danger">{{ Session::get('message') }}</div>
                        @endif


                        <div class="row">

                            <div class="col-md-12">
                                <label> Form </label>
                                <div class="input-group">
                                    <select class="form-control form-control-sm" name="formId" style="padding:0px;">
                                        <option disabled selected>--Select form--</option>
                                        @foreach( $forms as $form )
                                            <option value="{{ $form->id }}">
                                                {{ $form->form_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <label> Stream Name</label>
                                <input type="text" class="form-control form-control-sm" placeholder="stream name" name="stream_name" required autocomplete="stream_name">
                            </div>
                        </div>


                        <br/>

                        <div class="row" style="padding-top:1em;">
                            <div class="col-md-6">
                                    <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>                                                                    </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary btn-block btn-sm"> Save </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>



    
@include('layouts.footer')