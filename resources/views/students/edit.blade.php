@include('layouts.header')
@include('layouts.sidebar')
    <main class="app-content">
          <div class="app-title">
            <div>
              <h1><i class="fa fa-edit"></i> Edit student</h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
              <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
              <li class="breadcrumb-item">Students</li>
              <li class="breadcrumb-item"><a href="#">Edit students</a></li>
            </ul>
          </div>
          <div class="row">
            <div class="col-md-10 offset-md-1">
              <div class="tile">

                <form method="POST" action="{{ route('updateStudent') }}">
                    @csrf

                    @if (Session::has('message'))
                       <div class="alert alert-danger">{{ Session::get('message') }}</div>
                    @endif
                    <input type="hidden" name="studentId" value="{{$id}}">
                    <div class="row">
                    <div class="col-md-6" >
                       <div class="form-radio form-radio-inline">
                            @if($student->status == 1)
                          <input class="form-radio-input" type="radio" name="status" checked value="1">
                          <label class="form-check-label" for="status">Active</label> 
                          <input class="form-radio-input" type="radio" name="status" value="0">
                          <label class="form-radio-label" for="status">Inactive</label>
                          @else
                          <input class="form-radio-input" type="radio" name="status" value="1">
                          <label class="form-check-label" for="status">Active</label> 
                          <input class="form-radio-input" type="radio" name="status" checked value="0">
                          <label class="form-radio-label" for="status">Inactive</label>
                          @endif
                        </div>
                    </div>
                            
                        </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label> Parent </label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                <div data-toggle="modal" data-target="#addParent" class="input-group-text btn-group btn-group-sm " style="cursor: pointer;"> + </div>

                                </div>
                                 <?php 
                                    $studParent = DB::table('students')->where('id', $id)->pluck('parent_id')->first();
                                    $parentName = DB::table('parents')->where('id', $studParent)->pluck('parent_name')->first();


                                 ?>
                                <select class="form-control" name="parent" style="padding:0px;">
                                    <option disabled selected>{{$parentName}}</option>
                                    @foreach( $parents as $parent )
                                        <option value="{{ $parent->id }}">
                                            {{ $parent->parent_name }} - {{ $parent->phone_number }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <label> Relationship to Student </label>
                            <div class="input-group">
                                 <?php 
                                    $studRelationship= DB::table('students')->where('id', $id)->pluck('parent_relationship_id')->first();
                                    $relationshipName = DB::table('student_parent_relationship')->where('id', $studRelationship)->pluck('relationship')->first();


                                 ?>
                                <select class="form-control" name="relationship" style="padding:0px;">
                                    <option disabled selected>{{$relationshipName}}</option>
                                    @foreach( $relationships as $relation )
                                        <option value="{{ $relation->id }}">
                                            {{ $relation->relationship }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                    </div>


                    <br/>


                    <div class="row">
                        
                        <div class="col-md-6">
                            <label> Home County </label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text btn-group btn-group-sm">
                                       <a data-href="#" data-toggle="modal" data-target="#add">+</a>

                                  </div>
                                </div>
                                <?php 
                                    $studCounty= DB::table('students')->where('id', $id)->pluck('county_id')->first();
                                    $countyName = DB::table('counties')->where('id', $studCounty)->pluck('county_name')->first();


                                 ?>
                                <select class="form-control drop" name="countyId" style="padding:0px;">
                                    <option disabled selected>{{$countyName}}</option>
                                    @foreach( $counties as $county )
                                        <option value="{{ $county->id }}">
                                            {{ $county->county_name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <label> Streams </label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                  <div class="input-group-text btn-group btn-group-sm">
                                   <a data-href="#" data-toggle="modal" data-target="#addStreams" style="cursor: pointer;">+</a>
                                  </div>
                                </div>
                                <?php 
                                    $studStream = DB::table('students')->where('id', $id)->pluck('stream_id')->first();
                                    $streamName = DB::table('streams')->where('id', $studStream)->pluck('stream_name')->first();


                                 ?>

                                <select class="form-control" name="streamId" style="padding:0px;">
                                    <option selected disabled>{{$streamName}}</option>
                                    @foreach( $streams as $stream )
                                        <option value="{{ $stream->id }}">
                                            {{ $stream->stream_name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                    <br>

                    <div class="row">
                           <div class="col-md-6">
                            <label> Gender </label>
                            <div class="input-group">
                                <?php 
                                    $studGender= DB::table('students')->where('id', $id)->pluck('gender_id')->first();
                                    $genderName = DB::table('genders')->where('id', $studGender)->pluck('gender')->first();


                                 ?>
                                <select class="form-control form-control-sm" name="gender" style="padding:0px;">
                                    <option selected disabled>{{ $genderName }}</option>
                                    @foreach( $genders as $gender )
                                        <option value="{{ $gender->id }}">
                                            {{ $gender->gender }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                       <div class="col-md-6">
                            <label>K.C.P.E / Previous exam Grade </label>
                            <input type="text" class="form-control form-control-sm" name="grade" value="{{$student->grade}}" required autocomplete="grade">
                        </div>
                        
                    </div>
                    <br>
                    <div class="row">
                      
                   
                        <div class="col-md-6">
                            <label> Student Full Name </label>
                            <input type="text" class="form-control form-control-sm" name="student_name" value="{{$student->student_name}}" required autocomplete="student_name">
                        </div>

                        <div class="col-md-6">
                            <label> Admission Number </label>
                            <input type="text" class="form-control form-control-sm" name="admission_number" value="{{$student->admission_number}}" required autocomplete="admission_number">
                        </div>
                    </div>

                    <br/>

                    <div class="row">
                        <div class="col-md-12">
                            <label> KCPE Index Number </label>
                            <input type="text" class="form-control form-control-sm" name="kcpe_index_number" value="{{$student->kcpe_index_number}}" autocomplete="kcpe_index_number">
                        </div>

        
                    </div>

                    <br/>

                    <div class="row" style="padding-top:1em;">
                        <?php 
                            if ($formId == 1) {
                                $formUrl ="form1";
                            }else if ($formId == 2){
                                $formUrl ="form2";
                            }else if ($formId == 3){
                                $formUrl ="form3";
                            }elseif ($formId == 4) {
                                    $formUrl ="form4";
                            }else{
                                $formUrl = "students";
                            }

                         ?>
                        <div class="col-md-6">
                            <a href="{{route($formUrl)}}" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Back to students</a>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary btn-block btn-sm"> Update Student </button>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
      </div>
</main>
          <!--Add parent Modal -->

     <div class="modal fade" id="addParent">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Parent</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                   
                    <form method="POST" action="{{ route('saveparent', $formId) }}">
                        @csrf

                        @if (Session::has('message'))
                           <div class="alert alert-danger">{{ Session::get('message') }}</div>
                        @endif

                        <div class="row">
                            <div class="col-md-6 ">
                                <label>Full Name </label>
                            <input type="text"  class="form-control form-control-sm" name="parent_name" required autocomplete="parent_name">
                            </div>

                            <div class="col-md-6">
                                <label>  Occupation </label>
                            <input type="text" class="form-control form-control-sm" name="occupation" required autocomplete="occupation">
                            </div>
                        </div>

                        <br/>

                        <div class="row">
                            <div class="col-md-6">
                                <label> National ID </label>
                                <input type="number"  required class="form-control form-control-sm" name="national_id" autocomplete="national_id">
                            </div>

                            <div class="col-md-6">
                                <label>  Phone Number </label>
                                <input type="text"   class="form-control form-control-sm" name="phone_number" autocomplete="phone_number" required>
                            </div>
                        </div>

                        <br/>
                        
                        <div class="row" style="padding-top:1em;">
                            <div class="col-md-6">
                                    <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>                                                                    </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary btn-block btn-sm"> Save & Continue  </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addStreams">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Streams</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                   <form method="POST" action="{{route('addStream')}}">
                        @csrf

                        @if (Session::has('message'))
                        <div class="alert alert-danger">{{ Session::get('message') }}</div>
                        @endif


                        <div class="row">

                            <div class="col-md-12">
                                <label> Form </label>
                                <div class="input-group">
                                    <select class="form-control form-control-sm" name="formId" style="padding:0px;">
                                        <option disabled selected>--Select form--</option>
                                        @foreach( $forms as $form )
                                            <option value="{{ $form->id }}">
                                                {{ $form->form_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <label> Stream Name</label>
                                <input type="text" class="form-control form-control-sm" placeholder="stream name" name="stream_name" required autocomplete="stream_name">
                            </div>
                        </div>


                        <br/>

                        <div class="row" style="padding-top:1em;">
                            <div class="col-md-6">
                                    <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>                                                                    </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary btn-block btn-sm"> Save </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>



    
@include('layouts.footer')