 @include('layouts.header')
@include('layouts.sidebar')
<style type="text/css">
    .emp-profile{
    padding: 2%;
    margin-top: 1%;
    margin-bottom: 3%;
    border-radius: 0;
    background: transparent;
    min-height: 300px !important;
}
.profile-img{
    text-align: center;
}
.profile-img img{
    width: 70%;
    height: 10%;
}
.profile-img .file {
    position: relative;
    overflow: hidden;
    margin-top: -20%;
    width: 70%;
    border: none;
    border-radius: 0;
    font-size: 15px;
    background: #212529b8;
}
.profile-img .file input {
    position: absolute;
    opacity: 0;
    right: 0;
    top: 0;
}
.profile-head h5{
    color: #333;
}
.profile-head h6{
    color: #0062cc;
}
.profile-edit-btn{
    border: none;
    border-radius: 1.5rem;
    width: 70%;
    padding: 2%;
    font-weight: 600;
    color: #6c757d;
    cursor: pointer;
}
.proile-rating{
    font-size: 12px;
    color: #818182;
    margin-top: 5%;
}
.proile-rating span{
    color: #495057;
    font-size: 15px;
    font-weight: 600;
}
.profile-head .nav-tabs{
    margin-bottom:5%;
}
.profile-head .nav-tabs .nav-link{
    font-weight:600;
    border: none;
}
.profile-head .nav-tabs .nav-link.active{
    border: none;
    border-bottom:2px solid #0062cc;
}
.profile-work{
    padding: 14%;
    margin-top: -15%;
}
.profile-work p{
    font-size: 12px;
    color: #818182;
    font-weight: 600;
    margin-top: 10%;
}
.profile-work a{
    text-decoration: none;
    color: #495057;
    font-weight: 600;
    font-size: 14px;
}
.profile-work ul{
    list-style: none;
}
.profile-tab label{
    font-weight: 600;
}
.profile-tab p{
    font-weight: 600;
    color: #0062cc;
}
</style>

   <!-- Main Content Area -->
<main class="app-content">
	  <div class="app-title">
	    <div>
	      <h1><i class="fa fa-th-list"></i> Students Details</h1>
	    </div>
	    <ul class="app-breadcrumb breadcrumb side">
	      <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
	      <li class="breadcrumb-item">Students Details</li>
	      <li class="breadcrumb-item active"><a href="#">Students Details</a></li>
	    </ul>
	  </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
            	<form method="post">

	            <div class="row">
	                <div class="col-md-4">
	                    <div class="profile-img">
	                        <div id="firstImage">
	                            <img src="/images/avatars/avatar.png" style="height: 130px;width: 130px;" alt="avatar"/>
	                            </div>
	                              
	                            <div id="secondImage"></div>
	                    </div>
	                </div>
	                <div class="col-md-6">
	                    <div class="profile-head">
	                                <h5>
	                                    {{ $student->student_name }}
	                                </h5>
	                                <h6>
	                                   Form:  {{ $student->form_name }} -{{ $student->stream_name }}
	                                </h6>
	                                <h6>
	                                	Home County: {{$student->county_name}}
	                                </h6>
	                                <br>
	                        <ul class="nav nav-tabs" id="myTab" role="tablist">
	                            <li class="nav-item">
	                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">About</a>
	                            </li>
	                            <li class="nav-item">
	                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Parent Details</a>
	                            </li>           
	                            <li class="nav-item">
	                                <a class="nav-link" id="fee-tab" data-toggle="tab" href="#fee" role="tab" aria-controls="fee" aria-selected="false">Fee Details</a>
	                            </li>
	                        </ul>
	                    </div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-md-4">
	                </div>
	                <div class="col-md-8">
	                    <div class="tab-content profile-tab" id="myTabContent">
	                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

	                                        <table class="table table-striped table-sm">
	                                            <tr>
	                                                <td >Admission Number</td> 
	                                                <td>{{ $student->admission_number }}</td>
	                                            </tr>
	                                            <tr>
	                                                <td>Year joined</td> 
	                                                <td>{{ $student->jointed_year }}</td>
	                                            </tr>
	                                            <tr>
	                                                <td>K.C.P.E Index Number</td> 
	                                                <td>{{ $student->kcpe_index_number }}</td>
	                                            </tr>
	                                            <tr>
	                                                <td>K.C.P.E Grade</td> 
	                                                <td>{{ $student->grade }}</td>
	                                            </tr>
	                                            <tr>
	                                                <td> Date of Birth</td> 
	                                                <td>
	                                                    <?php $date = strtotime($student->dob); ?>
	                                                    {{ date("j-M-Y", $date) }}
	                                                </td>
	                                            </tr>
	                                   
	                                        </table>
	             
	                        </div>
	                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
	                               <table class="table table-striped table-sm">
	                                            <tr>
	                                                <td > Parent Name</td> 
	                                                <td>{{ $student->parent_name }}</td>
	                                            </tr>
	                                            <tr>
	                                                <td>Natuonal Id</td> 
	                                                <td>{{ $student->pnational_id }}</td>
	                                            </tr>
	                                            <tr>
	                                                <td> Phone Number </td> 
	                                                <td>{{ $student->pphone_number }}</td>
	                                            </tr>
	                                            <tr>
	                                                <td>Occupation </td> 
	                                                <td>{{ $student->occupation }}</td>
	                                            </tr>
	                                 
	                                   
	                                        </table>


	                                 
	                        </div>
	                                    
	                                   
	              
	                         <div class="tab-pane fade" id="fee" role="tabpanel" aria-labelledby="fee-tab">
	         
	                            <table class="table table-striped table-sm" >
	                                <tr>
	                                    <th> Date </th>
	                                    <th> Amount paid </th>
	                                    <th> Receipt Number </th>
	                                </tr>
	                                   @foreach($addedStudentFees as $addedStudentFee)
		                                    @if(!$addedStudentFee->receipt_number == null || !$addedStudentFee->receipt_number == 0)

		                                    <tr>   
		                                        <td> {{ date("j-M-Y", strtotime($addedStudentFee->created_at)) }} </td>
		                                        <td>{{ number_format($addedStudentFee->amount_paid, 2)}}</td>
		                                        <td>
		                                        	
		                                              <p><a href="/students/added/fee/{{$addedStudentFee->receipt_number}}/pdf" target="_blank" title="print Receipt">{{ $addedStudentFee->receipt_number }}</a></p>
		                                        </td>
		                                    </tr>
		                                        @endif
		                                 @endforeach
	                              
	                                @foreach($feepayment as $payment)
	                                    <tr>
	                                        @if(!$payment->receipt_number == null || !$payment->receipt_number == 0)
	                                        <td> {{ date("j-M-Y", strtotime($payment->created_at)) }} </td>
	                                        <td>
	                                            <?php
	                                            
	                                                $currentSemester = DB::table('semesters')
	                                                    ->where('current', 1)
	                                                    ->pluck('id')->first();
	                                                    
	                                                $receiptAmount = \App\Payment::where('student_id', $student->id)
	                                                    ->join('votehead_amounts', 'votehead_amounts.votehead_id', '=', 'payments.votehead_id')
	                                                    ->join('semesters', 'semesters.id', '=', 'votehead_amounts.semester_id')
	                                                    // ->where('payments.semester_id', $currentSemester)
	                                                    ->where('semesters.current', 1)
	                                                    ->where('payments.receipt_number', $payment->receipt_number )
	                                                    ->pluck('payments.amount')
	                                                    ->sum();
	                                            ?>
	                                            {{ number_format($receiptAmount, 2) }}
	                                        </td>


	                                        <td>
	                                            <p><a href="/student/{{ $payment->receipt_number }}/receipt" target="_blank" title="print Receipt">{{ $payment->receipt_number }}</a></p>
	                                        </td>
	                                        @endif
	                                    </tr>
	                                     
	                                @endforeach
	                                @foreach($balanceBroughtForwardDetails as $balanceBroughtForwardDetail)
                                    @if(!$balanceBroughtForwardDetail->receipt_number == null || !$balanceBroughtForwardDetail->receipt_number == 0)

                                    <tr>   
                                        <td> {{ date("j-M-Y", strtotime($balanceBroughtForwardDetail->created_at)) }} </td>
                                        <td>{{ number_format($balanceBroughtForwardDetail->amount_paid, 2)}}</td>
                                        <td>
                                              <p><a href="/balance/{{$balanceBroughtForwardDetail->receipt_number}}/brought/forward/pdf" target="_blank" title="print Receipt">{{ $balanceBroughtForwardDetail->receipt_number }}</a></p>
                                        </td>
                                    </tr>
                                        @endif
                                 @endforeach
	                                @foreach($oldReciepts as $oldReciept)
	                                    @if(!$oldReciept->receipt_number == null || !$oldReciept->receipt_number == 0)

	                                    <tr>   
	                                        <td> {{ date("j-M-Y", strtotime($oldReciept->created_at)) }} </td>
	                                        <td>{{ number_format($oldReciept->amount_paid, 2)}}</td>
	                                        <td>
	                                        	
	                                              <p><a href="/old/receipt/{{$oldReciept->receipt_number}}/pdf" target="_blank" title="print Receipt">{{ $oldReciept->receipt_number }}</a></p>
	                                        </td>
	                                    </tr>
	                                        @endif
	                                 @endforeach
	                          
	                              
	                               
	                            </table>
	                                   

	                        </div>
	                    </div>
	                </div>
	            </div>
	     
	        </form>
            </div>
        </div>
    </div>
</div>
</main>
@include('layouts.footer')