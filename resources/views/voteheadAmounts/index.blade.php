@include('layouts.header')
@include('layouts.sidebar')


    <!-- Main Content Area -->
     <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Votehead Amounts</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item">Votehead Amounts</li>
          <li class="breadcrumb-item active"><a href="#">Votehead Amounts</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="tile">
            <div class="tile-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" id="sampleTable">
                    <thead>
                        <tr>
                             <th>Votehead Name</th>
                            <th>Semester</th>
                            <th>Amount</th>
                            <th> Action </th>
                        </tr>
                    </thead>

                    <?php $num = 0; ?>
                    <tbody class="voteheadAmount">
                        @foreach($vamounts as $vamount)
                        <?php $num++; ?>
                        <tr>
                            <td>{{ $vamount->votehead_name}}</td>
                            <td>{{ $vamount->semester_name}}</td>
                            <td>{{ $vamount->amount}}</td>
                            <td>
                                <a data-href="#" data-toggle="modal" data-target="#editmyModal{{$vamount->vamount_id}}"><i class="fa fa-edit text-primary"></i></a>
                                <a data-href="#" data-toggle="modal" data-target="#myModal{{$vamount->vamount_id}}"><i class="fa fa-trash text-danger"></i></a>
                                    <!--delete modal-->
                                <div class="modal fade" id="myModal{{$vamount->vamount_id}}">
                                    <div class="modal-dialog modal-dialog-centered modal-sm" role="delete">
                                        <div class="modal-content">
                                            <div class="modal-header bg">
                                                <h5 class="modal-title"> Comfirm Delete</h5>
                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <P>You are about to delete this Votehead Amount?</P>
                                                      <div class="row" style="padding-top:1em;"> 
                                                        <div class="col-md-6">
                                                            <button type="button" class="btn btn-block btn-secondary btn-sm" data-dismiss="modal">Close</button>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <a class="btn btn-sm btn-danger btn-ok btn-block" href="{{route('deleteVoteheaAamount',$vamount->vamount_id)}}">Delete</a>
                                                            </div>
                                                        </div>
                                                   

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <!--edit Modal -->
                                <div class="modal fade" id="editmyModal{{$vamount->vamount_id}}">
                                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header bg">
                                                <h5 class="modal-title"> Votehead Amount</h5>
                                                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                               
                                                        <form method="POST" action="{{route('update_vamount',$vamount->vamount_id)}}">
                                                            @csrf

                                                            @if (Session::has('message'))
                                                            <div class="alert alert-danger">{{ Session::get('message') }}</div>
                                                            @endif


                                                            <div class="row">

                                                                <div class="col-md-6">
                                                                    <label> Semester </label>
                                                                      
                                                                        <select class="form-control" name="semester_id" style="padding:0px;">
                                                                            @foreach( $semesters as $semester )
                                                                                <option value="{{ $semester->id }}">
                                                                                    {{ $semester->semester_name }}
                                                                                </option>
                                                                            @endforeach
                                                                        </select>
                                                                </div>

                                                                <div class="col-md-6">
                                                                    <label> Votehead </label>
                                                                      
                                                                        <select class="form-control" name="votehead_id" style="padding:0px;">
                                                                            @foreach( $voteheads as $votehead )
                                                                                <option value="{{ $votehead->id }}">
                                                                                    {{ $votehead->votehead_name }}
                                                                                </option>
                                                                            @endforeach
                                                                        </select>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-12">
                                                                <label> Amount</label>
                                                            <input type="text" value="{{$vamount->amount}}" class="form-control form-control-sm" name="amount" required autocomplete="amount">
                                                            </div>


                                                            <br/>

                                                            <div class="row" style="padding-top:1em;">
                                                                <div class="col-md-6">
                                                                        <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>                                                                    </div>
                                                                <div class="col-md-6">
                                                                    <button type="submit" class="btn btn-primary btn-block btn-sm"> Update </button>
                                                                </div>
                                                            </div>

                                                        </form>
                                                 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
         </div>
    </div>
</div>

  <div class="card col-md-4 " >
        <div style="padding:1em;">
             <form method="POST" action="{{route('add_vamount')}}">
                @csrf

                <div class="alert alert-success kam-success" role="alert" style="display:none;">
                  Votehead amount added successfully!
                </div>

                <div class="alert alert-danger kam-error" role="alert" style="display:none;">
                  Votehead amount already exists!
                </div>
                <div class="alert alert-danger kam-error2" role="alert" style="display:none;">
                  Input all reqiured fields!
                </div>

                <div class="row">

                    <div class="col-md-12">
                        <label>Select semester </label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                        
                            <div data-toggle="modal" data-target="#addSemester" class="input-group-text btn-group btn-group-sm " style="cursor: pointer;"> + </div>

                            </div>
                            <select class="form-control" name="semester_id" id="semester_id"style="padding:0px;">
                                    <option>--Select semester--</option>t
                                @foreach( $semesters as $semester )
                                    <option value="{{ $semester->id }}">
                                        {{ $semester->semester_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label>Select votehead </label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <div class="input-group-text btn-group btn-group-sm kam_add_button" id="addvotehead">+</div>
                            </div>
                            <select class="form-control" name="votehead_id" id="votehead_id" style="padding:0px;">
                                @foreach( $voteheads as $votehead )
                                    <option value="{{ $votehead->id }}">
                                        {{ $votehead->votehead_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <label> Amount </label>
                    <input type="text" class="form-control form-control-sm" name="amount" id="amount" required autocomplete="amount">
                    </div>


                </div>
                <br/>

                <div class="row">
                 
                    <div class="col-md-12">
                        <button type="button" id="saveVoteheadAmount"class="btn btn-success btn-block btn-sm">Save</button>
                    </div>
                 
                </div>

            </form>
        </div>
    </div>
</div>
  <!--add semesters Modal -->
<div class="modal fade" id="addSemester">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg">
                <h5 class="modal-title"> Semesters</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
            </div>
            <div class="modal-body">
              
                    <form method="POST" action="{{route('add_semester')}}">
                            @csrf

                                <div class="alert alert-success kam-success" role="alert" style="display:none;">
                                  Semester added successfully!
                                </div>

                                <div class="alert alert-danger kam-error" role="alert" style="display:none;">
                                  Semester already exists!
                                </div>
                                <div class="alert alert-danger kam-error2" role="alert" style="display:none;">
                                  Input Semester Name!
                                </div>


                        <div class="row">
                            <div class="col-md-12">
                                <label> Semester Name </label>
                            <input type="text" class="form-control form-control-sm" id="semester_name" name="semester_name" required autocomplete="semester_name">
                            </div>



                        </div>
                                <br/>

                        <div class="row" style="padding-top:1em;">
                            <div class="col-md-2">
                                <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>
                            </div>
                            <div class="col-md-5">
                                    <button type="button" id="savesemester"class="btn btn-success btn-block btn-sm">Save & New</button>
                            </div>
                            <div class="col-md-5">
                                <button type="submit" class="btn btn-primary btn-block btn-sm">Save & Close</button>
                            </div>
                        </div>

                    </form>
               
            </div>
        </div>
    </div>
</div>



</main>

@include('layouts.footer')