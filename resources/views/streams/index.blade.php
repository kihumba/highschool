@include('layouts.header')
@include('layouts.sidebar')


    <!-- Main Content Area -->
     <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> Streams</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item">Streams</li>
          <li class="breadcrumb-item active"><a href="#">Streams</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
            <a data-href="#" data-toggle="modal" data-target="#addStreams" class="btn btn-rounded btn-primary mb-3" style="font-weight: bold; color: #fff;">+</a>
            <div class="table-responsive">
                <table class="table table-hover table-bordered" id="sampleTable">
                    <thead>
                        <tr>
                            <th>Stream Name</th>
                            <th>Form</th>
                            <th>Added By</th>
                            <th>Created at</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($streams as $stream)
                        <tr>
                            <td>{{ $stream->stream_name }}</td>
                            <td>{{ $stream->form_name }}</td>
                            <td>{{ $stream->name }}</td>
                            <td>{{ $stream-> created_at}}</td>
                            <td>
                                 <a data-href="#" data-toggle="modal" data-target="#editStream{{$stream->id}}"><i class="fa fa-edit text-primary"></i></a>&nbsp;&nbsp;
                                 <a data-href="#" data-toggle="modal" data-target="#deleteStream{{$stream->id}}"><i class="fa fa-trash text-danger"></i></a>

                            </td>
                        </tr>
                         <!-- DELETE A STREAMS -->
                          <div class="modal fade" id="deleteStream{{$stream->id}}">
                              <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                                  <div class="modal-content">
                                      <div class="modal-header bg">
                                          <h5 class="modal-title">Confirm Delete </h5>
                                          <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                      </div>
                                      <div class="modal-body">
                                        <p>You are about to Delete {{$stream->stream_name}}</p>
                                         <div class="row" style="padding-top:1em;">
                                            <div class="col-md-6">
                                                <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>                                                                    </div>
                                            <div class="col-md-6">
                                                <a href="{{route('deleteStream', $stream->id)}}" class="btn btn-danger btn-block btn-sm">Delete </a>
                                            </div>
                                        </div>
                                         
                                      </div>
                                  </div>
                              </div>
                          </div>
                             <!-- EDIT A STREAMS -->
                          <div class="modal fade" id="editStream{{$stream->id}}">
                              <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                                  <div class="modal-content">
                                      <div class="modal-header bg">
                                          <h5 class="modal-title">Confirm Delete </h5>
                                          <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                      </div>
                                      <div class="modal-body">
                                           <form method="POST" action="{{route('editStream', $stream->id)}}">
                                            @csrf

                                            @if (Session::has('message'))
                                            <div class="alert alert-danger">{{ Session::get('message') }}</div>
                                            @endif


                                            <div class="row">

                                                <div class="col-md-12">
                                                    <label> Form </label>
                                                    <div class="input-group">
                                                        <?php 
                                                        $streamForm = DB::table('streams')
                                                            ->where('form_id', $stream->form_id)
                                                            ->pluck('form_id')
                                                            ->first();


                                                         $formName =DB::table('forms')->where('id', $streamForm)
                                                            ->pluck('form_name')
                                                            ->first();


                                                         ?>
                                                        <select class="form-control form-control-sm" name="formId" style="padding:0px;">
                                                            <option selected disabled>{{$formName}}</option>

                                                            @foreach( $forms as $form )
                                                                <option value="{{ $form->id }}">
                                                                    {{ $form->form_name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-md-12">
                                                    <label> Stream Name</label>
                                                    <input type="text" class="form-control form-control-sm" value="{{$stream->stream_name}}" placeholder="stream name" name="stream_name" required autocomplete="stream_name">
                                                </div>
                                            </div>


                                            <br/>

                                            <div class="row" style="padding-top:1em;">
                                                <div class="col-md-6">
                                                        <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>                                                                    </div>
                                                <div class="col-md-6">
                                                    <button type="submit" class="btn btn-primary btn-block btn-sm"> Update </button>
                                                </div>
                                            </div>

                                        </form>
                                         
                                      </div>
                                  </div>
                              </div>
                          </div>
                          
                        @endforeach
                    </tbody>
                </table>
            </div>
         </div>
    </div>
</div>
</div>
  
<div class="modal fade" id="addStreams">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg">
                <h5 class="modal-title">Add Streams</h5>
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
            </div>
            <div class="modal-body">
               <form method="POST" action="{{route('addStream')}}">
                    @csrf

                    @if (Session::has('message'))
                    <div class="alert alert-danger">{{ Session::get('message') }}</div>
                    @endif


                    <div class="row">

                        <div class="col-md-12">
                            <label> Form </label>
                            <div class="input-group">
                                <select class="form-control form-control-sm" name="formId" style="padding:0px;">
                                    <option disabled selected>--Select form--</option>
                                    @foreach( $forms as $form )
                                        <option value="{{ $form->id }}">
                                            {{ $form->form_name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-12">
                            <label> Stream Name</label>
                            <input type="text" class="form-control form-control-sm" placeholder="stream name" name="stream_name" required autocomplete="stream_name">
                        </div>
                    </div>


                    <br/>

                    <div class="row" style="padding-top:1em;">
                        <div class="col-md-6">
                                <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>                                                                    </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary btn-block btn-sm"> Save </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

</main>

@include('layouts.footer')