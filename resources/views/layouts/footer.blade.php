   <!-- Essential javascripts for application to work-->
    <script src="/assets/js/jquery-3.3.1.min.js"></script>
    <script src="/assets/js/popper.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="/assets/js/plugins/pace.min.js"></script>
      <script type="text/javascript" src="/assets/js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
    <!-- Page specific javascripts-->
    <script type="text/javascript" src="/assets/js/plugins/chart.js"></script>
     <script type="text/javascript" src="/assets/js/plugins/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/bootstrap-datepicker.min.js"></script>
     <script type="text/javascript">
      $('#demoDate').datepicker({
        format: "yyyy/mm/dd",
        autoclose: true,
        todayHighlight: true
      });
    </script>
    <script type="text/javascript">
      var data = {
      	labels: ["January", "February", "March", "April", "May"],
      	datasets: [
      		{
      			label: "My First dataset",
      			fillColor: "rgba(220,220,220,0.2)",
      			strokeColor: "rgba(220,220,220,1)",
      			pointColor: "rgba(220,220,220,1)",
      			pointStrokeColor: "#fff",
      			pointHighlightFill: "#fff",
      			pointHighlightStroke: "rgba(220,220,220,1)",
      			data: [65, 59, 80, 81, 56]
      		},
      		{
      			label: "My Second dataset",
      			fillColor: "rgba(151,187,205,0.2)",
      			strokeColor: "rgba(151,187,205,1)",
      			pointColor: "rgba(151,187,205,1)",
      			pointStrokeColor: "#fff",
      			pointHighlightFill: "#fff",
      			pointHighlightStroke: "rgba(151,187,205,1)",
      			data: [28, 48, 40, 19, 86]
      		}
      	]
      };
      var pdata = [
      	{
      		value: 300,
      		color: "#46BFBD",
      		highlight: "#5AD3D1",
      		label: "Complete"
      	},
      	{
      		value: 50,
      		color:"#F7464A",
      		highlight: "#FF5A5E",
      		label: "In-Progress"
      	}
      ]
      
      var ctxl = $("#lineChartDemo").get(0).getContext("2d");
      var lineChart = new Chart(ctxl).Line(data);
      
      var ctxp = $("#pieChartDemo").get(0).getContext("2d");
      var pieChart = new Chart(ctxp).Pie(pdata);
    </script>
    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','../../www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      }
    </script>
     <script type="text/javascript">
    $(function(){
         $("#saveVotehead").on('click', function(){
            $.ajaxSetup({
                headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
            });

            var voteheadName = $("#votehead_name").val();
            if( voteheadName != null && voteheadName != "" ){
                $.ajax({
                    method: 'post',
                    url:'{{ route("addVotehead") }}',
                    dataType:'json',
                    data:{
                        voteheadName:voteheadName,
                    },
                    success: function(res){
                           if( res.error != null ){
                                $('.kam-success').hide();
                                $('.kam-error').show();
                                $('.kam-error2').hide();
                            }
                       var voteheads = [];

                        $("#votehead_name").val('');
                        $('.kam-success').show();
                        $(".voteheads").html('');
                        $('.kam-error2').hide();

                        $.each(res, function(index, value){

                            voteheads = '<tr><td>#</td> <td>'+value.votehead_name+'</td><td>'+value.name+'</td><td><a data-href="#" data-toggle="modal" data-target="#editmyModal'+value.id+'"><i class="fa fa-edit text-primary"></i></a><a data-href="#" data-toggle="modal" data-target="#myModal'+value.id+'"><i class="fa fa-trash text-danger"></i></a></td></tr>';

                
                            $(".voteheads").append(voteheads);
                        });
                    }
                });
            }else{
                $('.kam-error2').show();
                $('.kam-success').hide();
            }

        });
     });

    </script>
    <script type="text/javascript">
         $("#savesemester").on('click', function(){

            $.ajaxSetup({
                headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
            });

            var semesterName = $("#semester_name").val();
            
            if( semesterName != null && semesterName != "" ){
                $.ajax({
                    method: 'post',
                    url:'{{ route("add_semester") }}',
                    dataType:'json',
                    data:{
                        semesterName:semesterName,
                    },
                    success: function(res){
                           if( res.error != null ){
                                $('.kam-success').hide();
                                $('.kam-error').show();
                                $('.kam-error2').hide();
                            }

                        var semesters = [];
                        $(".semesters").html('');
                        $("#semester_name").val('');
                        $('.kam-success').show();
                        $('.kam-error2').hide();
                        $.each(res, function(index, value){

                            semesters = '<tr> <td>'+value.semester_name+'</td><td>'+value.name+'</td><td><span> - </span></td><td><a data-href="#" data-toggle="modal" data-target="#editmyModal'+value.id+'"><i class="fa fa-edit text-primary"></i></a><a data-href="#" data-toggle="modal" data-target="#myModal'+value.id+'"><i class="fa fa-trash text-danger"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><a data-href="#" data-toggle="modal" data-target="#confirmsemester'+value.id+'"><i class="fa fa-check-circle text-success"></i></a></td></tr>';

                
                            $(".semesters").append(semesters);
                        });
                    }
                });
            }else{
                $('.kam-error2').show();
                $('.kam-success').hide();
            }

        });
    </script>
     <script type="text/javascript">
         $("#saveVoteheadAmount").on('click', function(){

            $.ajaxSetup({
                headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
            });

            var voteheadId = $("#votehead_id").val();
            var semesterId = $("#semester_id").val();
            var amount = $("#amount").val();
            
            if( amount != null && amount != "" ){
                $.ajax({
                    method: 'post',
                    url:'{{ route("add_vamount") }}',
                    dataType:'json',
                    data:{
                        voteheadId:voteheadId,
                        semesterId:semesterId,
                        amount:amount,
                    },
                    success: function(res){
                         if( res.error != null ){
                                $('.kam-success').hide();
                                $('.kam-error').show();
                                $('.kam-error2').hide();
                            }
                        var voteheadAmount = [];
                        $(".voteheadAmount").html('');
                        $("#amount").val('');
                        $('.kam-success').show();
                        $('.kam-error2').hide();


                        $.each(res, function(index, value){

                            voteheadAmount = '<tr> <td>'+value.votehead_name+'</td><td>'+value.semester_name+'</td><td>'+value.amount+'</td><td><a data-href="#" data-toggle="modal" data-target="#editmyModal'+ value.vamount_id +'"><i class="fa fa-edit text-primary"></i></a><a data-href="#" data-toggle="modal" data-target="#myModal'+ value.vamount_id +'"><i class="fa fa-trash text-danger"></i></a></td></tr>';

                
                            $(".voteheadAmount").append(voteheadAmount);
                        });
                    }
                });
            }else{
                $('.kam-error2').show();
                $('.kam-success').hide();
            }

        });
    </script>
    <script type="text/javascript">
         $("#saveSubject").on('click', function(){

            $.ajaxSetup({
                headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
            });

            var subjectName = $("#subject_name").val();
            
            if( subjectName != null && subjectName != "" ){
                $.ajax({
                    method: 'post',
                    url:'{{ route("saveSubject") }}',
                    dataType:'json',
                    data:{
                        subjectName:subjectName,
                    },
                    success: function(res){
                           if( res.error != null ){
                                $('.kam-success').hide();
                                $('.kam-error').show();
                                $('.kam-error2').hide();
                            }

                        var subjects = [];
                        $(".subjects").html('');
                        $("#subject_name").val('');
                        $('.kam-success').show();
                        $('.kam-error2').hide();
                        $.each(res, function(index, value){

                            subjects = '<tr><td>#</td> <td>'+value.subject_name+'</td><td>'+value.name+'</td><td>'+value.created_at+'</td><td><a data-href="#" data-toggle="modal" data-target="#editmyModal'+value.id+'"><i class="fa fa-edit text-primary"></i></a>&nbsp;&nbsp;&nbsp;<a data-href="#" data-toggle="modal" data-target="#myModal'+value.id+'"><i class="fa fa-trash text-danger"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></td></tr>';

                
                            $(".subjects").append(subjects);
                        });
                    }
                });
            }else{
                $('.kam-error2').show();
                $('.kam-success').hide();
            }

        });
    </script>

        <script type="text/javascript">
        $("#assignSubjects").on('click', function(){

            $.ajaxSetup({
                headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
            });

            var teacherId = $("#teacherId").val();
            var formId = $("#formId").val();
            var streamId = $("#streamId").val();
            
                $.ajax({
                    method: 'post',
                    url:'{{ route("assignSubjects") }}',
                    dataType:'json',
                    data:{
                        teacherId:teacherId,
                        formId:formId,
                        streamId:streamId
                    },
                    success: function(res){
                       if( res.error != null ){
                                $('.kam-success').hide();
                                $('.kam-error').show();
                                $('.kam-error2').hide();
                            }

                        var teacherId = res.teacherId;
                        var formId = res.formId;
                        var streamId = res.streamId;
                        $("#teacherId").append(teacherId);
                        $("#formId").append(formId);
                        $("#streamId").append(streamId);
                        window.open('/assign/subjects');

                     
                     
                    }
                });
          

        });
    </script>

    <script type="text/javascript">
        function onlyOne(checkbox) {
            var checkboxes = document.getElementsByName('payment')
            checkboxes.forEach((item) => {
                if (item !== checkbox) item.checked = false
            })
        }
    </script>
     <script type="text/javascript">

        $(function(){

        $.ajaxSetup({
            headers:
            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

        $('.unlock').css('visibility', 'hidden');


        $('.received_input').on('keyup', function(){
            var amount = parseInt($(this).val());

                $('.amount_received').text(amount.toLocaleString());
            });

            $('.amount_paid').on('focusout', function(){

                var amount = $('.received_input').val();
                var studentId = $(this).attr('student_id');

                var voteheadAmount = $(this).val();
                var voteheadId = $(this).attr('votehead_id');
                var semesterId = $(this).attr('semester_id');
                var studentId = $(this).attr('student_id');
                var paymentDateDone = $("#demoDate").val();

                var paymentMethod = $('input[name="payment"]:checked').val();

                if( paymentMethod == '' || paymentMethod == null ){
                    $('.payment_m').css(
                        {
                            "border-color": "#E20612", 
                            "border-width":"1px", 
                            "border-style":"solid"
                        });
                }else if((amount == '' || amount == null )) {
                    $('.received_input').css('border', 'solid 2px red');
                }else if((paymentDateDone == '' || paymentDateDone == null )) {
                    $('.datetimepicker-input').css(
                   {
                        "border-color": "#E20612", 
                        "color": "#E20612", 
                        "border-width":"1px", 
                        "border-style":"solid"
                        });
                }else{
                    var newAmount = parseInt($(this).val());
                    var received = $('.received_input').val();

                    $(this).attr('disabled','disabled');

                    var unlock = $(this).nextAll('unlock');

                    $.ajax({
                        method: 'POST',
                        url:'{{ route("newpayment") }}',
                        dataType:'json',
                        data:{
                            paymentDateDone:paymentDateDone,
                            amount:voteheadAmount,
                            totalAmount:amount,
                            voteheadId:voteheadId,
                            semesterId:semesterId,
                            studentId:studentId,
                            paymentMethod:paymentMethod
                        },
                        success: function(res){
                            var assigned = res.assigned;

                            if(assigned == null || assigned == ''){
                                assigned = 0;
                            }

                            $('.amount_assigned').html(assigned);
                            $('.amount_balance').html( res.balance );
                            
                             if( res.balance == 0 ){
                                var amountGiven = $('.amount_received').text();
                                window.open('/student/'+res.id+'/receipt', '_blank');
                                if( res.formUrl == 0 ){
                                window.location.href="/students";
                              }else{
                                 window.location.href='/students/'+res.formUrl+'';
                              }
                            }
                        }
                    });

                }
            });

        });
      </script>
      <script type="text/javascript">
        
        $(function(){

            $.ajaxSetup({
                headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
            });

            $('.formId').change( function(){
                var formId = $(this).val();

                $.ajax({
                    method: 'POST',
                    url:'{{ route("getFormStreams") }}',
                    dataType:'json',
                    data:{
                        formId:formId
                    },
                    success: function(res){
                      console.log(res.options)

                        var options = [];

                        $('.streams').html('');

                        if( res != null && res != "" ){
                            $.each(res, function( index, value ) {
                                options = '<option value="'+value.id+'">'+value.stream_name+'</option>';
                                $('.streams').append( options );
                            });

                        }else{
                            $('.streams').append( '<option>--No streams found--</option>' );
                        }

                    }
                });
                
            });

        });

      </script>
      <script type="text/javascript">
              
          $(function(){

              $.ajaxSetup({
                  headers:
                  { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
              });

              $('#formIds').change( function(){
                  var formIds = $(this).val();

                  $.ajax({
                      method: 'POST',
                      url:'{{ route("getFormStreams") }}',
                      dataType:'json',
                      data:{
                          formId:formIds
                      },
                      success: function(res){

                          var options = [];

                          $('#streamId').html('');

                          if( res != null && res != "" ){
                              $.each(res, function( index, value ) {
                                  options = '<option value="'+value.id+'">'+value.stream_name+'</option>';
                                  $('#streamId').append( options );
                              });

                          }else{
                              $('#streamId').append( '<option>--No streams found--</option>' );
                          }

                      }
                  });
                  
              });

          });

      </script>
      <script type="text/javascript">
    
          $(function(){

              $.ajaxSetup({
                  headers:
                  { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
              });

              $('.forms').change( function(){
                  var forms = $(this).val();

                  $.ajax({
                      method: 'POST',
                      url:'{{ route("getStreams") }}',
                      dataType:'json',
                      data:{
                          formId:forms
                      },
                      success: function(res){

                          var options = [];

                          $('#streamId').html('');

                          if( res != null && res != "" ){
                              $.each(res, function( index, value ) {
                                  options = '<option value="'+value.id+'">'+value.stream_name+'</option>';
                                  $('#streamId').append( options );
                              });
                               $('.streams').show();
                               $('.subjects').show();
                               $('.exams').show();

                          }else{
                              $('#streamId').append( '<option>--No streams found--</option>' );
                          }

                      }
                  });
                  
              });

          });


           $(function(){

            $.ajaxSetup({
                headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
            });

            $('#examtype').change( function(){
                var subjectId = $('#subjectId').val();
                var streamId = $('#streamId').val();
                var examSeries = $('#examtype').val();

                $.ajax({
                    method: 'POST',
                    url:'{{ route("getStudents") }}',
                    dataType:'json',
                    data:{
                        streamId:streamId,
                        subjectId:subjectId,
                        examSeries:examSeries
                    },
                    success: function(res){
                        
                        var students = [];

                        $('.form_students').html('');
                        $('#uploaded_marks').html('');

                        if( res.students != null && res.students != "" ){
                            $.each(res.students, function( index, value ) {

                                students = '<div class="row" style="padding-top:0.4em;"><div class="col-md-5">'+value.student_name+'</div><div class="col-md-3">'+value.admission_number+'</div><div class="col-md-4"><input student="'+value.id+'" type="number" class="form-control form-control-sm marks_entered"></div></div>';

                                $('.form_students').append( students );
                            });
                        }else{
                            $('.form_students').append( 'No students in this Stream.' );
                        }

                        $('#studentsTableHeader').show();

                        if( res.existingStudents != null && res.existingStudents != "" ){
                            $.each(res.existingStudents, function( index, value ) {

                                existingStudents = '<tr><td>'+value.student_name+'</td><td>'+value.admission_number+'</td><td>'+value.cat_one+'</td><td>'+value.cat_two+'</td><td>'+value.end_term+'</td></tr>';

                                $('#uploaded_marks').append( existingStudents );
                            });
                        }else{
                            $('#uploaded_marks').append( 'No marks entered for this unit' );
                        }

                    }
                });

            });

        });



        </script>
        <script type="text/javascript">
            $(function(){

            $.ajaxSetup({
                headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
            });

            $('body').on('focusout','.marks_entered',function(){
                var marks = $(this).val();
                var studentId = $(this).attr('student');
                var subjectId = $('#subjectId').val();

                if(marks != null && marks != ""){
                    if(marks > 100){
                        $('.error2').show();
                    }else{
                        $.ajax({
                            method: 'POST',
                            url:'{{ route("savemarks") }}',
                            dataType:'json',
                            data:{
                                marks:marks,
                                subjectId:subjectId,
                                studentId:studentId
                            },
                            success: function(res){

                                $('#uploaded_marks').html('');

                                if( res != null && res != "" ){
                                    $.each(res, function( index, value ) {

                                        students = '<tr><td>'+value.student_name+'</td><td>'+value.admission_number+'</td><td>'+value.cat_one+'</td><td>'+value.cat_two+'</td><td>'+value.end_term+'</td></tr>';

                                        $('#uploaded_marks').append( students );
                                        $('.error2').hide();
                                    });
                                }else{
                                    $('#uploaded_marks').append( 'No marks entered for this unit' );
                                }
                            }
                        });
                    }
                }

            });

        });

        </script>
        <script type="text/javascript">
          $(function(){

              $.ajaxSetup({
                  headers:
                  { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
              });

               $('#searchStudentData').click( function(){
                  var admission = $('#admission').val();

                  if( admission != null && admission != "" ){

                      $('.admissionNumError').hide();
                      $('#admission').css({'border-color': '#03AC13'});

                      $.ajax({
                          method: 'POST',
                          url:'{{ route("studentsData") }}',
                          dataType:'json',
                          data:{
                              admission:admission
                          },
                          success: function(res){

                              $('.studentDetails').html('');

                              var table;

                              if( res != null ){
                                  table = '<table class="table table-striped"><tr><td>Name:</td><td>'+res.student_name+'</td></tr><tr><td>ADM NUM:</td><td>'+res.admission_number+'</td></tr><tr><td>Gender:</td><td>'+res.gender+'</td><td><input type="hidden" id="studentId" value="'+res.id+'"></td></tr></table>';

                                  $('.selectSemester').show();
                                  $('.showResults').hide();
                                  $('.noResults').hide();

                              }else{
                                  $('.selectSemester').hide();
                                  $('.showResults').hide();
                                  $('.noResults').hide();
                                  table = '<h6 style="color:red;">This admission number does not exist!</h6>';

                              }

                              $('.studentDetails').append(table);
                          }
                      });
                      
                  }else{
                      $('.admissionNumError').show();
                      $('.showResults').hide();
                      $('.noResults').hide();
                      $('#admission').css({'border-color': '#FD0002'});
                  }

               });

           });


    </script>
    <script type="text/javascript">
          
        $(function(){

            $.ajaxSetup({
                headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
            });

            $('#semesterId').click( function(){
                var semesterId = $(this).val();
                var studentId = $('#studentId').val();

                 $.ajax({
                    method: 'POST',
                    url:'{{ route("getResults") }}',
                    dataType:'json',
                    data:{
                        semesterId:semesterId,
                        studentId:studentId
                    },
                    success: function(res){

                        $('.semesterResults').html('');

                        if( res != null && res != '' ){
                            $('.showResults').show();
                            $('.noResults').hide();
                        }else{
                            $('.showResults').hide();
                            $('.noResults').show();
                        }

                        $.each(res, function( index, value ) {
                          if(value.cat_one == null){
                            value.cat_one = 0;
                          }
                            if(value.cat_two == null){
                            value.cat_two = 0;
                          }
                            if(value.end_term == null){
                            value.end_term = 0;
                          }
                            

                            var total = parseInt(value.cat_one) + parseInt(value.cat_two) + parseInt(value.end_term);

                            var grade = null;

                            if( total >= 70 && total <=100 ){
                                grade = 'A';
                            }else if( total >= 60 ){
                                grade = 'B';
                            }else if( total >= 50 ){
                                grade = 'C';
                            }else if( total >= 40 ){
                                grade = 'D';
                            }else if( total < 40 ){
                                grade = 'E';
                            }else{
                                grade = "INV";
                            }

                            table = '<tr><td>'+value.subject_name+'</td><td>'+value.cat_one+'</td><td>'+value.cat_two+'</td><td>'+value.end_term+'</td><td>'+total+'</td><td>'+grade+'</td></tr>';

                            $('.semesterResults').append( table );
                        });
                    }
                });

            });

        });
      </script>
      <script>
        $(document).ready(function(){
            $('input[type="checkbox"]').click(function(){
                if($(this).prop("checked") == true){
                    $('.allTranscripts').hide();
                    $('.singleTranscript').show();
                    
                    // console.log("Checkbox is checked.");
                }
                else if($(this).prop("checked") == false){
                    $('.singleTranscript').hide();
                    $('.allTranscripts').show();
                    // console.log("Checkbox is unchecked.");
                }
            });
        });
    </script>
    <script type="text/javascript">
      $(function(){

              $.ajaxSetup({
                  headers:
                  { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
              });

              $('.form').change( function(){
                  var form = $(this).val();

                  $.ajax({
                      method: 'POST',
                      url:'{{ route("getStreams") }}',
                      dataType:'json',
                      data:{
                          formId:form
                      },
                      success: function(res){

                          var options = [];

                          $('#streamId').html('<option value="all">All</option>');
                          $('.print').show();

                          if( res != null && res != "" ){
                              $.each(res, function( index, value ) {
                                  options = '<option value="'+value.id+'">'+value.stream_name+'</option>';
                                  $('#streamId').append( options );
                              });
                               $('.streams').show();
                               $('.subjects').show();
                               $('.exams').show();

                          }else{
                              $('#streamId').append( '<option>--No streams found--</option>' );
                          }

                      }
                  });
                  
              });

          });

    </script>
    <script type="text/javascript">
       $("#getFormIdStreamId").on('click', function(){

          $.ajaxSetup({
              headers:
              { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
          });

          var formId = $(".form").val();
          var streamId = $("#streamId").val();
          
          if( formId == null || formId == "" ){
            $('.form').css('border', 'solid 2px red');
          }else if( streamId == null || streamId == "" ){
            $('#streamId').css('border', 'solid 2px red');
          }else{
              $.ajax({
                  method: 'post',
                  url:'{{ route("getFormIdStreamId") }}',
                  dataType:'json',
                  data:{
                      streamId:streamId,
                      formId:formId
                  },
                  success: function(res){
                    
                          if( res.streamId != null && res.formId != null ){

                              window.open('/print/'+res.streamId+'/'+res.formId+'/transcript', '_blank');
                              // window.location.href="/select/report";

                          }
                  }
              });
          }

      });
  </script>
  <script type="text/javascript">
    $(function(){
         $("#addedFeeBalance").on('click', function(){
            $.ajaxSetup({
                headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
            });

            var studentId = $("#studentId").val();
            var amountPaid = $("#amountPaidA").val();
            var paymentDateDone = $("#demoDate").val();
            var paymentMethod = $('input[name="payment"]:checked').val();
             if( paymentMethod == '' || paymentMethod == null ){
                    $('.payment_m').css(
                        {
                            "border-color": "#E20612", 
                            "border-width":"1px", 
                            "border-style":"solid"
                        });
              
                }else if((paymentDateDone == '' || paymentDateDone == null )) {
                    $('.datetimepicker-input').css(
                   {
                        "border-color": "#E20612", 
                        "color": "#E20612", 
                        "border-width":"1px", 
                        "border-style":"solid"
                        });
                  }else if((amountPaid == '' || amountPaid == null )) {
                $('#amountPaidA').css('border', 'solid 2px red');
                 $('.balanc').css(
                   {
                        "border-color": "#E20612", 
                        "color": "#E20612", 
                        "border-width":"1px", 
                        "border-style":"solid"
                        });
                }else{
                $.ajax({
                    method: 'post',
                    url:'{{ route("payAddedfee") }}',
                    dataType:'json',
                    data:{
                        studentId:studentId,
                        paymentMethod:paymentMethod,
                        paymentDateDone:paymentDateDone,
                        amountPaid:amountPaid,
                    },
                    success: function(res){
              
                            window.open('/students/added/fee/'+res.id+'/pdf', '_blank');
                             if( res.formUrl == 0 ){
                                window.location.href="/students";
                              }else{
                                 window.location.href='/students/'+res.formUrl+'';
                              }
                    
                    }
                });
            }

        });
     });

    </script>
     <script>
    $(document).ready(function(){
        $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
                $('.oldstudents').show();
                
                // console.log("Checkbox is checked.");
            }
            else if($(this).prop("checked") == false){
                $('.oldstudents').hide();
                // console.log("Checkbox is unchecked.");
            }
        });
    });
</script>
 <script type="text/javascript">
    $(function(){
         $("#payclose").on('click', function(){
            $.ajaxSetup({
                headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
            });

            var studentId = $("#studentId").val();
            var amountPaid = $("#amountPaid").val();
            var semesterId = $("#semesterId").val();
            var totalBalance = $("#totalBalance").val();
            var paymentDateDone = $("#demoDate").val();
            var paymentMethod = $('input[name="payment"]:checked').val();
             if( paymentMethod == '' || paymentMethod == null ){
                    $('.payment_m').css(
                        {
                            "border-color": "#E20612", 
                            "border-width":"1px", 
                            "border-style":"solid"
                        });
                }else if((totalBalance == '' || totalBalance == null )) {
                    $('#totalBalance').css('border', 'solid 2px red');
                }else if((amountPaid == '' || amountPaid == null )) {
                $('#amountPaid').css('border', 'solid 2px red');
                }else if((semesterId == '' || semesterId == null )) {
                $('#semesterId').css('border', 'solid 2px red');
                }else if((paymentDateDone == '' || paymentDateDone == null )) {
                    $('.datetimepicker-input').css(
                   {
                        "border-color": "#E20612", 
                        "color": "#E20612", 
                        "border-width":"1px", 
                        "border-style":"solid"
                        });
                }else{
                $.ajax({
                    method: 'post',
                    url:'{{ route("paidOldReceiptfee") }}',
                    dataType:'json',
                    data:{
                        studentId:studentId,
                        paymentMethod:paymentMethod,
                        paymentDateDone:paymentDateDone,
                        totalBalance:totalBalance,
                        semesterId:semesterId,
                        amountPaid:amountPaid,
                    },
                    success: function(res){
                       
                            var balance;
                        $('.kam-success').show();

                        $('.paymentsBalance').html('');

                        if( res.balance != null && res.balance != "" ){
                            

                                balance = '<div class="col-md-6"><div class="paymentsBalance"><label>Total balance </label><input type="text" class="form-controlform-control-sm" id="totalBalance" name="totalBalance" value='+res.balance+' required autocomplete="totalBalance"></div></div>'

                                $('.paymentsBalance').append( balance );
                            }

                            window.open('/old/receipt/'+res.id+'/pdf', '_blank');

                            window.location.href="/inactive/students";
                    
                    }
                });
            }

        });
     });

    </script>
     <script type="text/javascript">
      $(function(){
           $("#balanceBroughtForward").on('click', function(){
              $.ajaxSetup({
                  headers:
                  { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
              });

              var studentId = $("#stud_id").val();
              var amount = $("#amountPaid").val();
              var paymentMethod = $('input[name="payment"]:checked').val();
              var paymentDateDone = $("#demoDate").val();
              if((paymentMethod == '' || paymentMethod == null )) {
                 $('.payment_m').css(
                        {
                            "border-color": "#E20612", 
                            "border-width":"1px", 
                            "border-style":"solid"
                        });

              }else if((paymentDateDone == '' || paymentDateDone == null )) {
                      $('.datetimepicker-input').css(
                     {
                          "border-color": "#E20612", 
                          "color": "#E20612", 
                          "border-width":"1px", 
                          "border-style":"solid"
                          });
                    }else if((amount == '' || amount == null )) {
                  $('#amountPaid').css('border', 'solid 2px red');
                   $('.bal').css(
                     {
                          "border-color": "#E20612", 
                          "color": "#E20612", 
                          "border-width":"1px", 
                          "border-style":"solid"
                          });
                  }else{
                  $.ajax({
                      method: 'post',
                      url:'{{ route("paybalancebroughtForward") }}',
                      dataType:'json',
                      data:{
                          studentId:studentId,
                          paymentDateDone:paymentDateDone,
                          paymentMethod:paymentMethod,
                          amount:amount
                      },
                      success: function(res){
                              window.open('/balance/'+res.id+'/brought/forward/pdf', '_blank');

                              window.location.href='/students/'+res.formUrl+'';
                      
                      }
                  });
              }

          });
       });

    </script>
     <script type="text/javascript">
    $(function(){
         $("#saveRemark").on('click', function(){
            $.ajaxSetup({
                headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
            });

            var remark = $("#remark").val();
            var min_marks = $("#min_marks").val();
            var max_marks = $("#max_marks").val();
            var grade = $("#grade").val();
            if( remark == null || remark == "" ){

              $('.kam-error2').show();
              $('.kam-success').hide();

            }else if(max_marks == null || max_marks == ""){

              $('.kam-error2').show();
              $('.kam-success').hide();

            }
            else if(min_marks == null || min_marks == ""){

              $('.kam-error2').show();
              $('.kam-success').hide();

            }else if(grade == null || grade == ""){

              $('.kam-error2').show();
              $('.kam-success').hide();

            }else{

        
                $.ajax({
                    method: 'post',
                    url:'{{ route("saveRemarks") }}',
                    dataType:'json',
                    data:{
                        remark:remark,
                        max_marks:max_marks,
                        min_marks:min_marks,
                        grade:grade
                    },
                    success: function(res){
                           if( res.error != null ){
                                $('.kam-success').hide();
                                $('.kam-error').show();
                                $('.kam-error2').hide();
                            }
                       var remarks = [];

                        $("#remark").val('');
                        $("#min_marks").val('');
                        $("#max_marks").val('');
                        $("#grade").val('');
                        $('.kam-success').show();
                        $(".remarks").html('');
                        $('.kam-error2').hide();

                        $.each(res, function(index, value){

                            remarks = '<tr><td>#</td> <td>'+value.min_marks+'% - '+value.max_marks+'%</td><td>'+value.grade+'</td><td>'+value.remark+'</td><td>'+value.name+'</td><td><a data-href="#" data-toggle="modal" data-target="#editmyModal'+value.id+'"><i class="fa fa-edit text-primary"></i></a><a data-href="#" data-toggle="modal" data-target="#myModal'+value.id+'"><i class="fa fa-trash text-danger"></i></a></td></tr>';

                
                            $(".remarks").append(remarks);
                        });
                    }
                });
            }
            // else{
            //     $('.kam-error2').show();
            //     $('.kam-success').hide();
            // }

        });
     });

    </script>

  </body>

</html>