  <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user"><!-- <img class="app-sidebar__user-avatar" src="../../assets/s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image"> -->
        <div>
          <p class="app-sidebar__user-name">{{ Auth::user()->name }}</p>
          <p class="app-sidebar__user-designation">Permission</p>
        </div>
      </div>
      <ul class="app-menu">
        <li><a class="app-menu__item active" href="{{route('home')}}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>

        <li><a class="app-menu__item active" href="{{route('students')}}"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">All Students</span></a></li>
        <li><a class="app-menu__item active" href="{{route('inactiveStudents')}}"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Inactive Students</span></a></li>
        <li><a class="app-menu__item active" href="{{route('teachers')}}"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label"> Teachers</span></a></li>
        <li><a class="app-menu__item active" href="{{route('discounted')}}"><i class="app-menu__icon fa fa-percent"></i><span class="app-menu__label"> Discounted Students</span></a></li>
        
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Students</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="{{route('form1')}}"><i class="icon fa fa-users"></i> Form One</a></li>
            <li><a class="treeview-item" href="{{route('form2')}}"><i class="icon fa fa-users"></i> Form Two</a></li>
            <li><a class="treeview-item" href="{{route('form3')}}"><i class="icon fa fa-users"></i> Form Three</a></li>
            <li><a class="treeview-item" href="{{route('form4')}}"><i class="icon fa fa-users"></i> Form Four</a></li>
          </ul>
        </li>    
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Academics</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">

            <li><a class="treeview-item" href="{{route('remarks')}}"><i class="icon fa fa-upload"></i> Remarks</a></li> 
            <li><a class="treeview-item" href="{{route('uploadMarks')}}"><i class="icon fa fa-upload"></i> Upload Marks</a></li>

            <li><a class="treeview-item" href="{{route('selectReport')}}"><i class="icon fa fa-download"></i> Transcripts</a></li>
          </ul>
        </li>  
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-cog"></i><span class="app-menu__label">Settings</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="{{route('voteheads')}}"><i class="icon fa fa-home"></i> Voteheads</a></li>
            <li><a class="treeview-item" href="{{route('voteheadAmounts')}}"><i class="icon fa fa-bank"></i> Votehead Amounts</a></li>
            <li><a class="treeview-item" href="{{route('semester')}}"><i class="icon fa fa-bars"></i> Semesters/Terms</a></li>
            <li><a class="treeview-item" href="{{route('assignedVotehead')}}"><i class="icon fa fa-laptop"></i> Assign Voteheads</a></li>
            <li><a class="treeview-item" href="{{route('streams')}}"><i class="icon fa fa-rss"></i> Streams</a></li>
            <li><a class="treeview-item" href="{{route('subjects')}}"><i class="icon fa fa-university"></i> Subjects</a></li>
          </ul>
        </li>
   
    

      </ul>
    </aside>