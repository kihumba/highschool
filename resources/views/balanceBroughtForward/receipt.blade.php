<style type="text/css">
  .fees{
    border-collapse: collapse;
    min-width: 100%;
    text-align: left;
  }

  .lists{
    text-align: center;
    list-style: none !important;
  }

  .first-element{
    font-weight: bold;
  }

  .location{
    font-size: 0.7em;
    text-align: center;
    font-weight: bold;
  }

  .college-detail{
    font-size: 0.9em;
  }

  .footer {
      position: fixed; 
      bottom: -40px; 
      left: 0px; 
      right: 0px;
      height: 50px;
  }
</style>

<div class="main">
    <div style="margin-left: -2em">
         <img src="images/img.png" alt="No header">
    </div>
  <hr color="green"/>

  <br/>

  <span style="float:left; font-weight: bold;">Receipt #: {{ $receipt_no }}</span><br><br>
  <span style="float:left; font-weight: bold;">Payment Date: {{date("j-M-Y", strtotime($datePaymentDone)) }}</span>
  <span style="float:right; font-weight: bold;">Date: {{ date("j-M-Y", strtotime($receiptDate)) }}</span>

  <br/><br/><br/>

  <span>Payment of fees received for <span style="font-weight: bold;">{{ $student}}</span> with particulars as follows:</span>

  <br/><br/><br/>
    <table>
          <tr>
            <td>Bank:</td>
            <td>
              @if($receiptData->payment_method == 1)
                <img style="width:20px;" src="images/checked.png">
              @else
                <img style="width:20px;" src="images/unchecked.png">
              @endif
            </td>
      
            <td>M-Pesa:</td>
            <td>
              @if($receiptData->payment_method == 2)
                <img style="width:20px;" src="images/checked.png">
              @else
                <img style="width:20px;" src="images/unchecked.png">
              @endif
            </td>
          
            <td>Cash:</td>
            <td>
              @if($receiptData->payment_method == 3)
                <img style="width:20px;" src="images/checked.png">
              @else
                <img style="width:20px;" src="images/unchecked.png">
              @endif
            </td>
          
            <td>Other:</td>
            <td>
              @if($receiptData->payment_method == 4)
                <img style="width:20px;" src="images/checked.png">
              @else
                <img style="width:20px;" src="images/unchecked.png">
              @endif
            </td>
          </tr>
  </table>
  <br/>
   <table border="1" class="fees">
    <tr>
      <th>DESCRIPTION</th>
      <th>INTIAL BALANCE</th>
      <th>AMOUNT PAID</th>
      <th>LAST TERM BALANCE <span style="font-style: italic;">(KSHS)</span></th>
    </tr>
    <tr>
        <td>LAST TERM FEE</td>
        <td>{{ number_format($payments_intialBalance, 2)}}</td>
        <td>{{ number_format($payments_amountPaid, 2)}}</td>
        <td>{{ number_format($payments_balance, 2)}}</td>
    </tr>


     </table>

  <br/><br/><br/>

  <span style="float:right;">
    <span style="font-weight: bold;">Served by:</span> {{ $servedBy }} <br/><br/><br/>
    <span style="font-weight: bold;">Signed:</span> _____________________
  </span>                       
                        
      
</div>                    
