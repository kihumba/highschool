@include('layouts.header')
@include('layouts.sidebar')
<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-users"></i> Assigned voteheds</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item"><a href="{{route('teachers')}}">Teachers</a></li>
          <li class="breadcrumb-item"><a href="#">Assigned voteheds</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-10 offset-md-1">
          <div class="tile">
            <div class="tile-body">
              <div class="col-md-12">
                <h5>Active voteheads</h5>
              <div class="row">
                <div class="col-md-3">
                  <h5>Form one<a href="{{ route('assignVotehead',1) }}" ><i class="fa fa-edit text-primary"></i></a></h5>
                   @foreach($activeVoteheads as $activeVotehead)
                    @if($activeVotehead->form_id == 1)
                   <p>{{$activeVotehead->votehead_name}}</p>
                    @endif
                   @endforeach
                  </div>
                  <div class="col-md-3">
                  <h5>Form Two<a href="{{ route('assignVotehead',2) }}" ><i class="fa fa-edit text-primary"></i></a></h5>
                   @foreach($activeVoteheads as $activeVotehead)
                    @if($activeVotehead->form_id == 2)
                     <p>{{$activeVotehead->votehead_name}}</p>
                    @endif
                   @endforeach
                  </div>
                  <div class="col-md-3">
                  <h5>Form Three<a href="{{ route('assignVotehead',3) }}" ><i class="fa fa-edit text-primary"></i></a></h5>
                   @foreach($activeVoteheads as $activeVotehead)
                    @if($activeVotehead->form_id == 3)
                     <p>{{$activeVotehead->votehead_name}}</p>
                    @endif
                   @endforeach
                  </div>
                   <div class="col-md-3">
                  <h5>Form Four<a href="{{ route('assignVotehead',4) }}" ><i class="fa fa-edit text-primary"></i></a></h5>
                   @foreach($activeVoteheads as $activeVotehead)
                    @if($activeVotehead->form_id == 4)
                       <p>{{$activeVotehead->votehead_name}}</p>
                    @endif
                   @endforeach
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
@include('layouts.footer')