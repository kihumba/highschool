@include('layouts.header')
@include('layouts.sidebar')
<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-users"></i> Assign voteheds</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item"><a href="{{route('teachers')}}">Teachers</a></li>
          <li class="breadcrumb-item"><a href="#">Assign voteheds</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-10 offset-md-1">
          <div class="tile">
            <div class="tile-body">

               <form method="post" action="{{route('saveActiveVoteheads')}}">
                @csrf
                <input type="hidden" name="formId" value="{{$id}}">
               <div class="row">
                @foreach($voteheads as $value)
                    <div class="col-md-3">
                        <label>
                            {{ Form::checkbox('votehead[]', $value->id, in_array($value->id, $formVoteheads) ? true : false, array('class' => 'votehead_id roleCheckbox')) }}
                            {{ $value->votehead_name }}
                        </label>
                    </div>
                @endforeach
            </div> 
                <div class="row" style="padding-top:1em;">
                  <div class="col-md-4">
                    <a href="{{route('assignedVotehead')}}" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Back to Assigned Voteheads</a>
                  </div>
                  
                  
                  <div class="col-md-4">
                    <button type="submit" class="btn btn-primary btn-block btn-sm"> Save & Continue  </button>
                  </div>
              </div>
            </form>
            </div>
          </div>
        </div>
      </div>
    </main>
@include('layouts.footer')