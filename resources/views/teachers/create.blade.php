@include('layouts.header')
@include('layouts.sidebar')
<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-users"></i> Add Teachers</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item"><a href="{{route('teachers')}}">Teachers</a></li>
          <li class="breadcrumb-item"><a href="#">Add teachers</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-8 offset-md-2">
          <div class="tile">
            <div class="tile-body">
                 <form method="POST" action="{{ route('saveTeacher') }}">
                  @csrf

                  @if (Session::has('message'))
                     <div class="alert alert-danger">{{ Session::get('message') }}</div>
                  @endif

                  <div class="row">
                      <div class="col-md-6 ">
                          <label>Full Name </label>
                      <input type="text"  class="form-control form-control-sm" name="teacher_name" required autocomplete="parent_name">
                      </div>

                      <div class="col-md-6">
                          <label>  Degree </label>
                      <input type="text" class="form-control form-control-sm" name="degree" required autocomplete="occupation">
                      </div>
                  </div>

                  <br/>

                  <div class="row">
                      <div class="col-md-6">
                          <label> National ID </label>
                          <input type="number"  required class="form-control form-control-sm" name="national_id" autocomplete="national_id">
                      </div>

                      <div class="col-md-6">
                          <label>  Phone Number </label>
                          <input type="text"   class="form-control form-control-sm" name="phone_number" autocomplete="phone_number" required>
                      </div>
                  </div>
                  <br>
                  <div class="row">
                     <div class="col-md-6">
                            <label> Gender </label>
                          <div class="input-group">
                            <select class="form-control form-control-sm" name="gender" style="padding:0px;">
                                <option selected disabled>Select Gender</option>
                                @foreach( $genders as $gender )
                                    <option value="{{ $gender->id }}">
                                        {{ $gender->gender }}
                                    </option>
                                @endforeach
                            </select>
                            </div>
                          
                        </div>
                        <div class="col-md-6">
                          <label> Registration Number<small></label>
                          <input type="number" required class="form-control form-control-sm" name="reg_number" autocomplete="Registration Number">
                      </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-6">
                          <label> Experience</label><small>(In years)/Optional</small> 
                          <input type="number" class="form-control form-control-sm" name="experience" autocomplete="Experience">
                      </div>
                      <div class="col-md-6">
                          <label> Previous Teaching School<small>(Optional)</small> </label>
                          <input type="text" class="form-control form-control-sm" name="previous_teaching_school" autocomplete="Previous Teaching School">
                      </div>
                  </div>


                  <br/>
                  
                  <div class="row" style="padding-top:1em;">
                      <div class="col-md-6">
                              <a href="{{route('teachers')}}" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Back to Teachers</a>                                                                    </div>
                      <div class="col-md-6">
                          <button type="submit" class="btn btn-primary btn-block btn-sm"> Save & Continue  </button>
                      </div>
                  </div>

              </form>



            </div>
          </div>
        </div>
      </div>
    </main>
     @include('layouts.footer')