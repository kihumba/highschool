@include('layouts.header')
@include('layouts.sidebar')

   <!-- Main Content Area -->
     <!-- Main Content Area -->
     <main class="app-content">
      <div class="app-title">
        <h1><i class="fa fa-th-list"></i>Teachers</h1>

        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item">Teachers</li>
          <li class="breadcrumb-item active"><a href="#">Teachers</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
                <a href="{{ route('createTeacher') }}" class="btn btn-rounded btn-primary mb-3" style="font-weight: bold; color: #fff;">+</a>

            <div class="table-responsive">
                <table class="table table-hover table-bordered nowrap" id="sampleTable">
                <thead>
                    <tr>
                        <th>Teacher's name</th>
                        <th>Reg Number</th>
                        <th>Phone Number</th>
                        <th>Gender</th>
                        <th>Degree</th>
                        <th>Joined on</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach( $teachers as $teacher )
                    <tr>
                      <td> {{ $teacher->teacher_name }}</td>
                      <td> {{ $teacher->reg_number }}</td>
                      <td> {{ $teacher->phone_number }}</td>
                      <td> {{ $teacher->gender }}</td>
                      <td> {{ $teacher->degree}}</td>
                      <td> {{ $teacher->created_at }}</td>
                      <td> @if($teacher->status == 0)
                          <span class="btn btn-sm btn-warning"><small>Inactive</small></span>
                          @else
                          <span class="btn btn-sm btn-success"><small>Active</small></span>
                          @endif
                        </td>

                     
                    
                      <td>
                          <a data-href="#" data-toggle="modal" data-target="#editTeacher{{$teacher->id}}"><i class="fa fa-edit text-primary"></i></a>&nbsp;&nbsp;
                          <a href="{{ route('formStreams',$teacher->id) }}" ><i class="fa fa-book text-primary"></i></a>&nbsp;&nbsp;
                          @if($teacher->status == 0)
                           <a data-href="#" data-toggle="modal" data-target="#activeTeacher{{$teacher->id}}"><i class="fa fa-power-off text-success"></i></a>&nbsp;&nbsp;

                          @else
                          <a data-href="#" data-toggle="modal" data-target="#deActiveTeacher{{$teacher->id}}"><i class="fa fa-power-off text-warning"></i></a>&nbsp;&nbsp;
                          @endif

                           <a data-href="#" data-toggle="modal" data-target="#deleteTeacher{{$teacher->id}}"><i class="fa fa-trash text-danger"></i></a>
                      </td>
                     
                    </tr>
                    <!-- ACTIVATE A TEACHER -->
                    <div class="modal fade" id="activeTeacher{{$teacher->id}}">
                      <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                          <div class="modal-content">
                              <div class="modal-header bg">
                                  <h5 class="modal-title">Activate </h5>
                                  <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                              </div>
                              <div class="modal-body">
                                <p>You are about to activate {{$teacher->teacher_name}}</p>
                                 <div class="row" style="padding-top:1em;">
                                  <div class="col-md-6">
                                          <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>                                                                    </div>
                                  <div class="col-md-6">
                                      <a href="{{route('activateTeacher', $teacher->id)}}" class="btn btn-success btn-block btn-sm"> Activate </a>
                                  </div>
                                </div>
                                 
                              </div>
                          </div>
                      </div>
                  </div> 
                  <!-- DEACTIVATE A TEACHER -->
                  <div class="modal fade" id="deActiveTeacher{{$teacher->id}}">
                      <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                          <div class="modal-content">
                              <div class="modal-header bg">
                                  <h5 class="modal-title">Deactivate </h5>
                                  <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                              </div>
                              <div class="modal-body">
                                <p>You are about to Deactivate {{$teacher->teacher_name}}</p>
                                 <div class="row" style="padding-top:1em;">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>                                                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{route('deactivateTeacher', $teacher->id)}}" class="btn btn-warning btn-block btn-sm">Deactivate </a>
                                    </div>
                                </div>
                                 
                              </div>
                          </div>
                      </div>
                  </div> 
                  <!-- DELETE A TEACHER -->
                  <div class="modal fade" id="deleteTeacher{{$teacher->id}}">
                      <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                          <div class="modal-content">
                              <div class="modal-header bg">
                                  <h5 class="modal-title">Confirm Delete </h5>
                                  <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                              </div>
                              <div class="modal-body">
                                <p>You are about to Delete {{$teacher->teacher_name}}</p>
                                 <div class="row" style="padding-top:1em;">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Close</button>                                                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{route('deleteTeacher', $teacher->id)}}" class="btn btn-danger btn-block btn-sm">Delete </a>
                                    </div>
                                </div>
                                 
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- EDIT A TEACHER -->
                    <div class="modal fade" id="editTeacher{{$teacher->id}}">
                      <div class="modal-dialog modal-dialog-centered" role="document">
                          <div class="modal-content">
                              <div class="modal-header bg">
                                  <h5 class="modal-title">EDIT {{ $teacher->teacher_name}} Record</h5>
                                  <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                              </div>
                              <div class="modal-body">
                                  <form method="POST" action="{{ route('editTeacher', $teacher->id)}}">
                                    @csrf

                                    @if (Session::has('message'))
                                       <div class="alert alert-danger">{{ Session::get('message') }}</div>
                                    @endif

                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <label>Full Name </label>
                                        <input type="text"  class="form-control form-control-sm" name="teacher_name" value="{{ $teacher->teacher_name }}" autocomplete="parent_name">
                                        </div>

                                        <div class="col-md-6">
                                            <label>  Degree </label>
                                        <input type="text" class="form-control form-control-sm" name="degree" value="{{$teacher->degree}}" autocomplete="occupation">
                                        </div>
                                    </div>

                                    <br/>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label> National ID </label>
                                            <input type="number" class="form-control form-control-sm" name="national_id" value="{{$teacher->national_id}}" autocomplete="national_id">
                                        </div>

                                        <div class="col-md-6">
                                            <label>  Phone Number </label>
                                            <input type="text"   class="form-control form-control-sm" name="phone_number" value="{{$teacher->phone_number}}" autocomplete="phone_number">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                       <div class="col-md-6">
                                              <label> Gender </label>
                                            <div class="input-group">
                                              <select class="form-control form-control-sm" name="gender" style="padding:0px;">
                                                @if($teacher->gender == "Male")
                                                  <option selected value="1">{{$teacher->gender}}</option>
                                                @else
                                                 <option selected value="2">{{$teacher->gender}}</option>

                                                @endif
                                                  @foreach( $genders as $gender )
                                                      <option value="{{ $gender->id }}">
                                                          {{ $gender->gender }}
                                                      </option>
                                                  @endforeach
                                              </select>
                                              </div>
                                            
                                          </div>
                                          <div class="col-md-6">
                                            <label> Registration Number<small></label>
                                            <input type="number" required class="form-control form-control-sm" name="reg_number" value="{{$teacher->reg_number}}" autocomplete="Registration Number">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                      <div class="col-md-6">
                                            <label> Experience</label><small>(In years)/Optional</small> 
                                            <input type="number" class="form-control form-control-sm" name="experience" value="{{$teacher->experience}}" autocomplete="Experience">
                                        </div>
                                        <div class="col-md-6">
                                            <label> Previous Teaching School<small>(Optional)</small> </label>
                                            <input type="text" class="form-control form-control-sm" name="previous_teaching_school" value="{{$teacher->previous_teaching_school}}" autocomplete="Previous Teaching School">
                                        </div>
                                    </div>


                                    <br/>
                                    
                                    <div class="row" style="padding-top:1em;">
                                        <div class="col-md-6">
                                                <button type="submit" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">close</button>                                                                    </div>
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-primary btn-block btn-sm"> Save & Continue  </button>
                                        </div>
                                    </div>

                                </form>

                                 
                              </div>
                          </div>
                      </div>
                  </div> 
                    @endforeach
                </tbody>
              </table>
            </div>
         </div>
    </div>
</div>
</div>

     
</main>

       @include('layouts.footer')

