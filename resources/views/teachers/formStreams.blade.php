@include('layouts.header')
@include('layouts.sidebar')
<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-users"></i> Assign Subjects to {{$teacherName}}</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item"><a href="{{route('teachers')}}">Teachers</a></li>
          <li class="breadcrumb-item"><a href="#">Assign teachers</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-10 offset-md-1">
          <div class="tile">
            <div class="tile-body">
   
            <br>
            <div class="col-md-12">
                  @if (Session::has('message'))
                  <div class="alert alert-danger">{{ Session::get('message') }}</div>
                  @endif
              <div class="row">
                <div class="col-md-3">
                  <h5>Form one streams</h5>
                   @foreach($streams as $stream)
                    @if($stream->form_id == 1)
                    <form method="post" action="{{route('assignSubjects')}}">
                      @csrf
                         <input type="hidden" name="teacherId" value="{{$id}}">
                        <input type="hidden" name="formId" value="1">
                        <input type="hidden" name="streamId" value="{{$stream->id}}">
                        <button type="submit" class="btn btn-sm btn-link" style="text-decoration: none" >{{$stream->stream_name}}&nbsp;
                        <?php 
                           $formStreamSubjects = DB::table('teacher_subjects')
                              ->where('teacher_id', $id)
                              ->where('stream_id', $stream->id)
                              ->first();

                         ?>
                        @if($formStreamSubjects)
                        <img src="/images/checked.png" alt="checked" style="height: 1em;width: 1em">
                        @else
                        <img src="/images/unchecked.png" alt="unchecked" style="height: 1em;width: 1em">
                        @endif
                        </button>


                    </form>
                    @endif
                   @endforeach
                  
                </div>
                 <div class="col-md-3">
                  <h5>Form Two streams</h5>
                   @foreach($streams as $stream)
                    @if($stream->form_id == 2)
                    <form method="post" action="{{route('assignSubjects')}}">
                      @csrf
                         <input type="hidden" name="teacherId" value="{{$id}}">
                        <input type="hidden" name="formId" value="2">
                        <input type="hidden" name="streamId" value="{{$stream->id}}">
                        <button type="submit" class="btn btn-sm btn-link" style="text-decoration: none;">{{$stream->stream_name}}&nbsp;
                         <?php 
                           $formStreamSubjects = DB::table('teacher_subjects')
                              ->where('teacher_id', $id)
                              ->where('stream_id', $stream->id)
                              ->first();

                         ?>
                        @if($formStreamSubjects)
                        <img src="/images/checked.png" alt="checked" style="height: 1em;width: 1em">
                        @else
                        <img src="/images/unchecked.png" alt="unchecked" style="height: 1em;width: 1em">
                        @endif
 
                        </button>
                    </form>
                    @endif
                   @endforeach
                </div>
                 <div class="col-md-3">
                  <h5>Form Three streams</h5>
                   @foreach($streams as $stream)
                    @if($stream->form_id == 3)
                    <form method="post" action="{{route('assignSubjects')}}">
                      @csrf
                         <input type="hidden" name="teacherId" value="{{$id}}">
                        <input type="hidden" name="formId" value="3">
                        <input type="hidden" name="streamId" value="{{$stream->id}}">
                        <button type="submit" class="btn btn-sm btn-link" style="text-decoration: none;">{{$stream->stream_name}}&nbsp;
                         <?php 
                           $formStreamSubjects = DB::table('teacher_subjects')
                              ->where('teacher_id', $id)
                              ->where('stream_id', $stream->id)
                              ->first();

                         ?>
                        @if($formStreamSubjects)
                        <img src="/images/checked.png" alt="checked" style="height: 1em;width: 1em">
                        @else
                        <img src="/images/unchecked.png" alt="unchecked" style="height: 1em;width: 1em">
                        @endif
                      </button>
                    </form>
                    @endif
                   @endforeach
                </div>
                 <div class="col-md-3">
                  <h5>Form four streams</h5>
                   @foreach($streams as $stream)
                    @if($stream->form_id == 4)
                    <form method="post" action="{{route('assignSubjects')}}">
                      @csrf
                         <input type="hidden" name="teacherId" value="{{$id}}">
                        <input type="hidden" name="formId" value="4">
                        <input type="hidden" name="streamId" value="{{$stream->id}}">
                        <button type="submit" class="btn btn-sm btn-link" style="text-decoration: none;">{{$stream->stream_name}}&nbsp;
                         <?php 
                           $formStreamSubjects = DB::table('teacher_subjects')
                              ->where('teacher_id', $id)
                              ->where('stream_id', $stream->id)
                              ->first();

                         ?>
                        @if($formStreamSubjects)
                        <img src="/images/checked.png" alt="checked" style="height: 1em;width: 1em">
                        @else
                        <img src="/images/unchecked.png" alt="unchecked" style="height: 1em;width: 1em">
                        @endif
                      </button>
                    </form>         
                    @endif
                   @endforeach
                </div>
                
              </div>
               

           
            </div>

      

            </div>
          </div>
        </div>
      </div>
    </main>
     @include('layouts.footer')