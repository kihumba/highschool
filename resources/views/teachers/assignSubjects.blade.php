@include('layouts.header')
@include('layouts.sidebar')
<main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-users"></i> Assign Subjects to {{$teacherName}}</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item"><a href="{{route('teachers')}}">Teachers</a></li>
          <li class="breadcrumb-item"><a href="#">Assign teachers</a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-10 offset-md-1">
          <div class="tile">
            <div class="tile-body">
   
            <br>
            <div class="col-md-12">
              <h5>{{$streamName}}</h5>
            <div class="">
               <form method="post" action="{{route('saveTeacherSubject')}}">
                @csrf
                <input type="hidden" name="teacherId" value="{{$teacherId}}">
                <input type="hidden" name="formId" value="{{$formId}}">
                <input type="hidden" name="streamId" value="{{$streamId}}">
               <div class="row">
                @foreach($subjects as $value)
                    <div class="col-md-3">
                        <label>
                            {{ Form::checkbox('subject[]', $value->id, in_array($value->id, $formSubjects) ? true : false, array('class' => 'subject_id roleCheckbox')) }}
                            {{ $value->subject_name }}
                        </label>
                    </div>
                @endforeach
            </div> 
                <div class="row" style="padding-top:1em;">
                  <div class="col-md-4">
                    <a href="{{route('teachers')}}" class="btn btn-secondary btn-block btn-sm" data-dismiss="modal">Back to Teachers</a>
                  </div>
                   <div class="col-md-4">
                    <a href="{{route('formStreams', $teacherId)}}" class="btn btn-info btn-block btn-sm" data-dismiss="modal">Back to all stream</a>
                  </div>
                  
                  <div class="col-md-4">
                    <button type="submit" class="btn btn-primary btn-block btn-sm"> Save & Continue  </button>
                  </div>
              </div>
            </form>
                
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
     @include('layouts.footer')