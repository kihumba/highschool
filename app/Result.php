<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
     protected $fillable = [
  	'cat_one',
  	'cat_two',
  	'end_term',
  	'student_id',
  	'semester_id',
  	'form_id',
  	'stream_id',
  	'subject_id'

  ];
}
