<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parentmodel extends Model
{
    protected $fillable = [
		'parent_name',
		'phone_nummer',
		'national_id',
		'occupation'
	];

	protected $table = "parents";
}
