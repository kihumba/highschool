<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Auth;
use PDF;
use App\Student;
use App\Remark;

class RemarksController extends Controller
{
   public function remarks(){
    $remarks = DB::table('remarks')
        ->join('users', 'users.id', '=', 'remarks.user_id')
            ->select(
                'remarks.remark',
                'remarks.grade',
                'remarks.min_marks',
                'remarks.max_marks',
                'remarks.id',
                'users.name'
            )
            ->orderBy('id', 'DESC')
            ->get();
    return view('academics.remarks.index')
        ->with('remarks', $remarks);
}

 public function saveRemarks(Request $request){
    if( $request->ajax()){
        $remark=new Remark();
        $remark->remark = $request->get('remark');
        $remark->grade = $request->get('grade');
        $remark->max_marks = $request->get('max_marks');
        $remark->min_marks = $request->get('min_marks');
        $remark->user_id = Auth::user()->id;
        $remark->save();

    $remarks = Remark::join('users', 'users.id', '=', 'remarks.user_id')
        ->select(
            'remarks.remark',
            'remarks.min_marks',
            'remarks.max_marks',
            'remarks.grade',
            'remarks.id',
            'users.name'
        )
        ->orderBy('id', 'DESC')
        ->get();

        return json_encode($remarks);
    }
    }

    public function updateRemark( Request $request, $id){
        Remark::where('id',$id)
        ->update([
            'remark' => $request->input('remark'),
            'min_marks' => $request->input('min_marks'),
            'max_marks' => $request->input('max_marks'),
            'grade' => $request->input('grade'),
            'user_id' => Auth::user()->id

        ]);


        return redirect()->back();
    }

     public function deleteRemark($id){
        Remark::where('id',$id)->delete();
        return redirect()->back();
    }
}
