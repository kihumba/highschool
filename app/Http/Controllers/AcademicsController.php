<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Auth;
use PDF;
use App\Student;
use App\Remark;

class AcademicsController extends Controller
{
    public function uploadMarks(){
    	$forms = DB::table('forms')->get();
    	$subjects = DB::table('subjects')->get();

    	return view('academics.uploadMarks')
    		->with('subjects', $subjects)
    		->with('forms', $forms);
    } 

    public function getStreams(Request $request){
    	if( $request->ajax()){
            $formId = $request->get('formId');
            Session::put('results_formId', $formId);
            $streams = DB::table('streams')->where('form_id', $formId)->get();

	        return json_encode($streams);
        }


}


    public function getStudents( Request $request ){
    	 if( $request->ajax()){
    	 	
	    	$semesterId = DB::table('semesters')
		    	->where('current', '=', 1)
		    	->pluck('id')
		    	->first();

	    	$subjectId = $request->get('subjectId');
	    	$streamId = $request->get('streamId');

	    	Session::put('subject_id', $subjectId);
	    	Session::put('stream_id', $streamId);
	    	Session::put('exam_series', $request->get('examSeries'));

	    	$formId = Session::get('results_formId');

	    	$students = Student::where('form_id', $formId)->where('stream_id', $streamId)->get();

	    	$existingStudents = DB::table('results')
	    	->join('students', 'students.id', '=', 'results.student_id')
	    	->where('results.form_id', $formId)
	    	->where('results.subject_id', $subjectId)
	    	->where('results.semester_id', $semesterId)
	    	->where('results.stream_id', $streamId)
	    	->select(
	    		'students.student_name',
	    		'students.admission_number',
	    		'results.cat_one',
	    		'results.cat_two',
	    		'results.end_term'
	    	)
	    	->get();

	    	$context = [
	    		'students' => $students,
	    		'existingStudents' => $existingStudents
	    	];

	    	return json_encode( $context );

	    }

    }


       public function savemarks( Request $request ){
       	 if( $request->ajax()){
	    	$formId = Session::get('results_formId');
	    	// $subjectId = Session::get('subject_id');
	    	$examSeries = Session::get('exam_series');
	    	$streamId = Session::get('stream_id');

	    	$marks = $request->get('marks');
	    	$subjectId = $request->get('subjectId');
	    	$studentId = $request->get('studentId');

	    	$semesterId = DB::table('semesters')
		    	->where('current', '=', 1)
		    	->pluck('id')
		    	->first();

		    $recordExists = DB::table('results')
			    ->where('student_id', $studentId )
			    ->where('semester_id', $semesterId )
			    ->where('subject_id', $subjectId )
			    ->first();


			if( $examSeries == 1 ){
				$catone = $marks;
				$cattwo = NULL;
				$endterm = NULL;
			}elseif( $examSeries == 2 ){
				$catone = NULL;
				$cattwo = $marks;
				$endterm = NULL;
			}elseif( $examSeries == 3 ){
				$catone = NULL;
				$cattwo = NULL;
				$endterm = $marks;
			}


			if( !$recordExists ){

				DB::table('results')->insert([
					'cat_one' => $catone,
					'cat_two' => $cattwo,
					'end_term' => $endterm,
					'student_id' => $studentId,
					'semester_id' => $semesterId,
					'stream_id' => $streamId,
					'form_id' => $formId,
					'subject_id' => $subjectId
				]);
			}else{

				if( $examSeries == 1 ){
					DB::table('results')->where('student_id', $studentId )
				    	->where('subject_id', $subjectId )
				    	->where('semester_id', $semesterId )
						->update([
							'cat_one' => $catone
					]);
				}elseif( $examSeries == 2 ){
					DB::table('results')->where('student_id', $studentId )
				    	->where('subject_id', $subjectId )
				    	->where('semester_id', $semesterId )
						->update([
							'cat_two' => $cattwo
					]);
				}elseif( $examSeries == 3 ){
					DB::table('results')->where('student_id', $studentId )
						->where('semester_id', $semesterId )
				    	->where('subject_id', $subjectId )
						->update([
							'end_term' => $endterm
					]);
				}

				
			}


			$students = DB::table('results')
	    	->join('students', 'students.id', '=', 'results.student_id')
	    	->where('results.form_id', $formId)
	    	->where('results.subject_id', $subjectId )
	    	->where('results.stream_id', $streamId )
	    	->where('results.semester_id', $semesterId )
	    	->select(
	    		'students.student_name',
	    		'students.admission_number',
	    		'results.cat_one',
	    		'results.cat_two',
	    		'results.end_term'
	    	)
	    	->get();


		    return json_encode( $students );
		}

    }
   

    public function selectReport(){
    	$semesters = DB::table('semesters')->get();
    	$forms = DB::table('forms')->get();

    	return view('academics.selectReport')
    		->with('forms', $forms)
    		->with('semesters', $semesters);
    }

    public function studentsData(Request $request){
	    $admNumber = $request->get('admission');

	    $student = Student::where('admission_number', $admNumber)
	    	->join('genders', 'genders.id', '=', 'students.gender_id')
	    	->select(
	    		'students.id',
	    		'students.student_name',
	    		'students.admission_number',
	    		'genders.gender'
	    	)
	    	->first();

	    return json_encode( $student );

    }

     public function getResults(Request $request){

        $semesterId = $request->get('semesterId');
        $studentId = $request->get('studentId');

        Session::put('semester_id', $semesterId);
        Session::put('studentId', $studentId);

        if($semesterId == "all"){

        $results = DB::table('results')
            ->join('students', 'students.id', '=', 'results.student_id')
            ->join('subjects', 'subjects.id', '=', 'results.subject_id')
            ->where('results.student_id', $studentId)
            ->get();
        }else{
        $results = DB::table('results')
            ->join('students', 'students.id', '=', 'results.student_id')
            ->join('subjects', 'subjects.id', '=', 'results.subject_id')
            ->where('results.semester_id', $semesterId)
            ->where('results.student_id', $studentId)
            ->get();


        }

        return json_encode( $results );
    }


    public function transcript(){
        $semesterId = Session::get('semester_id');
        $studentId = Session::get('studentId');

        $student = Student::join('results', 'students.id', '=', 'results.student_id')
            ->join('forms', 'forms.id', '=', 'students.form_id')
            ->join('streams', 'streams.id', '=', 'students.stream_id')
            ->join('semesters', 'semesters.id', '=', 'results.semester_id')
            ->select(
                'students.student_name',
                'students.admission_number',
                'students.created_at',
                'forms.form_name',
                'streams.stream_name',
                'semesters.semester_name'
            )
            ->where('students.id', $studentId)
            ->first();


        if($semesterId == "all"){   
            $results = DB::table('results')
                ->join('students', 'students.id', '=', 'results.student_id')
                ->join('subjects', 'subjects.id', '=', 'results.subject_id')
                ->where('results.student_id', $studentId )
                ->select(
                    'results.cat_one',
                    'results.cat_two',
                    'results.end_term',
                    'subjects.subject_name'
                )
                ->get();
                $totalMarks = 0;
        }else{
            $results = DB::table('results')
                ->join('students', 'students.id', '=', 'results.student_id')
                ->join('subjects', 'subjects.id', '=', 'results.subject_id')
                ->where('results.semester_id', $semesterId)
                ->where('results.student_id', $studentId )
                ->select(
                    'results.cat_one',
                    'results.cat_two',
                    'results.end_term',
                    'subjects.subject_name'
                )
                ->get();
             $studentForm = DB::table('students')->where('id',$studentId)->pluck('form_id')->first();
     
             $catOne = DB::table('results')
                ->where('semester_id', $semesterId)
                ->where('student_id', $studentId )
                ->pluck('cat_one')
                ->sum();
            $catTwo = DB::table('results')
                ->where('semester_id', $semesterId)
                ->where('student_id', $studentId )
                ->pluck('cat_two')
                ->sum();
            $endTerm = DB::table('results')
                ->where('semester_id', $semesterId)
                ->where('student_id', $studentId )
                ->pluck('end_term')
                ->sum();
            $totalMarks = $catOne + $catTwo + $endTerm;
           
            	if ($studentForm == 1) {
            		$gradeStudent=$totalMarks/7;
            	}elseif ($studentForm == 2) {
            		$gradeStudent=$totalMarks/7;
            	}elseif ($studentForm == 3) {
            		$gradeStudent=$totalMarks/7;
            	}elseif ($studentForm == 4) {
            		$gradeStudent=$totalMarks/7;
            	}
           
        	$studentCount = DB::table('students')->where('form_id', $studentForm)->count();
        	$catAllOne = DB::table('results')
                ->where('semester_id', $semesterId)
                ->where('form_id', $studentForm )
                ->pluck('cat_one')
                ->sum();
            $catAllTwo = DB::table('results')
                ->where('semester_id', $semesterId)
                ->where('form_id', $studentForm )
                ->pluck('cat_two')
                ->sum();
            $endAllTerm = DB::table('results')
                ->where('semester_id', $semesterId)
                ->where('form_id', $studentForm )
                ->pluck('end_term')
                ->sum();
            $totalAllMarks = $catAllOne + $catAllTwo + $endAllTerm;
        	$totalClassMarks = ($totalAllMarks/$studentCount);
        	$totalClassMarksGrade = ($totalClassMarks/$totalAllMarks)*100;
    	}
        $remarks = DB::table('remarks')->orderBy('id', 'ASC')->get();
				
            // dd($studentId);

        $transcript = "transcript";

        $data = [
            'name' => $student->student_name,
            'reg_num' => $student->admission_number,
            'formName' => $student->form_name,
            'streamName' => $student->stream_name,
            'semester' => $student->semester_name,
            'adm_date' => $student->created_at,
            'totalClassMarks' => $totalClassMarks,
            'totalClassMarksGrade' => $totalClassMarksGrade,
            'totalMarks' => $totalMarks,
            'remarks' => $remarks,
            'gradeStudent' => $gradeStudent,
            'results' => $results
        ];

        $pdf = PDF::loadView("academics." .$transcript, $data);
        return $pdf->stream('transcript.pdf');
}


public function getFormIdStreamId(Request $request){
	if( $request->ajax()){
		$streamId = $request->get('streamId');
		 $formId = $request->get('formId');

		return json_encode([
			'streamId' => $streamId,
			'formId' => $formId
		]);
	}

}

public function printTranscripts($streamId, $formId){


    $semesterId = DB::table('semesters')
        ->where('current', 1)
        ->pluck('id')
        ->first();

        if($streamId == "all"){
        	$studentIds = DB::table('students')->where('students.form_id', $formId)->pluck('id')->toArray();
 
        }else{

        	$studentIds = DB::table('students')->where('students.stream_id', $streamId)->pluck('id')->toArray();

    	}
		$remarks = DB::table('remarks')->orderBy('id', 'ASC')->get();	
            // dd($studentIds);

        $transcript = "allTranscript";

        $data = [
            'studentIds' => $studentIds,
            'remarks' => $remarks,
            'streamId' => $streamId,
            'formId' => $formId,
            'semesterId' => $semesterId
        ];

        $pdf = PDF::loadView("academics." .$transcript, $data);
        return $pdf->stream('transcript.pdf');

}


}
