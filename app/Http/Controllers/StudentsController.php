<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;
use DB;
use App\Student;
use App\Payment;
use App\User;
use App\Parentmodel;

class StudentsController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function students(){
         Session::forget('receipt_number');
        Session::forget('assigned');
    	$students= Student::join('forms','forms.id','=','students.form_id')
			->join('counties', 'counties.id', '=', 'students.county_id')
			->join('streams', 'streams.id', '=', 'students.stream_id')
			->join('genders', 'genders.id', '=', 'students.gender_id')
            ->select(
                'students.student_name',
                'students.admission_number',
                'students.kcpe_index_number',
                'students.form_id',
                'students.dob',
                'genders.gender',
                'counties.county_name',
                'streams.stream_name',
                'students.grade',
                'students.id'
            )
            ->get();
        $forms = DB::table('forms')->get();
    	return view('students.index')
    		->with('forms', $forms)
            ->with('students', $students);

    } 
  public function inactiveStudents(){
         Session::forget('receipt_number');
        Session::forget('assigned');
        $students= Student::join('forms','forms.id','=','students.form_id')
            ->join('counties', 'counties.id', '=', 'students.county_id')
            ->join('streams', 'streams.id', '=', 'students.stream_id')
            ->join('genders', 'genders.id', '=', 'students.gender_id')
            ->select(
                'students.student_name',
                'students.admission_number',
                'students.kcpe_index_number',
                'students.form_id',
                'students.dob',
                'genders.gender',
                'counties.county_name',
                'streams.stream_name',
                'forms.form_name',
                'students.grade',
                'students.id'
            )
            ->where('status', 0)
            ->get();
        $forms = DB::table('forms')->get();
        return view('students.inactiveStudents')
            ->with('forms', $forms)
            ->with('students', $students);

    } 

	public function form1(){
         Session::forget('receipt_number');
        Session::forget('assigned');
		$students= Student::join('forms','forms.id','=','students.form_id')
			->join('counties', 'counties.id', '=', 'students.county_id')
			->join('streams', 'streams.id', '=', 'students.stream_id')
			->join('genders', 'genders.id', '=', 'students.gender_id')
			->where('students.form_id', 1)		
            ->select(
                'students.student_name',
                'students.status',
                'students.admission_number',
                'students.kcpe_index_number',
                'students.form_id',
                'students.dob',
                'genders.gender',
                'counties.county_name',
                'streams.stream_name',
                'students.grade',
                'students.id'
            )
            ->where('status', 1)
            ->get();
        $forms = DB::table('forms')->get();
	    	return view('students.form1')
                ->with('forms', $forms)
	    		->with('students', $students);
	    } 
	public function form2(){
        Session::forget('receipt_number');
        Session::forget('assigned');
	    $students= Student::join('forms','forms.id','=','students.form_id')
	    	->join('counties', 'counties.id', '=', 'students.county_id')
			->join('streams', 'streams.id', '=', 'students.stream_id')
			->join('genders', 'genders.id', '=', 'students.gender_id')
			->where('students.form_id', 2)		
            ->select(
                'students.student_name',
                'students.admission_number',
                'students.kcpe_index_number',
                'students.form_id',
                'students.dob',
                'genders.gender',
                'counties.county_name',
                'streams.stream_name',
                'students.grade',
                'students.status',
                'students.id'
            )
            ->where('status', 1)
            ->get();
        $forms = DB::table('forms')->get();
	    	return view('students.form2')
                ->with('forms', $forms)
	    		->with('students', $students);
	    } 
	public function form3(){
         Session::forget('receipt_number');
        Session::forget('assigned');
    	$students= Student::join('forms','forms.id','=','students.form_id')
    		->join('counties', 'counties.id', '=', 'students.county_id')
			->join('streams', 'streams.id', '=', 'students.stream_id')
			->join('genders', 'genders.id', '=', 'students.gender_id')
			->where('students.form_id', 3)		
	        ->select(
	            'students.student_name',
                'students.form_id',
	            'counties.county_name',
	            'streams.stream_name',
	            'students.kcpe_index_number',
	            'students.admission_number',
	            'students.dob',
	            'genders.gender',
	            'students.grade',
	             'students.status',
                'students.id'
            )
            ->where('status', 1)
	        ->get();
        $forms = DB::table('forms')->get();
    	return view('students.form3')
            ->with('forms', $forms)
    		->with('students', $students);
	    } 
	public function form4(){
         Session::forget('receipt_number');
        Session::forget('assigned');
	    	$students= Student::join('forms','forms.id','=','students.form_id')
	    		->join('counties', 'counties.id', '=', 'students.county_id')
				->join('streams', 'streams.id', '=', 'students.stream_id')
				->join('genders', 'genders.id', '=', 'students.gender_id')
				->where('students.form_id', 4)	
	            ->select(
	                'students.student_name',
	                'students.kcpe_index_number',
                    'students.form_id',
	                'students.admission_number',
	                'counties.county_name',
	                'streams.stream_name',
	                'students.dob',
	                'students.grade',
	                'genders.gender',
	               'students.status',
                    'students.id'
                )
                ->where('status', 1)
	            ->get();
        $forms = DB::table('forms')->get();
	    	return view('students.form4')
                ->with('forms', $forms)
	    		->with('students', $students);
	    } 

    public function addstudent($formId){
    	$relationships = DB::table('student_parent_relationship')->get();
    	$parents = DB::table('parents')->get();
    	$counties = DB::table('counties')->get();
    	$forms = DB::table('forms')->get();
    	$genders = DB::table('genders')->get();
    	$streams = DB::table('streams')
    		->where('form_id', $formId)
    		->get();
    		// dd($formId);
    	return view('students.create')
    		->with('relationships', $relationships)
    		->with('forms', $forms)
    		->with('streams', $streams)
    		->with('genders', $genders)
    		->with('counties', $counties)
    		->with('parents', $parents)
    		->with('formId', $formId);
    }
    public function savestudent(Request $request){
    	$formId = $request->input('formId');

        $oldStudents = $request->input('oldStudents');
        $status = $request->input('status');
        $joinedYear = $request->input('yearOld');


        if($oldStudents){

    	 $studentExists = Student::where('admission_number', $request->input('admission_number') )
                ->first();

            if( !$studentExists ){
                $year = $request->input('year');
                $month = $request->input('month') + 1;
                $day = $request->input('dob');

                if( checkdate($month, $day, $year) ){
                    $student = new Student();
                    $student->student_name = $request->input('student_name');
                    $student->admission_number = $request->input('admission_number');
                    $student->kcpe_index_number	 = $request->input('index_number');
                    $student->dob = $year . "-" . $month . "-" . $day;
                    $student->parent_id = $request->input('parent');
                    $student->form_id = $formId;
                    $student->stream_id = $request->input('streamId');
                    $student->county_id = $request->input('countyId');
                    $student->jointed_year = $joinedYear;
                    $student->parent_relationship_id = $request->input('relationship');
                    $student->gender_id = $request->input('gender');
                    $student->status = $status;
                    $student->grade = $request->input('grade');
                    $student->save();


                    $currentSemester = DB::table('semesters')
                        ->where('current', 1)
                        ->pluck('id')
                        ->first();

                     //register student in payments table
                    $voteheadAmountIds = DB::table('form_voteheads')
                        ->where('form_id', $formId)
                        ->where('semester_id', $currentSemester)
                        ->pluck('votehead_amount_id')
                        ->toArray();

                    $voteheadDetails = DB::table('votehead_amounts')
                        ->whereIn('id', $voteheadAmountIds)
                        ->get();

                   

                        foreach ($voteheadDetails as $key => $voteheadamountid) {

                            DB::table('payments')
                                ->insert([
                                    'year' => date('Y'),
                                    'amount' => 0,
                                    'balance' => $voteheadamountid->amount,
                                    'semester_id' => $currentSemester,
                                    'student_id' => $student->id,
                                    'votehead_id' => $voteheadamountid->votehead_id
                                ]);
                                
                        }
            
                  


                    // START OF CREATE USER FROM THE STUDENT
                        // $studentPassword = str_random(5);

                        // $studentUser = new User();
                        // $studentUser->name = $request->input('student_name');
                        // $studentUser->username = $request->input('admission_number');
                        // $studentUser->password = Hash::make($studentPassword);
                        // $studentUser->save();

                        // DB::table('model_has_roles')->insert([
                        //     'role_id' => 3,
                        //     'model_type' => "App\User",
                        //     'model_id' => $studentUser->id
                        // ]);
                    // END OF CREATE USER FROM THE STUDENT
                        //SAVE PASSWORD   
                            // DB::table('pass_2020')->insert([
                            //     'username' => $studentUser->username,
                            //     'pass' => $studentPassword
                            // ]);

                        // END OF USER PASSWORD
                    // START OF STORE THE STUDENT' BALANCE BROUGHT FORWARD
                  
                    // END OF STORE THE STUDENT' BALANCE BROUGHT FORWARD


                // START OF SEND PASSWORD TO STUDENT
                   

                // END OF SEND PASSWORD TO STUDENT


                // START OF SEND PASSWORD TO PARENTS

                // END OF SEND PASSWORD TO PARENTS

                    if($formId == 1){
                    	return redirect()->route('form1', $formId);
                    }
                    else if($formId == 2){
                    	return redirect()->route('form2', $formId);

                    }
                    else if($formId == 3){
                    	return redirect()->route('form3', $formId);

                    } 
                    else if($formId == 4){
                    	return redirect()->route('form4', $formId);
                    }



            }else{
                Session::flash('message', "Please select a valid date!");
                return redirect()->back()
                ->with('formId', $formId);
            }
        }else{
            Session::flash('message', "This Admission Number is registered to " . $studentExists->student_name );
            return redirect()->back()
            ->with('formId', $formId);
        }
            //New Students
        }else{
             $studentExists = Student::where('admission_number', $request->input('admission_number') )
                ->first();

            if( !$studentExists ){
                $year = $request->input('year');
                $month = $request->input('month') + 1;
                $day = $request->input('dob');

                if( checkdate($month, $day, $year) ){
                    $student = new Student();
                    $student->student_name = $request->input('student_name');
                    $student->admission_number = $request->input('admission_number');
                    $student->kcpe_index_number  = $request->input('index_number');
                    $student->dob = $year . "-" . $month . "-" . $day;
                    $student->parent_id = $request->input('parent');
                    $student->form_id = $formId;
                    $student->stream_id = $request->input('streamId');
                    $student->county_id = $request->input('countyId');
                    $student->jointed_year = date('Y');
                    $student->parent_relationship_id = $request->input('relationship');
                    $student->gender_id = $request->input('gender');
                    $student->status = 1;
                    $student->grade = $request->input('grade');
                    $student->save();

                    $currentSemester = DB::table('semesters')
                        ->where('current', 1)
                        ->pluck('id')
                        ->first();

                     //register student in payments table
                    $voteheadAmountIds = DB::table('form_voteheads')
                        ->where('form_id', $formId)
                        ->where('semester_id', $currentSemester)
                        ->pluck('votehead_amount_id')
                        ->toArray();

                    $voteheadDetails = DB::table('votehead_amounts')
                        ->whereIn('id', $voteheadAmountIds)
                        ->get();

                   

                        foreach ($voteheadDetails as $key => $voteheadamountid) {

                            DB::table('payments')
                                ->insert([
                                    'year' => date('Y'),
                                    'amount' => 0,
                                    'balance' => $voteheadamountid->amount,
                                    'semester_id' => $currentSemester,
                                    'student_id' => $student->id,
                                    'votehead_id' => $voteheadamountid->votehead_id
                                ]);
                                
                        }
            
                  


                    // START OF CREATE USER FROM THE STUDENT
                        // $studentPassword = str_random(5);

                        // $studentUser = new User();
                        // $studentUser->name = $request->input('student_name');
                        // $studentUser->username = $request->input('admission_number');
                        // $studentUser->password = Hash::make($studentPassword);
                        // $studentUser->save();

                        // DB::table('model_has_roles')->insert([
                        //     'role_id' => 3,
                        //     'model_type' => "App\User",
                        //     'model_id' => $studentUser->id
                        // ]);
                    // END OF CREATE USER FROM THE STUDENT
                        //SAVE PASSWORD   
                            // DB::table('pass_2020')->insert([
                            //     'username' => $studentUser->username,
                            //     'pass' => $studentPassword
                            // ]);

                        // END OF USER PASSWORD
                    // START OF STORE THE STUDENT' BALANCE BROUGHT FORWARD
                  
                    // END OF STORE THE STUDENT' BALANCE BROUGHT FORWARD


                // START OF SEND PASSWORD TO STUDENT
                   

                // END OF SEND PASSWORD TO STUDENT


                // START OF SEND PASSWORD TO PARENTS

                // END OF SEND PASSWORD TO PARENTS

                    if($formId == 1){
                        return redirect()->route('form1', $formId);
                    }
                    else if($formId == 2){
                        return redirect()->route('form2', $formId);

                    }
                    else if($formId == 3){
                        return redirect()->route('form3', $formId);

                    } 
                    else if($formId == 4){
                        return redirect()->route('form4', $formId);
                    }



            }else{
                Session::flash('message', "Please select a valid date!");
                return redirect()->back()
                ->with('formId', $formId);
            }
        }else{
            Session::flash('message', "This Admission Number is registered to " . $studentExists->student_name );
            return redirect()->back()
            ->with('formId', $formId);
        }


        }


    }

     public function saveparent( Request $request, $id){
        $phoneNo = $request->input('phone_number');

            $parent = new Parentmodel();
            $parent->parent_name = $request->input('parent_name');
            $parent->national_id = $request->input('national_id');
            $parent->phone_number = $phoneNo;
            $parent->occupation = $request->input('occupation');
            $parent->save();

          // START OF CREATE USER FROM THE PARENTS
            // $password = str_random(5);

            // $parentUser = new User();
            // $parentUser->name = $request->input('full_name');
            // $parentUser->username = $phoneNo;
            // $parentUser->password = Hash::make($password);
            // $parentUser->save();

            // DB::table('model_has_roles')->insert([
            //     'role_id' => 4,
            //     'model_type' => "App\User",
            //     'model_id' => $parentUser->id
            // ]);

            //POPULATING TABLE PARENT USER
            // DB::table('parent_user')->insert([
            //     'parent_id' => $parent->id,
            //     'user_id' => $parentUser->id
            // ]);


            //SAVE PASSWORD TILL STUDENT CREATED
            // DB::table('temp_pass_007')->insert([
            //     'parent_id' => $parent->id,
            //     'password' => $password
            // ]);

        // END OF CREATE USER FROM THE PARENTS 
         //SAVE PASSWORD   
            // DB::table('pass_2020')->insert([
            //     'username' => $phoneNo,
            //     'pass' => $password
            // ]);

        // END OF USER PASSWORD  
        return redirect()->route('addstudent', $id);
    }
    public function studentDetails($id){
        $studentparentId = DB::table('students')
            ->where('id', $id)
            ->pluck('parent_id')
            ->first();
             if($studentparentId == Null){
                $students= Student::join('forms', 'forms.id', '=', 'students.form_id')
                ->join('streams', 'streams.id', '=', 'students.stream_id')
                ->join('counties', 'counties.id', '=', 'students.county_id')
                ->select(
                    'students.student_name',
                    'students.admission_number',
                    'students.jointed_year',
                    'students.kcpe_index_number',
                    'students.grade',
                    'students.dob',
                    'students.id',
                    'forms.form_name',
                    'counties.county_name',
                    'streams.stream_name',
                    'students.stream_id',
                    'forms.id as form_id'
                )
                ->where('students.id', $id)
                ->first();
             }else{

        $students= Student::join('parents','parents.id','=','students.parent_id')
            ->join('forms', 'forms.id', '=', 'students.form_id')
            ->join('streams', 'streams.id', '=', 'students.stream_id')
            ->join('counties', 'counties.id', '=', 'students.county_id')
            ->select(
                'students.student_name',
                'students.admission_number',
                'students.jointed_year',
                'students.kcpe_index_number',
                'students.grade',
                'counties.county_name',
                'students.dob',
                'students.id',
                'parents.parent_name',
                'parents.national_id as pnational_id',
                'parents.phone_number as pphone_number',
                'parents.occupation',
                'forms.form_name',
                'streams.stream_name',
                'students.stream_id',
                'forms.id as form_id'
            )
            ->where('students.id', $id)
            ->first();
        }
        $feeDetails = Payment::where('student_id', $id)
                ->select(
                    'created_at',
                    'receipt_number'
            )
            ->groupBy('receipt_number')
            ->orderBy('id', 'DESC')
            ->get();

        $addedStudentFees = DB::table('added_fee')
            ->where('student_id', $id)
            ->get(); 
        $oldReciepts = DB::table('old_receipt')
            ->where('student_id', $id)
            ->get();
        $balanceBroughtForwardDetails = DB::table('balance_brought_forward')
            ->where('student_id', $id)
            ->get();
                // dd($feeDetails);
        return view('students.studentDetails')
            ->with('addedStudentFees', $addedStudentFees)
            ->with('balanceBroughtForwardDetails', $balanceBroughtForwardDetails)
            ->with('oldReciepts', $oldReciepts)
            ->with('student', $students)
            ->with('feepayment', $feeDetails);
    }

    public function deleteStudent($id){
        DB::table('students')->where('id', $id)->delete();
        return redirect()->back();
    }

     public function changeForm(Request $request, $id){
         $streamId = $request->input('streamId');
         $formId = $request->input('formId');
            DB::table('students')->where('id', $id)->update([
                'form_id'=>$formId,
                'stream_id'=>$streamId

            ]);

        $currentSemester = DB::table('semesters')
            ->where('current', 1)
            ->pluck('id')
            ->first();

        $voteheadAmountIds = DB::table('form_voteheads')
            ->where('form_id', $formId)
            ->pluck('votehead_amount_id')
            ->toArray();

         $voteheaddetails = DB::table('votehead_amounts')
            ->join('form_voteheads', 'form_voteheads.votehead_amount_id', '=', 'votehead_amounts.id')
            ->join('students', 'students.form_id', '=', 'form_voteheads.form_id')
            ->select(
                'votehead_amounts.amount',
                'votehead_amounts.votehead_id',
                'students.id as id'
            )
            ->whereIn('votehead_amounts.id', $voteheadAmountIds)
            ->where('votehead_amounts.semester_id', $currentSemester)
            ->get();

            foreach ($voteheaddetails as $key => $vid) {

                $checkPayments = DB::table('payments')
                    ->where('amount', 0)
                    ->where('balance', $vid->amount)
                    ->where('semester_id', $currentSemester)
                    ->where('student_id', $vid->id)
                    ->where('votehead_id', $vid->votehead_id)
                    ->count();

                    if( $checkPayments > 0 ){

                        DB::table('payments')
                            ->where('amount', 0)
                            ->where('balance', $vid->amount)
                            ->where('semester_id', $currentSemester)
                            ->where('student_id', $vid->id)
                            ->where('votehead_id', $vid->votehead_id)
                            ->delete();

                        DB::table('payments')
                            ->insert([
                                'year' => date('Y'),
                                'amount' => 0,
                                'balance' => $vid->amount,
                                'semester_id' => $currentSemester,
                                'student_id' => $vid->id,
                                'votehead_id' => $vid->votehead_id
                            ]);
                    }else{

                        DB::table('payments')
                            ->insert([
                                'year' => date('Y'),
                                'amount' => 0,
                                'balance' => $vid->amount,
                                'semester_id' => $currentSemester,
                                'student_id' => $vid->id,
                                'votehead_id' => $vid->votehead_id
                            ]);
                    }

                
                    
            }



            return redirect()->back();
        }
    public function getFormStreams(Request $request){
         if( $request->ajax()){
            $formId = $request->get('formId');
            $streams = DB::table('streams')->where('form_id', $formId)->get();

        return json_encode($streams);
        }

    }

    public function editStudent($id){
        $formId = DB::table('students')->where('id', $id)->pluck('form_id')->first();
        $students = DB::table('students')->where('id', $id)->first();
        $relationships = DB::table('student_parent_relationship')->get();
        $parents = DB::table('parents')->get();
        $counties = DB::table('counties')->get();
        $forms = DB::table('forms')->get();
        $genders = DB::table('genders')->get();
        $streams = DB::table('streams')
            ->where('form_id', $formId)
            ->get();
        return view('students.edit')
            ->with('relationships', $relationships)
            ->with('student', $students)
            ->with('forms', $forms)
            ->with('streams', $streams)
            ->with('genders', $genders)
            ->with('counties', $counties)
            ->with('parents', $parents)
            ->with('formId', $formId)
            ->with('id', $id);
    }
     public function updateStudent(Request $request){
         $countyId = $request->input('countyId');
         $parentId = $request->input('parent');
         $relationships = $request->input('relationship');
         $streamId = $request->input('streamId');
         $genderId = $request->input('genderId');
         $studentId = $request->input('studentId');
         //streamId
        if($streamId){
            $stream = $streamId;
        }else{
            $stream = DB::table('students')
                ->where('id', $studentId)
                ->pluck('stream_id')
                ->first();
        }
        //genderId
        if($genderId){
            $gender = $genderId;
        }else{
            $gender = DB::table('students')
                ->where('id', $studentId)
                ->pluck('gender_id')
                ->first();
        }  
        //countyId
        if($countyId){
            $county = $countyId;
        }else{
            $county = DB::table('students')
                ->where('id', $studentId)
                ->pluck('county_id')
                ->first();
        } 
        //parent_relationship
        if($relationships){
            $relationship = $relationships;
        }else{
            $relationship = DB::table('students')
                ->where('id', $studentId)
                ->pluck('parent_relationship_id')
                ->first();
        } 
        if($parentId){
            $parent = $parentId;
        }else{
            $parent = DB::table('students')
                ->where('id', $studentId)
                ->pluck('parent_id')
                ->first();
        }


             DB::table('students')->where('id', $studentId)
                ->update([
                    'stream_id'=>$stream,
                    'parent_id'=>$parent,
                    'parent_relationship_id'=>$relationship,
                    'gender_id'=>$gender,
                    'county_id'=>$county,
                    'grade'=>$request->input('grade'),
                    'status'=>$request->input('status'),
                    'student_name'=>$request->input('student_name'),
                    'admission_number'=>$request->input('admission_number'),
                    'kcpe_index_number'=>$request->input('kcpe_index_number')

             ]);
            return redirect()->back();

    }

    public function changeAllStudentsForm(Request $request, $id){
         $streamId = $request->input('streamId');
         $formId = $request->input('formId');
         $limited = $request->input('limit');
        
        $students = DB::table('students')->where('form_id', $id)->limit($limited)->pluck('id')->toArray();
        if ($students) {
        
            foreach ($students as $key => $value) {
                 DB::table('students')->where('id', $value)
                ->update([
                            'form_id'=> $formId,
                            'stream_id'=> $streamId
                ]);
            }


        $currentSemester = DB::table('semesters')
            ->where('current', 1)
            ->pluck('id')
            ->first();

        $voteheadAmountIds = DB::table('form_voteheads')
            ->where('form_id', $formId)
            ->pluck('votehead_amount_id')
            ->toArray();

         $voteheaddetails = DB::table('votehead_amounts')
            ->join('form_voteheads', 'form_voteheads.votehead_amount_id', '=', 'votehead_amounts.id')
            ->join('students', 'students.form_id', '=', 'form_voteheads.form_id')
            ->select(
                'votehead_amounts.amount',
                'votehead_amounts.votehead_id',
                'students.id as id'
            )
            ->whereIn('votehead_amounts.id', $voteheadAmountIds)
            ->where('votehead_amounts.semester_id', $currentSemester)
            ->get();

            foreach ($voteheaddetails as $key => $vid) {

                $checkPayments = DB::table('payments')
                    ->where('amount', 0)
                    ->where('balance', $vid->amount)
                    ->where('semester_id', $currentSemester)
                    ->where('student_id', $vid->id)
                    ->where('votehead_id', $vid->votehead_id)
                    ->count();

                    if( $checkPayments > 0 ){

                        DB::table('payments')
                            ->where('amount', 0)
                            ->where('balance', $vid->amount)
                            ->where('semester_id', $currentSemester)
                            ->where('student_id', $vid->id)
                            ->where('votehead_id', $vid->votehead_id)
                            ->delete();

                        DB::table('payments')
                            ->insert([
                                'year' => date('Y'),
                                'amount' => 0,
                                'balance' => $vid->amount,
                                'semester_id' => $currentSemester,
                                'student_id' => $vid->id,
                                'votehead_id' => $vid->votehead_id
                            ]);
                    }else{

                        DB::table('payments')
                            ->insert([
                                'year' => date('Y'),
                                'amount' => 0,
                                'balance' => $vid->amount,
                                'semester_id' => $currentSemester,
                                'student_id' => $vid->id,
                                'votehead_id' => $vid->votehead_id
                            ]);
                    }

                
                    
            }


        }else{
            $formName = DB::table('forms')->where('id', $id)->pluck('form_name')->first();
             Session::flash('message', "There no student in " . $formName );
            return redirect()->back();

        }


        // dd($students);

        return redirect()->back();
    }
    public function upgradeStudents($id){
            $formName = DB::table('forms')->where('id', $id)->pluck('form_name')->first();
            $students = DB::table('students')->where('form_id', $id)->get();
            $forms = DB::table('forms')->get();
        return view('students.upgradeStudents')
            ->with('students', $students)
            ->with('forms', $forms)
            ->with('formName', $formName)
            ->with('formId', $id);
    }

    public function saveUpgradeStudent(Request $request){
         $streamId = $request->input('streamId');
         $intialFormId = $request->input('intialFormId');
         $formId = $request->input('formId');
         $studentId = $request->input('students');
        $formName = DB::table('forms')->where('id', $intialFormId)->pluck('form_name')->first();


         if ($studentId) {
             
            foreach ($studentId as $key => $value) {
                 DB::table('students')->where('id', $value)
                 ->where('form_id', $intialFormId)
                ->update([
                            'form_id'=> $formId,
                            'stream_id'=> $streamId
                ]);
            }

            
        $currentSemester = DB::table('semesters')
            ->where('current', 1)
            ->pluck('id')
            ->first();

        $voteheadAmountIds = DB::table('form_voteheads')
            ->where('form_id', $formId)
            ->pluck('votehead_amount_id')
            ->toArray();

         $voteheaddetails = DB::table('votehead_amounts')
            ->join('form_voteheads', 'form_voteheads.votehead_amount_id', '=', 'votehead_amounts.id')
            ->join('students', 'students.form_id', '=', 'form_voteheads.form_id')
            ->select(
                'votehead_amounts.amount',
                'votehead_amounts.votehead_id',
                'students.id as id'
            )
            ->whereIn('votehead_amounts.id', $voteheadAmountIds)
            ->where('votehead_amounts.semester_id', $currentSemester)
            ->get();

            foreach ($voteheaddetails as $key => $vid) {

                $checkPayments = DB::table('payments')
                    ->where('amount', 0)
                    ->where('balance', $vid->amount)
                    ->where('semester_id', $currentSemester)
                    ->where('student_id', $vid->id)
                    ->where('votehead_id', $vid->votehead_id)
                    ->count();

                    if( $checkPayments > 0 ){

                        DB::table('payments')
                            ->where('amount', 0)
                            ->where('balance', $vid->amount)
                            ->where('semester_id', $currentSemester)
                            ->where('student_id', $vid->id)
                            ->where('votehead_id', $vid->votehead_id)
                            ->delete();

                        DB::table('payments')
                            ->insert([
                                'year' => date('Y'),
                                'amount' => 0,
                                'balance' => $vid->amount,
                                'semester_id' => $currentSemester,
                                'student_id' => $vid->id,
                                'votehead_id' => $vid->votehead_id
                            ]);
                    }else{

                        DB::table('payments')
                            ->insert([
                                'year' => date('Y'),
                                'amount' => 0,
                                'balance' => $vid->amount,
                                'semester_id' => $currentSemester,
                                'student_id' => $vid->id,
                                'votehead_id' => $vid->votehead_id
                            ]);
                    }

                
                    
            }

        $students = DB::table('students')->where('form_id', $intialFormId)->get();
        $forms = DB::table('forms')->get();
        return view('students.upgradeStudents')
            ->with('students', $students)
            ->with('forms', $forms)
            ->with('formName', $formName)
            ->with('formId', $intialFormId);

            }else{
             Session::flash('message', "There no student in " . $formName );
            return redirect()->back();

        }




    }

}
