<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Auth;
use App\Teacher;

class TeachersController extends Controller
{
       public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
    	$teachers = DB::table('teachers')
    		->join('genders', 'genders.id', '=', 'teachers.gender_id')
    		->select(
    			'teachers.teacher_name',
    			'teachers.id',
    			'teachers.national_id',
    			'teachers.experience',
    			'teachers.previous_teaching_school',
    			'teachers.phone_number',
    			'teachers.status',
    			'teachers.degree',
    			'teachers.created_at',
    			'genders.gender',
    			'teachers.reg_number'
    		)
    		->get();
    	$genders = DB::table('genders')->get();

    	return view('teachers.index')
    		->with('teachers', $teachers)
    		->with('genders', $genders);
    }

    public function create(){
    	$genders = DB::table('genders')->get();
    	return view('teachers.create')
    		->with('genders', $genders);
    } 

    public function saveTeacher(Request $request){
        $phoneNo = $request->input('phone_number');

            $teacher = new Teacher();
            $teacher->teacher_name = $request->input('teacher_name');
            $teacher->national_id = $request->input('national_id');
            $teacher->phone_number = $phoneNo;
            $teacher->status = 1;
            $teacher->previous_teaching_school = $request->input('previous_teaching_school');
            $teacher->gender_id = $request->input('gender');
            $teacher->experience = $request->input('experience');
            $teacher->degree = $request->input('degree');
            $teacher->reg_number = $request->input('reg_number');
            $teacher->save();

          // START OF CREATE USER FROM THE PARENTS
            // $password = str_random(5);

            // $parentUser = new User();
            // $parentUser->name = $request->input('full_name');
            // $parentUser->username = $phoneNo;
            // $parentUser->password = Hash::make($password);
            // $parentUser->save();

            // DB::table('model_has_roles')->insert([
            //     'role_id' => 4,
            //     'model_type' => "App\User",
            //     'model_id' => $parentUser->id
            // ]);

        // END OF CREATE USER FROM THE PARENTS 
         //SAVE PASSWORD   
            // DB::table('pass_2020')->insert([
            //     'username' => $phoneNo,
            //     'pass' => $password
            // ]);

        // END OF USER PASSWORD  
    	return redirect()->route('formStreams', $teacher->id);
    }
    public function assignSubjects(Request $request){
         $teacherId = $request->input('teacherId');
         $streamId = $request->input('streamId');
         $formId = $request->input('formId');

    	$teacherName = DB::table('teachers')
    		->where('id', $teacherId)
    		->pluck('teacher_name')
    		->first();
        $streamName = DB::table('streams')
            ->where('id', $streamId)
            ->pluck('stream_name')
            ->first();
        $forms = DB::table('forms')->get();
        $subjects = DB::table('subjects')->get();
        $streams = DB::table('streams')->get();

        $formSubjects = DB::table('teacher_subjects')
            ->where('teacher_id', $teacherId)
            ->where('stream_id', $streamId)
            ->where('form_id', $formId)
            ->pluck('teacher_subjects.subject_id', 'teacher_subjects.subject_id')
            ->all();
    	return view('teachers.assignSubjects')
            ->with('formId', $formId)
            ->with('streamName', $streamName)
            ->with('streamId', $streamId)
            ->with('teacherId', $teacherId)
            ->with('subjects', $subjects)
            ->with('streams', $streams)
            ->with('formSubjects', $formSubjects)
            ->with('forms', $forms)
            ->with('teacherName', $teacherName);
    }

    public function activateTeacher(Request $request, $id){
    	DB::table('teachers')->where('id', $id)
    		->update([
    			'status'=> 1
    		]);
		return redirect()->back();

    }  

    public function deactivateTeacher(Request $request, $id){
    	DB::table('teachers')->where('id', $id)
    		->update([
    			'status'=> 0
    		]);
		return redirect()->back();

    }  

    public function deleteTeacher($id){
    	DB::table('teachers')->where('id', $id)->delete();
    		
		return redirect()->back();

    } 

    public function editTeacher(Request $request, $id){
    	DB::table('teachers')->where('id', $id)
    		->update([
    			'teacher_name'=>$request->input('teacher_name'),
    			'national_id'=>$request->input('national_id'),
    			'phone_number'=>$request->input('phone_number'),
    			'previous_teaching_school'=>$request->input('previous_teaching_school'),
    			'gender_id'=>$request->input('gender'),
    			'experience'=>$request->input('experience'),
    			'degree'=>$request->input('degree'),
    			'reg_number'=>$request->input('reg_number')
    		]);
		return redirect()->back();

    }
    public function getStreams(Request $request){
         if( $request->ajax()){
            $formId = $request->get('formId');
            $streams = DB::table('streams')->where('form_id', $formId)->get();

        return json_encode($streams);
        }

    }
     public function getSubjects(Request $request){
             if( $request->ajax()){
                $streamId = $request->get('streamIds');
                 $subjects = DB::table('subjects')->get();
                $teacherSubjects = DB::table('teacher_subjects')
                    ->where('teacher_id', $id)
                    ->where('stream_id', $streamId)
                    ->pluck('teacher_subjects.subject_id', 'teacher_subjects.subject_id')
                    ->all();

            return json_encode([
                    'teacherSubjects' => $teacherSubjects, 
                    'subjects' => $subjects
                ]);
        }
        }
        public function formStreams($id){
            $teacherName = DB::table('teachers')
                ->where('id', $id)
                ->pluck('teacher_name')
                ->first();
            $forms = DB::table('forms')->get();
            $streams = DB::table('streams')->get();
           
            return view('teachers.formStreams')
            ->with('id', $id)
            ->with('forms', $forms)
            ->with('teacherName', $teacherName)
            ->with('streams', $streams);

        }

    public function saveTeacherSubject(Request $request){
        $teacherId = $request->input('teacherId');
        $formId = $request->input('formId');
        $streamId = $request->input('streamId');

      
        $subjectIds = $request->input('subject');
        foreach ($subjectIds as $key => $value) {
            $checkSubjectTeacherExist = DB::table('teacher_subjects')
                ->where('teacher_id', '<>',$teacherId)
                ->where('subject_id', $value)
                ->where('stream_id', $streamId)
                ->first();
         
            if($checkSubjectTeacherExist){
                 $streams = DB::table('streams')->get();
                 $forms = DB::table('forms')->get();
                 $teacherName = DB::table('teachers')
                    ->where('id', $checkSubjectTeacherExist->teacher_id)
                    ->pluck('teacher_name')
                    ->first(); 
                $subjectName = DB::table('subjects')
                    ->where('id', $checkSubjectTeacherExist->subject_id)
                    ->pluck('subject_name')
                    ->first();
                $streamName = DB::table('streams')
                    ->where('id', $streamId)
                    ->pluck('stream_name')
                    ->first();

                 Session::flash('message', $subjectName." in " .$streamName." is Assigned to " . $teacherName );
               return redirect()->route('formStreams', $teacherId);
          

            }
        }
       

        if( $subjectIds ){
            foreach ($subjectIds as  $subjectId) {
            DB::table('teacher_subjects')
                ->where('teacher_id', $teacherId)
                ->where('stream_id', $streamId)
                ->delete();
        }
    }
        

        foreach ($subjectIds as $subjectId) {

            DB::table('teacher_subjects')->insert([
                'teacher_id' => $teacherId,
                'stream_id' => $streamId,
                'form_id' => $formId,
                'subject_id' => $subjectId
            ]);

        }

        
        return redirect()->route('formStreams', $teacherId);
       

    }
}
