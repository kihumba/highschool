<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Votehead;
use App\Semester;
use App\Subject;
use DB;

class SettingsController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
   
    public function streams()
    {
        $streams = DB::table('streams')
        ->join('forms', 'forms.id', '=', 'streams.form_id')
        ->join('users', 'users.id', '=', 'streams.user_id')
        ->select(
            'forms.form_name',
            'users.name',
            'streams.created_at',
            'streams.form_id',
            'streams.stream_name',
            'streams.id'
        )
        ->get();

        $forms = DB::table('forms')->get();
        return view('streams.index')
            ->with('streams', $streams)
            ->with('forms', $forms);
    }

    public function editStream(Request $request, $id){
        $formIds = $request->input('formId');
        if($formIds){
            $formId = $formIds;
        }else{
            $formId = DB::table('streams')
                ->where('id', $id)
                ->pluck('form_id')
                ->first();
        }

         DB::table('streams')->where('id',$id)
            ->update([
                'stream_name' =>$request->input('stream_name'),
                'form_id' =>$formId,
                'user_id' => Auth::user()->id

            ]);
        return redirect()->back();
    }

 public function deleteStream($id){
        DB::table('streams')->where('id',$id)->delete();
        return redirect()->back();
    }


    public function subjects()
    {
        $subjects = DB::table('subjects')
        ->join('users', 'users.id', '=', 'subjects.user_id')
        ->select(
            'subjects.subject_name',
            'subjects.id',
            'subjects.created_at',
            'users.name'
        )
        ->get();
        return view('subjects.index')
            ->with('subjects', $subjects);
    }

    public function saveSubject(Request $request){
         if( $request->ajax()){

            $subject=new Subject();
            $subject->subject_name = $request->get('subjectName');
            $subject->user_id = Auth::user()->id;
            $subject->save();

            $subjects = DB::table('subjects')
                ->join('users', 'users.id', '=', 'subjects.user_id')
                ->select(
                    'subjects.subject_name',
                    'subjects.id',
                    'subjects.created_at',
                    'users.name'
                )
                ->orderBy('id', 'DESC')
                ->get();
            return json_encode($subjects);
        } else{
            $subject=new Subject();
            $subject->subject_name = $request->input('subject_name');
            $subject->user_id = Auth::user()->id;
            $subject->save();


        return redirect()->back();
    }
    }

    public function editSubject(Request $request, $id){
        DB::table('subjects')->where('id', $id)
        ->update([
            'subject_name'=>$request->input('subject_name')
        ]);
        return redirect()->back();
    }

    public function deleteSubjects($id){
        DB::table('subjects')->where('id', $id)->delete();
        return redirect()->back();
    }

   
    public function store(Request $request)
    {
        DB::table('streams')->insert([
            'stream_name' =>$request->input('stream_name'),
            'form_id' =>$request->input('formId'),
            'user_id' => Auth::user()->id

        ]);
        return redirect()->back();
    }

    public function voteheads(){
        $voteheads =  DB::table('voteheads')
            ->join('users', 'users.id', '=', 'voteheads.user_id')
            ->select(
                'voteheads.votehead_name',
                'voteheads.id',
                'users.name'
            )
            ->orderBy('id', 'DESC')
            ->get();


        return view('voteheads.index')
            ->with('voteheads',$voteheads);

    
    }
    public function addVotehead(Request $request){
        if( $request->ajax()){
            $votehead=new Votehead();
            $votehead->votehead_name = $request->get('voteheadName');
            $votehead->user_id = Auth::user()->id;
            $votehead->save();

        $voteheads = Votehead::join('users', 'users.id', '=', 'voteheads.user_id')
            ->select(
                'voteheads.votehead_name',
                'voteheads.id',
                'users.name'
            )
            ->orderBy('id', 'DESC')
            ->get();

            return json_encode($voteheads);
        }
    }

    public function updateVotehead( Request $request, $id){
        $voteheads=Votehead::where('id',$id)
        ->update([
            'votehead_name' => $request->input('votehead_name'),
            'user_id' => Auth::user()->id

        ]);


        return redirect()->back();
    }

     public function deleteVotehead($id){
        Votehead::where('id',$id)->delete();
        return redirect()->back();
    }

    //SEMESTER

    public function semester(){
        $semesters = Semester::join('users','users.id', '=', 'semesters.user_id')
            ->select(
                'semesters.semester_name',
                'semesters.id',
                'users.name',
                'semesters.current'
            )
            ->orderBy('id', 'DESC')
            ->get();
        

        return view('semesters.index')
            ->with('semesters',$semesters);
    }

     public function add_semester( Request $request){
        if( $request->ajax()){

            $semester=new Semester();
            $semester->semester_name = $request->get('semesterName');
            $semester->user_id = Auth::user()->id;
            $semester->save();

            $semesters = DB::table('semesters')
                ->join('users', 'users.id', '=', 'semesters.user_id')
                ->select(
                    'semesters.semester_name',
                    'semesters.id',
                    'users.name',
                    'semesters.current'
                )
                ->orderBy('id', 'DESC')
                ->get();
            return json_encode($semesters);
        } else{
            $semester=new Semester();
            $semester->semester_name = $request->input('semester_name');
            $semester->user_id = Auth::user()->id;
            $semester->save();


        return redirect()->back();
    }
    }

     public function update_semester( Request $request,$id){
        $semesters=DB::table('semesters')->where('id',$id)
        ->update([
            'semester_name' => $request->input('semester_name'),
            'user_id' => Auth::user()->id

        ]);


        return redirect()->back();
    }

    public function deleteSemester($id){
      DB::table('semesters')->where('id',$id)->delete();
        return redirect()->back();
    }

    //VOTEHEAD AMOUNTS

     public function voteheadAmounts(){

        $vamounts = DB::table('votehead_amounts')
        ->join('semesters', 'semesters.id', '=', 'votehead_amounts.semester_id')
        ->join('voteheads', 'voteheads.id', '=','votehead_amounts.votehead_id' )
        ->select(
            'votehead_amounts.id as vamount_id',
            'votehead_amounts.amount',
            'voteheads.votehead_name',
            'semesters.semester_name'
        )
        ->get();

        $semesters = DB::table('semesters')->get();
        $voteheads = DB::table('voteheads')->get();

        return view('voteheadAmounts.index')
            ->with('vamounts',$vamounts)
            ->with('semesters',$semesters)
            ->with('voteheads',$voteheads);
    }

    public function add_vamount( Request $request){
      if( $request->ajax()){
            DB::table('votehead_amounts')->insert([
                'amount' => $request->get('amount'),
                'semester_id' => $request->get('semesterId'),
                'votehead_id' => $request->get('voteheadId')
                ]);

              $voteheadAmount = DB::table('votehead_amounts')
                ->join('semesters', 'semesters.id', '=', 'votehead_amounts.semester_id')
                ->join('voteheads', 'voteheads.id', '=','votehead_amounts.votehead_id' )
                ->select(
                    'votehead_amounts.id as vamount_id',
                    'votehead_amounts.amount',
                    'voteheads.votehead_name',
                    'semesters.semester_name',
                    'semesters.id'
                )
                ->orderBy('id', 'DESC')
                ->get();

                return json_encode($voteheadAmount);
            }
        }

    public function deleteVoteheaAamount($id){
        $vamounts=DB::table('votehead_amounts')->where('id',$id)->delete();


        return redirect()->back();
    }

     public function update_vamount( Request $request, $id){
        $vamounts=DB::table('votehead_amounts')->where('id', $id)
        ->update([
            'amount' => $request->input('amount')

        ]);


        return redirect()->back();
    }

    public function assignedVotehead(){
        $currentSemester = DB::table('semesters')
            ->where('current', 1)
            ->pluck('id')
            ->first();
        $activeVoteheads = DB::table('form_voteheads')
            ->join('votehead_amounts', 'votehead_amounts.id', '=', 'form_voteheads.votehead_amount_id')
            ->join('voteheads', 'voteheads.id', '=', 'votehead_amounts.votehead_id')
            ->select(
                'form_voteheads.form_id',
                'voteheads.votehead_name'

            )
            ->where('form_voteheads.semester_id', $currentSemester)
            ->get();

        return view('formVoteheads.assignedVotehead')
            ->with('activeVoteheads', $activeVoteheads);
    }

    public function assignVotehead($id){
          $currentSemester = DB::table('semesters')
            ->where('current', 1)
            ->pluck('id')
            ->first();

        $formVoteheads = DB::table('form_voteheads')
            ->where('form_id', $id)
            ->where('semester_id', $currentSemester)
            ->pluck('form_voteheads.votehead_amount_id', 'form_voteheads.votehead_amount_id')
            ->all();
        $voteheads = DB::table('voteheads')
            ->join('votehead_amounts', 'votehead_amounts.votehead_id', '=', 'voteheads.id')
            ->select(
                'votehead_amounts.semester_id',
                'votehead_amounts.id as id',
                'votehead_amounts.amount',
                'voteheads.id as votehead_id',
                'voteheads.votehead_name'
            )
            ->where('votehead_amounts.semester_id', $currentSemester)
            ->get();
      return view('formVoteheads.assignVotehead')
            ->with('id', $id)
            ->with('voteheads', $voteheads)
            ->with('formVoteheads', $formVoteheads);
    }


    public function saveActiveVoteheads(Request $request){
        $formId = $request->input('formId');
        $voteheadAmtIds = $request->input('votehead');

        $currentSemester = DB::table('semesters')
                ->where('current', 1)
                ->pluck('id')
                ->first();

        $checkvotehead = DB::table('form_voteheads')
            ->where('form_id', $formId)
            ->where('semester_id', $currentSemester)
            ->get();

        if( $voteheadAmtIds ){

             foreach ($voteheadAmtIds as  $voteheadAmtId) {
                DB::table('form_voteheads')
                    ->where('form_id', $formId)
                    ->where('semester_id', $currentSemester)
                    ->delete();
            }
    

                foreach( $voteheadAmtIds as $voteheadAmtId ){

                    DB::table('form_voteheads')->insert([
                        'form_id' => $formId,
                        'votehead_amount_id' => $voteheadAmtId,
                        'semester_id' => $currentSemester
                    ]);

                }



                $voteheadAmountIds = DB::table('form_voteheads')
                    ->where('form_id', $formId)
                    ->pluck('votehead_amount_id')
                    ->toArray();

                 $voteheaddetails = DB::table('votehead_amounts')
                    ->join('form_voteheads', 'form_voteheads.votehead_amount_id', '=', 'votehead_amounts.id')
                    ->join('students', 'students.form_id', '=', 'form_voteheads.form_id')
                    ->select(
                        'votehead_amounts.amount',
                        'votehead_amounts.votehead_id',
                        'students.id as id'
                    )
                    ->whereIn('votehead_amounts.id', $voteheadAmountIds)
                    ->where('votehead_amounts.semester_id', $currentSemester)
                    ->get();

                    foreach ($voteheaddetails as $key => $vid) {

                        $checkPayments = DB::table('payments')
                            ->where('amount', 0)
                            ->where('balance', $vid->amount)
                            ->where('semester_id', $currentSemester)
                            ->where('student_id', $vid->id)
                            ->where('votehead_id', $vid->votehead_id)
                            ->count();

                            if( $checkPayments > 0 ){

                                DB::table('payments')
                                    ->where('amount', 0)
                                    ->where('balance', $vid->amount)
                                    ->where('semester_id', $currentSemester)
                                    ->where('student_id', $vid->id)
                                    ->where('votehead_id', $vid->votehead_id)
                                    ->delete();

                                DB::table('payments')
                                    ->insert([
                                        'year' => date('Y'),
                                        'amount' => 0,
                                        'balance' => $vid->amount,
                                        'semester_id' => $currentSemester,
                                        'student_id' => $vid->id,
                                        'votehead_id' => $vid->votehead_id
                                    ]);
                            }else{

                                DB::table('payments')
                                    ->insert([
                                        'year' => date('Y'),
                                        'amount' => 0,
                                        'balance' => $vid->amount,
                                        'semester_id' => $currentSemester,
                                        'student_id' => $vid->id,
                                        'votehead_id' => $vid->votehead_id
                                    ]);
                            }     
                    }
        }else{
            return redirect()->route('assignVotehead', $formId);
        }
        return redirect()->route('assignVotehead', $formId);
     }


    // public function semesterUpdate($id){


    //       DB::table('semesters')->where('current', 1)
    //     ->update([
    //         'current' => 0
    //     ]); 

    //       DB::table('semesters')->where('id', $id)
    //     ->update([
    //         'current' => 1
    //     ]); 

    //     return redirect()->back();

    // }



    public function semesterUpdate($id){


        $currentSemester = DB::table('semesters')
            ->where('current', 1)
            ->pluck('id')
            ->first();


            $paidAmounts = DB::table('payments')
                ->join('voteheads', 'voteheads.id', '=', 'payments.votehead_id')
                ->join('votehead_amounts', 'votehead_amounts.votehead_id', '=', 'payments.votehead_id')
                ->join('semesters', 'semesters.id', '=', 'votehead_amounts.semester_id')
                ->select(
                    DB::raw("
                        (SUM(payments.amount)) as balance,
                        payments.student_id as id
                    ")
                )
                ->where('payments.semester_id', $currentSemester)
                ->where('semesters.current', 1)
                ->groupBy('payments.student_id')
                ->get();

            $balance = array();

        foreach ($paidAmounts as $key => $value) {
            
            $voteheadStudent = DB::table('students')
                ->join('form_voteheads', 'form_voteheads.form_id', '=', 'students.form_id')
                ->join('votehead_amounts', 'votehead_amounts.id', '=', 'form_voteheads.votehead_amount_id')
                ->select(
                    DB::raw("
                        (SUM(votehead_amounts.amount)) as balance
                    ")
                )
                ->where('form_voteheads.semester_id', $currentSemester)
                ->where('students.id', $value->id)
                ->groupBy('students.id')
                ->get();

                $getbalance = $voteheadStudent;

                foreach ($getbalance as $key => $val) {

                    if($value->balance != 0 && $value->balance < $val->balance){

                        $bal = $val->balance - $value->balance;

                        $balance[] = array('balance' => $bal, 'studentId' => $value->id);
                    }
                }

        }

         $object = json_decode(json_encode($balance));

        foreach ($object as $key => $value) {

            $checkStudent = DB::table('balance_brought_forward')
            ->where('student_id', $value->studentId)
            ->first();

            if($checkStudent){

                $newBalance = $checkStudent->balance + $value->balance;
                    DB::table('balance_brought_forward')
                    ->where('student_id', $value->studentId)
                    ->update([
                        'balance' => $newBalance
                    ]);
            }else{

                 DB::table('balance_brought_forward')
                    ->insert([
                        'student_id' => $value->studentId,
                        'intial_balance' => $value->balance,
                        'amount' => $value->balance
                    ]);

            }

             $checkAddedfee = DB::table('added_fee')
                ->where('student_id', $value->studentId)
                ->first();

            if($checkAddedfee){
                DB::table('added_fee')
                    ->where('student_id', $value->studentId)
                    ->update([
                        'semester_id' => $id
                    ]);
                 


            }

            
        }

        DB::table('semesters')->where('current', 1)
        ->update([
            'current' => 0
        ]);
        

        $semesters=DB::table('semesters')->where('id',$id)
        ->update([
            'current' => 1

        ]);

        return redirect()->back();

    }


}
