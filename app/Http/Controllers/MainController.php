<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class MainController extends Controller
{
    public function getpdf(){
    	$data = [
       
            // 'pdfData' => $pdfData
        ];

        $pdf = PDF::loadView('reports.letter', $data);

        return $pdf->stream('letter.pdf');

    }

     public function nonTeachingCourses(){
    	$data = [
       
            // 'pdfData' => $pdfData
        ];

        $pdf = PDF::loadView('reports.nonTeachingCourses', $data);

        return $pdf->stream('nonTeachingCourses.pdf');

    }  


    public function ittcTeachers(){
        $data = [
       
            // 'pdfData' => $pdfData
        ];

        $pdf = PDF::loadView('reports.teacher', $data);

        return $pdf->stream('teacher.pdf');

    }
    public function ittcvocational(){
        $data = [
       
            // 'pdfData' => $pdfData
        ];

        $pdf = PDF::loadView('reports.vocational', $data);

        return $pdf->stream('vocational.pdf');

    }




    public function email(){
        return view('email.email');
    }
}
