<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $students = DB::table('students')->count();
        $teachers = DB::table('teachers')->count();
        $streams = DB::table('streams')->count();
        $streams = DB::table('streams')->count();
        $subjects = DB::table('subjects')->count();
        return view('home')
            ->with('students', $students)
            ->with('subjects', $subjects)
            ->with('streams', $streams)
            ->with('teachers', $teachers);
    }
}
