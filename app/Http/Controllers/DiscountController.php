<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class DiscountController extends Controller
{
    public function discount(Request $request, $id){
    	$currentSemester = DB::table('semesters')
            ->where('current', 1)
            ->pluck('id')
            ->first();

    	DB::table('discount_fees')
                ->insert([
                'semester_id' => $currentSemester,
                'discount_amount' => $request->input('amount'),
                'discount_description' => $request->input('discount_description'),
                'user_id' => Auth::user()->id,
                'student_id' => $id
            ]);
		return redirect()->back();
    }
     public function editDiscount(Request $request, $id){
    	$currentSemester = DB::table('semesters')
            ->where('current', 1)
            ->pluck('id')
            ->first();

    	DB::table('discount_fees')
    			->where('id', $id)
                ->update([
                'semester_id' => $currentSemester,
                'discount_amount' => $request->input('amount'),
                'discount_description' => $request->input('discount_description'),
                'user_id' => Auth::user()->id,
            ]);
		return redirect()->back();
    }

    public function discounted(){
    	$currentSemester = DB::table('semesters')
            ->where('current', 1)
            ->pluck('id')
            ->first();
        $discounts = DB::table('discount_fees')
            ->join('users', 'users.id', '=', 'discount_fees.user_id')
            ->join('students', 'students.id', '=', 'discount_fees.student_id')
            ->select(
            	'discount_fees.id',
            	'discount_fees.discount_amount',
            	'discount_fees.discount_description',
            	'students.admission_number',
            	'students.student_name',
            	'students.id as student_id',
            	'users.name'
            )
            ->where('discount_fees.semester_id', $currentSemester)
            ->get();

            return view('discounts.index')
                ->with('discounts', $discounts);
    }

    public function deleteDiscount($id){
    	DB::table('discount_fees')->where('id', $id)->delete();
    	return redirect()->back();
    }
}
