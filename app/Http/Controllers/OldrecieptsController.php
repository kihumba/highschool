<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Student;
use App;

class OldrecieptsController extends Controller
{
    public function oldReciepts($id){
    	$semesters = DB::table('semesters')->get();
        $students = DB::table('students')
            ->where('id', $id)
            ->first();
    	return view('oldReciepts.oldReciept')
    		->with('student', $students)
            ->with('studentId', $id)
    		->with('semesters', $semesters);
    }

    public function paidOldReceiptfee(Request $request){
    	 if( $request->ajax()){
	        $amountpaid = $request->get('amountPaid');
	        $totalBalance = $request->get('totalBalance');
	        $semesterId = $request->get('semesterId');
            $paymentMethod = $request->get('paymentMethod');
	        $studentId = $request->get('studentId');
	        $paymentDateDone = date("Y/m/d H:i:s", strtotime( $request->get('paymentDateDone') ));
	        $paymentNumber = str_random(9);

       

        $studentOldBalId = DB::table('old_receipt')
            ->where('student_id', $studentId)
            ->where('semester_id', $semesterId)
            ->pluck('student_id')
            ->last();
        if($studentOldBalId){

        $balance = DB::table('old_receipt')
            ->where('student_id', $studentId)
            ->pluck('balance')
            ->last();

         DB::table('old_receipt')->insert([
                'amount_paid' => $amountpaid,
                'amount' => $totalBalance,
                'receipt_number' => $paymentNumber,
                'balance' => $totalBalance - $amountpaid,
                'semester_id'=>$semesterId,
                'payment_method'=>$paymentMethod,
                'date_of_payment'=>$paymentDateDone,
                'student_id'=>$studentId
            ]);
       
        }else{

        	
             DB::table('old_receipt')->insert([
                'amount_paid' => $amountpaid,
                'amount' => $totalBalance,
                'balance' => $totalBalance - $amountpaid,
                'receipt_number'=>$paymentNumber,
                'semester_id'=>$semesterId,
                'payment_method'=>$paymentMethod,
                'date_of_payment'=>$paymentDateDone,
                'student_id'=>$studentId
            ]);



        }
    }
    	$studentbalance = DB::table('old_receipt')
            ->where('student_id', $studentId)
            ->pluck('balance')
            ->last();

        if($studentbalance){
            $balance = $studentbalance;

        }else{
             $balance = "";
        }
       

      return json_encode([
      						'balance' => $balance,
                            'studentId' => $studentId,
      						'id' => $paymentNumber
      					]);
    } 

    public function oldReceiptPdf($id){
    	
        $studentId = DB::table('old_receipt')
             ->where('receipt_number', $id)
             ->pluck('student_id')
             ->first();



         $studentName = DB::table('students')
            ->where('id', $studentId)
            ->pluck('student_name')
            ->first(); 

        $receiptDate = DB::table('old_receipt')->where('receipt_number', $id)->pluck('created_at')->first();
        $datePaymentDone = DB::table('old_receipt')->where('receipt_number', $id)->pluck('date_of_payment')->first();

        $servedBy = Auth::user()->name;



        $semester_name = DB::table('old_receipt')
            ->join('semesters', 'semesters.id', '=', 'old_receipt.semester_id')
            ->where('receipt_number', $id)
            ->pluck('semesters.semester_name')
            ->first();
        $balance = DB::table('old_receipt')
            ->where('receipt_number', $id)
            ->pluck('balance')
            ->first();

        $intial_balance = DB::table('old_receipt')
            ->where('receipt_number', $id)
            ->pluck('amount')
            ->first();
        $amountPaid = DB::table('old_receipt')
            ->where('receipt_number', $id)
            ->pluck('amount_paid')
            ->first();
         $receiptData = DB::table('old_receipt')
            ->where('receipt_number', $id)
            ->first();


            // dd($receiptData);
  


               $html = view('oldReciepts.receipt', 
                [
                    'payments_intialBalance' => $intial_balance, 
                    'payments_balance' => $balance, 
                    'datePaymentDone' => $datePaymentDone, 
                    'payments_amountPaid' => $amountPaid, 
                    'student' => $studentName, 
                    'receipt_no' => $id,
                    'receiptData' => $receiptData,
                    'semester_name' => $semester_name,
                    'receiptDate' => $receiptDate,
                    'servedBy' => $servedBy
                ]
            )->render();

            $pdf = App::make('dompdf.wrapper');
            $invPDF = $pdf->loadHTML($html);

            return $pdf->stream('receipt.pdf');
              
    	

    }
}
