<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Payment;
use DB;
use PDF;
use App;
use Auth;
use Session;

class PaymentController extends Controller
{
	   public function __construct()
    {
        $this->middleware('auth');
    }

    public function feePayment($id){
		$formId = Student::where('id', $id)->pluck('form_id')->first();

	     $currentSemester = DB::table('semesters')
	        ->where('current', 1)
	        ->pluck('id')
	        ->first();

	 

        	$voteheadAmountIds = DB::table('form_voteheads')
	    		->join('votehead_amounts', 'votehead_amounts.id', '=', 'form_voteheads.votehead_amount_id')
	    		->join('semesters', 'semesters.id', '=', 'votehead_amounts.semester_id')
		    	->where('form_voteheads.semester_id', $currentSemester)
		    	->whereYear('semesters.updated_at', date('Y'))
		    	->where('semesters.current', 1)
		    	->where('form_voteheads.form_id', $formId)
		    	->pluck('votehead_amounts.votehead_id')
		    	->toArray();


    		foreach( $voteheadAmountIds as  $votehead ){
				$paidAmounts[] = DB::table('payments')
					->join('voteheads', 'voteheads.id', '=', 'payments.votehead_id')
					->join('votehead_amounts', 'votehead_amounts.votehead_id', '=', 'payments.votehead_id')
					->join('semesters', 'semesters.id', '=', 'votehead_amounts.semester_id')
					->select(
						DB::raw("
							(votehead_amounts.amount - SUM(payments.amount)) as balance,
							voteheads.votehead_name,
							votehead_amounts.amount as votehead_amount,
							SUM(payments.amount) as paid,
							voteheads.id as votehead_id,
							votehead_amounts.semester_id
						")
					)
					->where('payments.student_id', $id)
					->where('payments.semester_id', $currentSemester)
					->where('semesters.current', 1)
					->where('payments.votehead_id', $votehead)
					->groupBy('payments.votehead_id')
					->get();
				}

                $addedFeeAmount = DB::table('added_fee')
                    ->where('student_id', $id)
                    ->pluck('amount')
                    ->sum();
                $addedFeePaid = DB::table('added_fee')
                    ->where('student_id', $id)
                    ->pluck('amount_paid')
                    ->sum();
                $addedFeeDescription = DB::table('added_fee')
                    ->where('student_id', $id)
                    ->pluck('fee_description')
                    ->first();
                $addedFeeBalance = $addedFeeAmount - $addedFeePaid;
                
                $SumAmount = DB::table('balance_brought_forward')
                    ->where('student_id', $id)
                    ->pluck('amount')
                    ->sum();
                $sumAmountPaid = DB::table('balance_brought_forward')
                    ->where('student_id', $id)
                    ->pluck('amount_paid')
                    ->sum();

                $balanceBroughtForward = $SumAmount - $sumAmountPaid;
	            

		

			
			return view('payments.feePayment')
                ->with('addedFeeDescription', $addedFeeDescription)
                ->with('balanceBroughtForward', $balanceBroughtForward)
                ->with('addedFeeBalance', $addedFeeBalance)
				->with('student_id', $id)
				->with('balances', $paidAmounts);
    }


     public function newpayment(Request $request){
    	$voteheadAmount = $request->get('amount');
    	$semesterId = $request->get('semesterId');
    	$totalAmount = $request->get('totalAmount');

    	$amountPaid = $request->get('amount');
    	$voteheadId = $request->get('voteheadId');
    	$studentId = $request->get('studentId');
    	$paymentMethod = $request->get('paymentMethod');
    	$paymentDateDone =date("Y/m/d H:i:s", strtotime( $request->get('paymentDateDone') ));

    	$currentSession = $voteheadAmount + Session::get('assigned');

    	if( $currentSession <= $totalAmount){

    		do{
	    		if(!Session::has('assigned')){
		    		Session::put('assigned', $voteheadAmount);
		    		$currentSession = $voteheadAmount;
		    	}else{
		    		$currentSession = Session::get('assigned');
		    		$currentSession = $currentSession + $voteheadAmount;
		    	}

		    		Session::put('assigned', $currentSession);

		    		$paymentId = str_random(9);

		    		$idExists = Payment::where('receipt_number', $paymentId)
		    			->first();

					do{
	    				$paymentId = str_random(9);
	    			}while($idExists);
    				if(!Session::has('receipt_number')){
    					Session::put('receipt_number', $paymentId);
    				}

	    		

	    			$payment = new Payment();
	    			$payment->amount = $amountPaid;
	    			$payment->balance = null;
	    			$payment->votehead_id = $voteheadId;
	    			$payment->semester_id = $request->get('semesterId');
	    			$payment->student_id = $studentId;
	    			$payment->date_payment_done = $paymentDateDone;
	    			$payment->receipt_number = Session::get('receipt_number');

	    			if( $payment->save() ){
	    				$formId = DB::table('students')
	    					->where('id', $studentId)
	    					->pluck('form_id')
	    					->first();

	    				$voteheadIds = DB::table('form_voteheads')
			                ->where('semester_id', $semesterId)
			                ->where('form_id', $formId)
			                ->pluck('votehead_amount_id')
			                ->toArray();

			            $totalAmountPayable = DB::table('votehead_amounts')
			                ->join('semesters', 'semesters.id', '=', 'votehead_amounts.semester_id')
			                ->where('semesters.current', 1)
  			                ->whereIn('votehead_amounts.id', $voteheadIds)
			                ->pluck('votehead_amounts.amount')
			                ->sum();

			            // $balance = $totalAmountPayable - $amountPaid;
	    				
	    			
	    		
							$paymentMade = DB::table('receipt_balances')
								->where('student_id', $studentId)
								->where('semester_id', $request->get('semesterId'))
								->first();

							if( $paymentMade ){
								$initialBalance = DB::table('receipt_balances')
									->where('student_id', $studentId)
									->where('semester_id', $request->get('semesterId'))
									->pluck('new_balance')
									->last();
							}else{
								$initialBalance = $totalAmountPayable;
							}

							$newBalance = $initialBalance - $totalAmount;

							if( $currentSession == $totalAmount ){
					
								DB::table('receipt_balances')->insert([
									'receipt_number' => Session::get('receipt_number'),
									'initial_balance' => $initialBalance,
									'amount_paid' => $totalAmount,
									'new_balance' => $newBalance,
									'student_id' => $studentId,
									'semester_id' => $request->get('semesterId'),
									'payment_method' => $paymentMethod,
									'created_at' => date('Y-m-d H:i:s')
								]);
							}
							// END OF POPULATE RECEIPT BALANCES TABLE

						
							// END OF POPULATE RECEIPT BALANCES TABLE
					}
	    			
	    			

			    	$formId = Student::where('id', $request->get('studentId'))
				    	->pluck('form_id')
				    	->first();

			    	$voteheadAmountIds = DB::table('form_voteheads')
				    	->where('form_id', $formId)
				    	->pluck('votehead_amount_id')
				    	->toArray();


				    	foreach( $voteheadAmountIds as $votehead ){
							$paidAmounts[] = DB::table('payments')
								->join('voteheads', 'voteheads.id', '=', 'payments.votehead_id')
								->join('votehead_amounts', 'votehead_amounts.votehead_id', '=', 'payments.votehead_id')
								->select(
									DB::raw("
										votehead_amounts.amount - (SUM(payments.amount)) as balance,
										voteheads.votehead_name,
										votehead_amounts.amount as votehead_amount,
										SUM(payments.amount) as paid,
										voteheads.id as votehead_id,
										votehead_amounts.semester_id
									")
								)
								->where('student_id', $request->get('studentId'))
								->where('payments.votehead_id', $votehead)
								->get();

						}
					$studentForm = Student::where('id', $request->get('studentId'))
				    	->pluck('form_id')
				    	->first();
				    	if ($studentForm == 1) {
				    		$formUrl ="form1";
				    	}else if ($studentForm == 2){
				    		$formUrl ="form2";
				    	}else if ($studentForm == 3){
							$formUrl ="form3";
				    	}elseif ($studentForm == 4) {
				    			$formUrl ="form4";
				    	}else{
				    		$formUrl = 0;
				    	}


	    		return json_encode([
	    			'formUrl' => $formUrl, 
	    			'assigned' => Session::get('assigned'), 
	    			'balance' => $totalAmount - Session::get('assigned'),
	    			'id' => Session::get('receipt_number')
	    		]);

	    	}while($currentSession <= $totalAmount);

    	}else{
    		return json_encode(Session::get("Amounts Exceeds"));
    	}

    }


    public function receipt($id){
		Session::forget('assigned');
        Session::forget('receipt_number');

        $studentData = DB::table('students')
            ->join('payments', 'payments.student_id', '=', 'students.id')
            ->select(
                'students.id',
                'students.student_name'
            )
            ->where('payments.receipt_number', $id)
            ->first(); 
        $paidVoteheads = DB::table('payments')
            ->join('voteheads', 'voteheads.id','=', 'payments.votehead_id')
            ->where('receipt_number', $id)
            ->where('student_id',$studentData->id)
            ->where('amount', '<>', 0)
            ->select(
                'voteheads.votehead_name',
                'payments.amount'
                
            )
            ->get();       

        $receiptDate = DB::table('payments')->where('receipt_number', $id)->pluck('created_at')->first();
        $datePaymentDone = DB::table('payments')->where('receipt_number', $id)->pluck('date_payment_done')->first();

        $servedBy = Auth::user()->name;

        $receiptData = DB::table('receipt_balances')
            ->where('receipt_number', $id)
            ->first();

         $currentSemester = DB::table('semesters')
            ->where('current', 1)
            ->pluck('id')
            ->first();

        /***************************************************************
            1. Cheque
            2. M-pesa
            3. Cash
            4. Other
        ****************************************************************/
           
      
        $data = [
            'datePaymentDone' => $datePaymentDone, 
            'paidVoteheads' => $paidVoteheads, 
            'student' => $studentData, 
            'receipt_no' => $id,
            'receiptDate' => $receiptDate,
            'servedBy' => $servedBy,
            'receiptData' => $receiptData
        ];

        $pdf = PDF::loadView('payments.receipt', $data);

        return $pdf->stream('receipt.pdf');

    }
    public function studentReport($id){
    	$studentName = DB::table('students')
    			->where('id', $id)
    			->first();
     	$currentSemester = DB::table('semesters')
            ->where('current', 1)
            ->pluck('id')
            ->first();

            

		$payments = DB::table('payments')
            ->where('semester_id', $currentSemester)
			->where('student_id', $id)
			->get();

		$amountPaid =  DB::table('payments')
            ->join('semesters', 'semesters.id', '=', 'payments.semester_id')
            ->where('payments.semester_id', $currentSemester)
            ->where('semesters.current', 1)
            ->where('payments.student_id', $id)
            ->pluck('payments.amount')
            ->sum();

        $voteheadIds = DB::table('form_voteheads')
            ->where('semester_id', $currentSemester)
            ->where('form_id', $studentName->form_id)
            ->pluck('votehead_amount_id')
            ->toArray();

        $totalAmount = DB::table('votehead_amounts')
            ->join('semesters', 'semesters.id', '=', 'votehead_amounts.semester_id')
            ->where('semesters.current', 1)
            ->whereIn('votehead_amounts.id', $voteheadIds)
            ->pluck('votehead_amounts.amount')
            ->sum();

        $balance = $totalAmount - $amountPaid;

                   
        $payments = DB::table('payments')
         	->join('voteheads', 'voteheads.id', 'payments.votehead_id')
         	->join('votehead_amounts', 'votehead_amounts.votehead_id', '=', 'payments.votehead_id')
			->join('semesters', 'semesters.id', '=', 'votehead_amounts.semester_id')
         	->select(
         		'voteheads.votehead_name',
         		'payments.amount',
         		'payments.created_at'
         	)
            ->where('payments.student_id', $id)
            ->where('payments.amount', '>', 0)
            ->where('payments.semester_id', $currentSemester)
            ->where('semesters.current', 1)
            ->get();

        $addedFeeReceipt = DB::table('added_fee')
            ->where('student_id', $id)
            ->where('semester_id', $currentSemester)
            ->pluck('receipt_number')
            ->last(); 

        $totalAmountAddedFee = DB::table('added_fee')
            ->where('student_id', $id)
            ->where('semester_id', $currentSemester)
            ->pluck('amount')
            ->first();
        $totalAmountpaidAddedFee = DB::table('added_fee')
            ->where('student_id', $id)
            ->where('semester_id', $currentSemester)
            ->pluck('amount_paid')
            ->sum();
       

            if($addedFeeReceipt == Null){
                
                $addbalance = DB::table('added_fee')
                    ->where('student_id', $id)
                    ->where('semester_id', $currentSemester)
                    ->pluck('amount')
                    ->first();
                $totalBalances = $balance + $addbalance;
            }else{
                $addbalance = DB::table('added_fee')
                    ->where('semester_id', $currentSemester)
                    ->where('student_id', $id)
                    ->where('receipt_number', $addedFeeReceipt)
                    ->pluck('balance')
                    ->last();
                $totalBalances = $balance + $addbalance;
            }
         $addedStudentFees = DB::table('added_fee')
            ->where('semester_id', $currentSemester)
            ->where('student_id', $id)
            ->get();

         $balanceBroughtForwardDetails = DB::table('balance_brought_forward')
            ->where('student_id', $id)
            ->where('amount_paid', '<>', !0)
            ->select(
                'balance_brought_forward.created_at',
                'balance_brought_forward.amount_paid'


            )
            ->get();
        $balanceReceiptId = DB::table('balance_brought_forward')
            ->where('student_id', $id)
            ->where('amount', Null)
            ->first();

             if($balanceReceiptId == Null){

                $balanceBroughtForward = DB::table('balance_brought_forward')
                    ->where('student_id', $id)
                    ->pluck('amount')
                    ->first();
                $balanceAmountPaid = 0;
                $totalBalanceAmountPaid = DB::table('balance_brought_forward')
                    ->where('student_id', $id)
                    ->pluck('amount')
                    ->first();
                   

            }else{
                 $balanceBroughtForward = DB::table('balance_brought_forward')
                    ->where('student_id', $id)
                    ->pluck('balance')
                    ->last();
                 $balanceAmountPaid = DB::table('balance_brought_forward')
                    ->where('student_id', $id)
                    ->pluck('amount_paid')
                    ->sum();

                $totalBalanceAmountPaid = DB::table('balance_brought_forward')
                    ->where('student_id', $id)
                    ->pluck('amount')
                    ->first();
                }

	return view('payments.reports.studentReport')
        ->with('studentName', $studentName)
        ->with('addedStudentFees', $addedStudentFees)
        ->with('balanceBroughtForwardDetails', $balanceBroughtForwardDetails)
        ->with('balanceAmountPaid', $balanceAmountPaid)
        ->with('totalAmountAddedFee', $totalAmountAddedFee)
        ->with('balanceBroughtForward', $balanceBroughtForward)
        ->with('totalBalanceAmountPaid', $totalBalanceAmountPaid)
        ->with('amountPaid', $amountPaid + $totalAmountpaidAddedFee)
        ->with('balance', $totalBalances)
        ->with('totalAmount', $totalAmount)
        ->with('id', $id)
        ->with('payments', $payments);

    }  

     public function printreport($id){

        $studentData = DB::table('students')
            ->where('id', $id)
            ->first();

        $payments = DB::table('payments')
            ->where('student_id', $id)
            ->get();
         $currentSemester = DB::table('semesters')
            ->where('current', 1)
            ->pluck('id')
            ->first();

        $totalAmountPaid =  DB::table('payments')
            ->join('semesters', 'semesters.id', '=', 'payments.semester_id')
            ->where('payments.semester_id', $currentSemester)
            ->where('semesters.current', 1)
            ->where('payments.student_id', $id)
            ->pluck('payments.amount')
            ->sum();

        $voteheadAmountIds = DB::table('form_voteheads')
            ->where('form_id', $studentData->form_id)
            ->pluck('votehead_amount_id')
            ->toArray();

        $totalAmount = DB::table('votehead_amounts')
            ->join('semesters', 'semesters.id', '=', 'votehead_amounts.semester_id')
            ->where('semesters.current', 1)
            ->whereIn('votehead_amounts.id', $voteheadAmountIds)
            ->pluck('votehead_amounts.amount')
            ->sum();

        $balance = $totalAmount - $totalAmountPaid;

         $addedFeeReceipt = DB::table('added_fee')
            ->where('student_id', $id)
            ->where('semester_id', $currentSemester)
            ->pluck('receipt_number')
            ->last(); 

        $totalAmountAddedFee = DB::table('added_fee')
            ->where('student_id', $id)
            ->where('semester_id', $currentSemester)
            ->pluck('amount')
            ->first();
        $totalAmountpaidAddedFee = DB::table('added_fee')
            ->where('student_id', $id)
            ->where('semester_id', $currentSemester)
            ->pluck('amount_paid')
            ->sum();
            if($addedFeeReceipt == Null){
                $addbalance = DB::table('added_fee')
                    ->where('student_id', $id)
                    ->where('semester_id', $currentSemester)
                    ->pluck('amount')
                    ->first();
                $totalBalances = $balance + $addbalance;
            }else{
                $addbalance = DB::table('added_fee')
                    ->where('semester_id', $currentSemester)
                    ->where('student_id', $id)
                    ->where('receipt_number', $addedFeeReceipt)
                    ->pluck('balance')
                    ->last();
                $totalBalances = $balance + $addbalance;
            }

            
                   
        $payments = DB::table('payments')
            ->join('voteheads', 'voteheads.id', 'payments.votehead_id')
            ->join('votehead_amounts', 'votehead_amounts.votehead_id', '=', 'payments.votehead_id')
			->join('semesters', 'semesters.id', '=', 'votehead_amounts.semester_id')
            ->select(
                'voteheads.votehead_name',
                'payments.amount',
                'payments.created_at'
            ) 
            ->where('payments.student_id', $id)
            ->where('payments.amount', '>', 0)
            ->where('semesters.current', 1)
            ->get();

        $balanceBroughtForwardDetails = DB::table('balance_brought_forward')
            ->where('student_id', $id)
            ->where('amount_paid', '<>', !0)
            ->select(
                'balance_brought_forward.created_at',
                'balance_brought_forward.amount_paid'


            )
            ->get();

        $balanceReceiptId = DB::table('balance_brought_forward')
            ->where('student_id', $id)
            ->where('amount', Null)
            ->first();

             if($balanceReceiptId == Null){

                $balanceBroughtForward = DB::table('balance_brought_forward')
                    ->where('student_id', $id)
                    ->pluck('amount')
                    ->first();
                $balanceAmountPaid = 0;
                $totalBalanceAmountPaid = DB::table('balance_brought_forward')
                    ->where('student_id', $id)
                    ->pluck('amount')
                    ->first();
                   

            }else{
                 $balanceBroughtForward = DB::table('balance_brought_forward')
                    ->where('student_id', $id)
                    ->pluck('balance')
                    ->last();
                 $balanceAmountPaid = DB::table('balance_brought_forward')
                    ->where('student_id', $id)
                    ->pluck('amount_paid')
                    ->sum();

                $totalBalanceAmountPaid = DB::table('balance_brought_forward')
                    ->where('student_id', $id)
                    ->pluck('amount')
                    ->first();
                }
          $addedStudentFees = DB::table('added_fee')
            ->where('semester_id', $currentSemester)
            ->where('student_id', $id)
            ->get();


            $html = view('payments.reports.feeReport', 
                [
                    'name' => $studentData->student_name, 
                    'amountPaid' => $totalAmountPaid + $totalAmountpaidAddedFee + $balanceAmountPaid,
                    'totalAmount' => $totalAmount + $totalAmountAddedFee + $totalBalanceAmountPaid,
                    'balance' => $totalBalances + $balanceBroughtForward,
                    'balanceBroughtForwardDetails' => $balanceBroughtForwardDetails,
                    'addedStudentFees' => $addedStudentFees,
                    'payments' => $payments
                ]
            )->render();

            $pdf = App::make('dompdf.wrapper');
            $invPDF = $pdf->loadHTML($html);

            return $pdf->stream('feeReport.pdf');
    }

    public function addStudentFee(Request $request, $id){
        $currentSemester = DB::table('semesters')
            ->where('current', 1)
            ->pluck('id')
            ->first();
        DB::table('added_fee')->insert([
            'amount' => $request->input('amount'),
            'fee_description' => $request->input('fee_description'),
            'intial_balance' => $request->input('amount'),
            'semester_id'=> $currentSemester,
            'student_id'=> $id,
            'date_payment_done'=> Null,
            'amount_paid'=> Null,
            'balance'=> Null,
            'receipt_number'=> Null
        ]);
        return redirect()->back();

    }
    public function payAddedfee(Request $request){

         if( $request->ajax()){
            $amountpaid = $request->get('amountPaid');
            $paymentMethod = $request->get('paymentMethod');
            $studentId = $request->get('studentId');
            $paymentDateDone = date("Y/m/d H:i:s", strtotime( $request->get('paymentDateDone') ));
            $paymentNumber = str_random(9);

      
        $currentSemester = DB::table('semesters')
                ->where('current', 1)
                ->pluck('id')
                ->first();
       

        $studentReceipt = DB::table('added_fee')
            ->where('student_id', $studentId)
            ->pluck('receipt_number')
            ->last();
        if($studentReceipt){

        $balance = DB::table('added_fee')
            ->where('student_id', $studentId)
            ->pluck('balance')
            ->last();

         DB::table('added_fee')->insert([
                'amount_paid' => $amountpaid,
                'amount' => Null,
                'fee_description' => Null,
                'intial_balance' => $balance,
                'receipt_number' => $paymentNumber,
                'balance' => $balance - $amountpaid,
                'semester_id'=>$currentSemester,
                'payment_method'=>$paymentMethod,
                'date_payment_done'=>$paymentDateDone,
                'student_id'=>$studentId
            ]);
       
        }else{
             $intial_balance = DB::table('added_fee')
                ->where('student_id', $studentId)
                ->pluck('intial_balance')
                ->first();
            
             DB::table('added_fee')->insert([
                'amount_paid' => $amountpaid,
                'amount' => Null,
                'fee_description' => Null,
                'intial_balance' => $intial_balance,
                'receipt_number' => $paymentNumber,
                'balance' => $intial_balance - $amountpaid,
                'semester_id'=>$currentSemester,
                'payment_method'=>$paymentMethod,
                'date_payment_done'=>$paymentDateDone,
                'student_id'=>$studentId
            ]);
          



        }
        $studentForm = Student::where('id', $request->get('studentId'))
            ->pluck('form_id')
            ->first();
            if ($studentForm == 1) {
                $formUrl ="form1";
            }else if ($studentForm == 2){
                $formUrl ="form2";
            }else if ($studentForm == 3){
                $formUrl ="form3";
            }elseif ($studentForm == 4) {
                    $formUrl ="form4";
            }else{
                $formUrl = 0;
            }
             return json_encode([
                'id' => $paymentNumber,
                'formUrl' => $formUrl
            ]);

        }
        
     
    }

    public function payAddedfeeReciept($id){
        $studentId = DB::table('added_fee')
             ->where('receipt_number', $id)
             ->pluck('student_id')
             ->first();



         $studentName = DB::table('students')
            ->where('id', $studentId)
            ->pluck('student_name')
            ->first(); 
        $description = DB::table('added_fee')
            ->where('student_id', $studentId)
            ->pluck('fee_description')
            ->first(); 

        $receiptDate = DB::table('added_fee')->where('receipt_number', $id)->pluck('created_at')->first();
        $datePaymentDone = DB::table('added_fee')->where('receipt_number', $id)->pluck('date_payment_done')->first();

        $servedBy = Auth::user()->name;



        $balance = DB::table('added_fee')
            ->where('receipt_number', $id)
            ->pluck('balance')
            ->first();

        $intial_balance = DB::table('added_fee')
            ->where('receipt_number', $id)
            ->pluck('intial_balance')
            ->first();
        $amountPaid = DB::table('added_fee')
            ->where('receipt_number', $id)
            ->pluck('amount_paid')
            ->first();
         $receiptData = DB::table('added_fee')
            ->where('receipt_number', $id)
            ->first();
 


               $html = view('addedFees.receipt', 
                [
                    'payments_intialBalance' => $intial_balance, 
                    'payments_balance' => $balance, 
                    'datePaymentDone' => $datePaymentDone, 
                    'payments_amountPaid' => $amountPaid, 
                    'student' => $studentName, 
                    'receipt_no' => $id,
                    'receiptData' => $receiptData,
                    'description' => $description,
                    'receiptDate' => $receiptDate,
                    'servedBy' => $servedBy
                ]
            )->render();

            $pdf = App::make('dompdf.wrapper');
            $invPDF = $pdf->loadHTML($html);

            return $pdf->stream('receipt.pdf');
              
        

     }

}
