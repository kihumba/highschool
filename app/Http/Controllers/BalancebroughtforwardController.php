<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App;
use App\Student;

class BalancebroughtforwardController extends Controller
{
        public function paybalancebroughtForward(Request $request){
     	 if( $request->ajax()){
	        $amountpaid = $request->get('amount');
	        $studentId = $request->get('studentId');
	        $paymentMethod = $request->get('paymentMethod');
	        $paymentDateDone = date("Y/m/d H:i:s", strtotime( $request->get('paymentDateDone') ));
	        $paymentNumber = str_random(9);

        $studentPaidAmount = DB::table('balance_brought_forward')
            ->where('student_id', $studentId)
            ->pluck('amount_paid')
            ->sum();
        if($studentPaidAmount == 0){

        $intial_balance = DB::table('balance_brought_forward')
            ->where('student_id', $studentId)
            ->pluck('intial_balance')
            ->first();

         DB::table('balance_brought_forward')->insert([
                'amount_paid' => $amountpaid,
                'intial_balance' => $intial_balance,
                'receipt_number' => $paymentNumber,
                'balance' => $intial_balance - $amountpaid,
                'payment_method'=>$paymentMethod,
                'date_payment_done'=>$paymentDateDone,
                'student_id'=>$studentId
            ]);
       
        }else{

        	$intial_balanc = DB::table('balance_brought_forward')
	            ->where('student_id', $studentId)
	            ->pluck('balance')
	            ->last();

             DB::table('balance_brought_forward')->insert([
                'amount_paid' => $amountpaid,
                'intial_balance' => $intial_balanc,
                'balance' => $intial_balanc - $amountpaid,
                'receipt_number'=>$paymentNumber,
                'payment_method'=>$paymentMethod,
                'date_payment_done'=>$paymentDateDone,
                'student_id'=>$studentId
            ]);



        }
    }

    	 $studentForm = Student::where('id', $studentId)->pluck('form_id')->first();
	    	if ($studentForm == 1) {
	    		$formUrl ="form1";
	    	}else if ($studentForm == 2){
	    		$formUrl ="form2";
	    	}else if ($studentForm == 3){
				$formUrl ="form3";
	    	}elseif ($studentForm == 4) {
	    			$formUrl ="form4";
	    	}else{
	    		$formUrl = 0;
	    	}
 
      return json_encode([
      						'id' => $paymentNumber,
      						'formUrl' => $formUrl
      					]);

     }

     public function balanceBroughtForwardPdf($id){
     	$studentId = DB::table('balance_brought_forward')
     		 ->where('receipt_number', $id)
     		 ->pluck('student_id')
     		 ->first();



     	 $studentName = DB::table('students')
            ->where('id', $studentId)
            ->pluck('student_name')
            ->first(); 

        $receiptDate = DB::table('balance_brought_forward')->where('receipt_number', $id)->pluck('created_at')->first();
        $datePaymentDone = DB::table('balance_brought_forward')->where('receipt_number', $id)->pluck('date_payment_done')->first();

        $servedBy = Auth::user()->name;



        $payments_intialBalance = DB::table('balance_brought_forward')
        	->where('receipt_number', $id)
        	->pluck('intial_balance')
        	->first();

    	$payments_balance = DB::table('balance_brought_forward')
        	->where('receipt_number', $id)
        	->pluck('balance')
        	->first();
    	$payments_amountPaid = DB::table('balance_brought_forward')
        	->where('receipt_number', $id)
        	->pluck('amount_paid')
        	->first();
    	$receiptData = DB::table('balance_brought_forward')
            ->where('receipt_number', $id)
            ->first();


               $html = view('balanceBroughtForward.receipt', 
                [
		            'payments_intialBalance' => $payments_intialBalance, 
		            'payments_balance' => $payments_balance, 
		            'datePaymentDone' => $datePaymentDone, 
		            'payments_amountPaid' => $payments_amountPaid, 
		            'student' => $studentName, 
		            'receipt_no' => $id,
		            'receiptDate' => $receiptDate,
		            'receiptData' => $receiptData,
		            'servedBy' => $servedBy
                ]
            )->render();

            $pdf = App::make('dompdf.wrapper');
            $invPDF = $pdf->loadHTML($html);

            return $pdf->stream('receipt.pdf');
              

     }
}
