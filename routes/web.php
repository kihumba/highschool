<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('ittc', ['as' => 'getpdf', 'uses' => 'MainController@getpdf']);
Route::get('get/ittc', ['as' => 'ittcTeachers', 'uses' => 'MainController@ittcTeachers']);
Route::get('get/ittc/vocational', ['as' => 'ittcvocational', 'uses' => 'MainController@ittcvocational']);
Route::get('get/email', ['as' => 'email', 'uses' => 'MainController@email']);
Route::get('get/pdf', ['as' => 'getpdf', 'uses' => 'MainController@getpdf']);
Route::get('get/nonTeachingCourses', ['as' => 'nonTeachingCourses', 'uses' => 'MainController@nonTeachingCourses']);


Auth::routes();



Route::group(['middleware' => 'auth'], function () {
	Route::get('/home', 'HomeController@index')->name('home');


	//START STUDENTS ROUTES
	Route::get('/inactive/students', 'StudentsController@inactiveStudents')->name('inactiveStudents');
	Route::get('/students', 'StudentsController@students')->name('students');
	Route::post('/get/form/streams', 'StudentsController@getFormStreams')->name('getFormStreams');
	Route::post('/save/student', 'StudentsController@savestudent')->name('savestudent');
	Route::post('/update/student', 'StudentsController@updateStudent')->name('updateStudent');
	Route::post('/save/{id}/parent', 'StudentsController@saveparent')->name('saveparent');
	Route::get('/edit/{id}/student', 'StudentsController@editStudent')->name('editStudent');
	Route::post('/change/{id}/student/form', 'StudentsController@changeForm')->name('changeForm');
	Route::post('/change/all/{id}/student/form', 'StudentsController@changeAllStudentsForm')->name('changeAllStudentsForm');
	Route::get('/delete/{id}/student', 'StudentsController@deleteStudent')->name('deleteStudent');
	Route::get('/students/{id}/upgrade', 'StudentsController@upgradeStudents')->name('upgradeStudents');
	Route::post('/students/save/upgrade', 'StudentsController@saveUpgradeStudent')->name('saveUpgradeStudent');
	Route::get('/students/form1', 'StudentsController@form1')->name('form1');
	Route::get('/students/form2', 'StudentsController@form2')->name('form2');
	Route::get('/students/form3', 'StudentsController@form3')->name('form3');
	Route::get('/students/form4', 'StudentsController@form4')->name('form4');
	Route::get('/add/{id}/students', 'StudentsController@addstudent')->name('addstudent');
	Route::get('/students/{id}/details', 'StudentsController@studentDetails')->name('studentDetails');
	//END OF STUDENTS ROUTES

	//START TEACHERS ROUTES
	Route::get('/teachers', 'TeachersController@index')->name('teachers');
	Route::get('/create/teacher', 'TeachersController@create')->name('createTeacher');
	Route::post('/assign/subjects', 'TeachersController@assignSubjects')->name('assignSubjects');
	Route::get('/form/{id}/streams', 'TeachersController@formStreams')->name('formStreams');
	Route::get('/activate/{id}/teacher', 'TeachersController@activateTeacher')->name('activateTeacher');
	Route::get('/deactivateTeacher/{id}/teacher', 'TeachersController@deactivateTeacher')->name('deactivateTeacher');
	Route::get('/delete/{id}/teacher', 'TeachersController@deleteTeacher')->name('deleteTeacher');
	Route::post('/edit/{id}/teacher', 'TeachersController@editTeacher')->name('editTeacher');
	Route::post('/save/teacher', 'TeachersController@saveTeacher')->name('saveTeacher');
	Route::post('/save/teachers/subjects', 'TeachersController@saveTeacherSubject')->name('saveTeacherSubject');
	//END OF TEACHERS ROUTES

	//START OF STREAMS ROUTES
	Route::get('/streams', 'SettingsController@streams')->name('streams');
	Route::post('/add/Stream', 'SettingsController@store')->name('addStream');
	Route::get('/delete/{id}/Stream', 'SettingsController@deleteStream')->name('deleteStream');
	Route::post('/edit/{id}/Stream', 'SettingsController@editStream')->name('editStream');

	//END OF STREAMS ROUTES

	//START OF SUBJECTS ROUTES
	Route::get('/subjects', 'SettingsController@subjects')->name('subjects');
	Route::get('/delete/{id}/subjects', 'SettingsController@deleteSubjects')->name('deleteSubjects');
	Route::post('/edit/{id}/subjects', 'SettingsController@editSubject')->name('editSubject');
	Route::post('/save/subjects', 'SettingsController@saveSubject')->name('saveSubject');

	//END OF SUBJECTS ROUTES

	
	//START OF VOTEHEADS ROUTES
	Route::get('/voteheads', 'SettingsController@voteheads')->name('voteheads');
	Route::post('/add/votehead', 'SettingsController@addVotehead')->name('addVotehead');
	Route::get('/delete/{id}/votehead', 'SettingsController@deleteVotehead')->name('deleteVotehead');
	Route::post('/update/{id}/votehead', 'SettingsController@updateVotehead')->name('updateVotehead');


	//START OF SEMESTER ROUTES
	Route::get('/semesters', 'SettingsController@semester')->name('semester');
	Route::get('/delete/{id}/semester', 'SettingsController@deleteSemester')->name('deleteSemester');
	Route::get('/update/{id}/semester/status', 'SettingsController@semesterUpdate')->name('semesterUpdate');
	Route::post('/update/{id}/semester', 'SettingsController@update_semester')->name('update_semester');
	Route::post('/add/semester', 'SettingsController@add_semester')->name('add_semester');

	//END OF SEMESTER ROUTES
	//START OF VOTEHEAD AMOUNTS ROUTES
	Route::get('/delete/{id}/votehead/amounts', 'SettingsController@deleteVoteheaAamount')->name('deleteVoteheaAamount');
	Route::get('/votehead/amounts', 'SettingsController@voteheadAmounts')->name('voteheadAmounts');
	Route::post('/add/votehead/amounts', 'SettingsController@add_vamount')->name('add_vamount');
	Route::post('/update/{id}/votehead/amounts', 'SettingsController@update_vamount')->name('update_vamount');


	//END OF VOTEHEAD AMOUNTS ROUTES
	//START OF FORM VOTEHEAD  ROUTES
	Route::get('/assigned/votehead', 'SettingsController@assignedVotehead')->name('assignedVotehead');
	Route::post('/save/active/voteheads', 'SettingsController@saveActiveVoteheads')->name('saveActiveVoteheads');
	Route::get('/assign/{id}/votehead', 'SettingsController@assignVotehead')->name('assignVotehead');

	
	//END OF FORM VOTEHEAD  ROUTES
	//START OF ACADEMIC   ROUTES 
	Route::post('/get/Students/', 'AcademicsController@getStudents')->name('getStudents');
	Route::post('/get/results/', 'AcademicsController@getResults')->name('getResults');
	Route::post('/get/students/data/', 'AcademicsController@studentsData')->name('studentsData');
	Route::post('/save/marks', 'AcademicsController@savemarks')->name('savemarks');
	Route::post('/get/streams/subjects', 'AcademicsController@getStreams')->name('getStreams');
	Route::get('/upload/marks', 'AcademicsController@uploadMarks')->name('uploadMarks');
	Route::get('/select/report', 'AcademicsController@selectReport')->name('selectReport');
	Route::get('/print/transcript', 'AcademicsController@transcript')->name('transcript');
	Route::post('/get/form/stream/ids', 'AcademicsController@getFormIdStreamId')->name('getFormIdStreamId');
	Route::get('/print/{streamId}/{formId}/transcript', 'AcademicsController@printTranscripts')->name('printTranscripts');


	//END OF ACADEMIC  ROUTES
	//START OF REMARKS
	Route::get('/remarks', 'RemarksController@remarks')->name('remarks');
	Route::post('/save/remark', 'RemarksController@saveRemarks')->name('saveRemarks');
	Route::post('/update/{id}/remark', 'RemarksController@updateRemark')->name('updateRemark');
	Route::get('/delete/{id}/remark', 'RemarksController@deleteRemark')->name('deleteRemark');

	//END OF REMARKS
	//START OF PAYMENTS   ROUTES
	Route::get('/fee/{id}/payment', 'PaymentController@feePayment')->name('feePayment');
	Route::get('/print/{id}/report', 'PaymentController@printreport')->name('printreport');
	Route::get('/fee/{id}/report', 'PaymentController@studentReport')->name('studentReport');
	Route::get('/student/{id}/receipt', 'PaymentController@receipt')->name('receipt');
	Route::post('/new/payment', 'PaymentController@newpayment')->name('newpayment');
	Route::post('/add/{id}/student/fee', 'PaymentController@addStudentFee')->name('addStudentFee');
	Route::post('/pay/added/student/fee', 'PaymentController@payAddedfee')->name('payAddedfee');
	Route::get('/students/added/fee/{id}/pdf', 'PaymentController@payAddedfeeReciept')->name('payAddedfeeReciept');


	//END OF PAYMENTS   ROUTES
	// START DISCOUNT ROUTES
	Route::post('/give/{id}/discount', ['as' => 'discount', 'uses' => 'DiscountController@discount' ] );
	Route::post('/edit/{id}/discount', ['as' => 'editDiscount', 'uses' => 'DiscountController@editDiscount' ] );
	Route::get('/delete/{id}/discount', ['as' => 'deleteDiscount', 'uses' => 'DiscountController@deleteDiscount' ] );
	Route::get('/discounted/students', ['as' => 'discounted', 'uses' => 'DiscountController@discounted' ] );

	// END DISCOUNT ROUTES
	// START OLD RECIEPTS ROUTES
	Route::get('/old/{id}/receipts', ['as' => 'oldReciepts', 'uses' => 'OldrecieptsController@oldReciepts' ] );
	Route::get('/old/receipt/{id}/pdf', ['as' => 'oldReceiptPdf', 'uses' => 'OldrecieptsController@oldReceiptPdf' ] );
	Route::post('/pay/old/receipts', ['as' => 'paidOldReceiptfee', 'uses' => 'OldrecieptsController@paidOldReceiptfee' ] );
	// END OLD RECIEPTS ROUTES

	// END BALANCE BROUGHT FORWARD ROUTES
	Route::post('/pay/balance/brought/forward', ['as' => 'paybalancebroughtForward', 'uses' => 'BalancebroughtforwardController@paybalancebroughtForward' ] );
	Route::get('/balance/{id}/brought/forward/pdf', ['as' => 'balanceBroughtForwardPdf', 'uses' => 'BalancebroughtforwardController@balanceBroughtForwardPdf' ] );


	// END BALANCE BROUGHT FORWARD ROUTES


	Route::resource('roles','RoleController');
    Route::resource('users','UserController');

	Route::get('/logout', function(){
	    Session::flush();
	    Auth::logout();
	    return Redirect::to("/login")
	      ->with('message', array('type' => 'success', 'text' => 'You have successfully logged out'));
	});

});

Auth::routes();