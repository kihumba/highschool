<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_subjects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('form_id', false, true)->unsigned()->index();
            $table->bigInteger('subject_id', false, true)->unsigned()->index();

            $table->foreign('form_id')
              ->references('id')
              ->on('forms')
              ->onUpdate('cascade')
              ->onDelete('cascade');

            $table->foreign('subject_id')
              ->references('id')
              ->on('subjects')
              ->onUpdate('cascade')
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_subjects');
    }
}
