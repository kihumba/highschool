<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormVoteheadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_voteheads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('form_id', false, true)->unsigned()->index();
            $table->bigInteger('votehead_amount_id', false, true)->unsigned()->index();
            $table->bigInteger('semester_id', false, true)->unsigned()->index();

            $table->foreign('form_id')
              ->references('id')
              ->on('forms')
              ->onUpdate('cascade')
              ->onDelete('cascade');
            $table->foreign('semester_id')
              ->references('id')
              ->on('semesters')
              ->onUpdate('cascade')
              ->onDelete('cascade');

            $table->foreign('votehead_amount_id')
              ->references('id')
              ->on('votehead_amounts')
              ->onUpdate('cascade')
              ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_voteheads');
    }
}
