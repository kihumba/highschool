<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOldReceiptTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('old_receipt', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('amount', 12,2);
            $table->float('amount_paid', 12,2);
            $table->float('balance', 12,2);
            $table->string('receipt_number');
            $table->date('date_of_payment');
            $table->bigInteger('semester_id', false, true)->unsigned()->index();
            $table->bigInteger('student_id', false, true)->unsigned()->index();
           

            $table->foreign('semester_id')
              ->references('id')
              ->on('semesters')
              ->onUpdate('cascade')
              ->onDelete('cascade');  

             $table->foreign('student_id')
              ->references('id')
              ->on('students')
              ->onUpdate('cascade')
              ->onDelete('cascade');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('old_receipt');
    }
}
