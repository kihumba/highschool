<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipt_balances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('receipt_number', 10);
            $table->float('initial_balance', 12, 2);
            $table->float('amount_paid', 12, 2);
            $table->float('new_balance', 12, 2);
            $table->integer('payment_method');

            $table->bigInteger('student_id', false, true)->unsigned()->index();
            $table->bigInteger('semester_id', false, true)->unsigned()->index();

            $table->foreign('student_id')
              ->references('id')
              ->on('students')
              ->onUpdate('cascade')
              ->onDelete('cascade');

            $table->foreign('semester_id')
              ->references('id')
              ->on('semesters')
              ->onUpdate('cascade')
              ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt_balances');
    }
}
