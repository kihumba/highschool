<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('year');
            $table->float('amount', 12, 2);
            $table->float('balance', 12, 2)->nullable();
            $table->string('receipt_number', 20)->nullable();
            $table->timestamp('date_payment_done')->nullable();

            $table->bigInteger('semester_id', false, true)->unsigned()->index();
            $table->bigInteger('student_id', false, true)->unsigned()->index();
            $table->bigInteger('votehead_id', false, true)->unsigned()->index();

            $table->foreign('semester_id')
              ->references('id')
              ->on('semesters')
              ->onUpdate('cascade')
              ->onDelete('cascade');

            $table->foreign('student_id')
              ->references('id')
              ->on('students')
              ->onUpdate('cascade')
              ->onDelete('cascade');

            $table->foreign('votehead_id')
              ->references('id')
              ->on('voteheads')
              ->onUpdate('cascade')
              ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
