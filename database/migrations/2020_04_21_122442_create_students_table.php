<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('student_name', 200);
            $table->string('admission_number', 50);
            $table->string('kcpe_index_number', 50)->nullable();
            $table->string('grade', 10)->nullable();
            $table->string('jointed_year', 10)->nullable();
            $table->date('dob', 50);

            $table->bigInteger('parent_id', false, true)->unsigned()->index();
            $table->bigInteger('stream_id', false, true)->unsigned()->index();
            $table->bigInteger('form_id', false, true)->unsigned()->index();
            $table->bigInteger('parent_relationship_id', false, true)->unsigned()->index();
            $table->bigInteger('gender_id', false, true)->unsigned()->index();
            $table->bigInteger('county_id', false, true)->unsigned()->index();

            $table->foreign('parent_id')
              ->references('id')
              ->on('parents')
              ->onUpdate('cascade')
              ->onDelete('restrict');

            $table->foreign('stream_id')
              ->references('id')
              ->on('streams')
              ->onUpdate('cascade')
              ->onDelete('restrict');

            $table->foreign('form_id')
              ->references('id')
              ->on('forms')
              ->onUpdate('cascade')
              ->onDelete('restrict'); 

            $table->foreign('parent_relationship_id')
              ->references('id')
              ->on('student_parent_relationship')
              ->onUpdate('cascade')
              ->onDelete('restrict');

            $table->foreign('gender_id')
              ->references('id')
              ->on('genders')
              ->onUpdate('cascade')
              ->onDelete('restrict');

            $table->foreign('county_id')
              ->references('id')
              ->on('counties')
              ->onUpdate('cascade')
              ->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
