<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBalanceBroughtForwardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_brought_forward', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('amount', 12, 2)->nullable();
            $table->float('intial_balance', 12, 2)->nullable();
            $table->float('amount_paid', 12, 2)->nullable();
            $table->float('balance', 12, 2)->nullable();
            $table->string('receipt_number')->nullable();
            $table->timestamp('date_payment_done')->nullable();
            $table->bigInteger('student_id', false, true)->unsigned()->index();

            $table->foreign('student_id')
              ->references('id')
              ->on('students')
              ->onUpdate('cascade')
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_brought_forward');
    }
}
