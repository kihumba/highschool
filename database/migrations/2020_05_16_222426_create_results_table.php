<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
             $table->bigIncrements('id');

            $table->float('cat_one', 5, 1)->nullable();
            $table->float('cat_two', 5, 1)->nullable();
            $table->float('end_term', 5, 1)->nullable();

            $table->bigInteger('student_id', false, true)->unsigned()->index();
            $table->bigInteger('semester_id', false, true)->unsigned()->index();
            $table->bigInteger('form_id', false, true)->unsigned()->index();
            $table->bigInteger('stream_id', false, true)->unsigned()->index();
            $table->bigInteger('subject_id', false, true)->unsigned()->index();

            $table->foreign('student_id')
              ->references('id')
              ->on('students')
              ->onUpdate('cascade')
              ->onDelete('cascade');
              
            $table->foreign('form_id')
              ->references('id')
              ->on('forms')
              ->onUpdate('cascade')
              ->onDelete('cascade');

            $table->foreign('semester_id')
              ->references('id')
              ->on('semesters')
              ->onUpdate('cascade')
              ->onDelete('cascade');

            $table->foreign('stream_id')
              ->references('id')
              ->on('streams')
              ->onUpdate('cascade')
              ->onDelete('cascade');

            $table->foreign('subject_id')
              ->references('id')
              ->on('subjects')
              ->onUpdate('cascade')
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
