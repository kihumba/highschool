<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_fees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('discount_amount', 12,2);
            $table->string('discount_description', 50);
            $table->bigInteger('student_id', false, true)->unsigned()->index();
            $table->bigInteger('semester_id', false, true)->unsigned()->index();
            $table->bigInteger('user_id', false, true)->unsigned()->index();
           

            $table->foreign('student_id')
              ->references('id')
              ->on('students')
              ->onUpdate('cascade')
              ->onDelete('cascade'); 
              
            $table->foreign('user_id')
              ->references('id')
              ->on('users')
              ->onUpdate('cascade')
              ->onDelete('cascade');

            $table->foreign('semester_id')
              ->references('id')
              ->on('semesters')
              ->onUpdate('cascade')
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_fees');
    }
}
