<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddedFeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('added_fee', function (Blueprint $table) {
           $table->bigIncrements('id');
            $table->string('fee_description', 50)->nullable();
            $table->float('amount', 12, 2)->nullable();
            $table->float('intial_balance', 12, 2)->nullable();
            $table->float('amount_paid', 12, 2)->nullable();
            $table->float('balance', 12, 2)->nullable();
            $table->string('receipt_number')->nullable();
            $table->string('payment_method')->nullable();
            $table->timestamp('date_payment_done')->nullable();
            $table->bigInteger('student_id', false, true)->unsigned()->index();
            $table->bigInteger('semester_id', false, true)->unsigned()->index();

            $table->foreign('student_id')
              ->references('id')
              ->on('students')
              ->onUpdate('cascade')
              ->onDelete('cascade'); 
            $table->foreign('semester_id')
              ->references('id')
              ->on('semesters')
              ->onUpdate('cascade')
              ->onDelete('cascade'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('added_fee');
    }
}
