<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('teacher_name', 200);
            $table->string('national_id', 20);
            $table->string('phone_number', 20);
            $table->string('degree', 100);
            $table->string('reg_number', 20);
            $table->string('status', 5);
            $table->string('experience', 10)->nullable();
            $table->string('previous_teaching_school', 100)->nullable();
            $table->bigInteger('gender_id', false, true)->unsigned()->index();

            $table->foreign('gender_id')
              ->references('id')
              ->on('genders')
              ->onUpdate('cascade')
              ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
