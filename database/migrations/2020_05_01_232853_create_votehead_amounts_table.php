<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoteheadAmountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votehead_amounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('amount', 12, 2);

            $table->bigInteger('semester_id', false, true)->unsigned()->index();
            $table->bigInteger('votehead_id', false, true)->unsigned()->index();

            $table->foreign('semester_id')
              ->references('id')
              ->on('semesters')
              ->onUpdate('cascade')
              ->onDelete('cascade');

            $table->foreign('votehead_id')
              ->references('id')
              ->on('voteheads')
              ->onUpdate('cascade')
              ->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votehead_amounts');
    }
}
